﻿using FastClash.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using FastClash.BotFunction;
using FastClash.BotFunction.Other;

namespace FastClash.ViewModels
{

    /// <summary>
    /// A Logging class implementing the Singleton pattern and an internal Queue to be flushed perdiodically
    /// </summary>
    public class LogWriter : BindableBase
    {
        private static readonly LogWriter instance = new LogWriter();
        private static readonly Queue<LogEntry> LogQueue = new Queue<LogEntry>();
        private static readonly string LogDir = GlobalVariables.LogPath;

        private static readonly string LogFile = DateTime.Now.ToString("MM-dd-yyyy_HH-mm",
            CultureInfo.InvariantCulture) + ".txt";

        private const int MaxLogAge = 60; // Max Age in seconds
        private const int QueueSize = 100; // Max Queue Size
        private static DateTime _lastFlushed = DateTime.Now;

        /// <summary>
        /// Private constructor to prevent instance creation
        /// </summary>
        //private LogWriter() { }

        /// <summary>
        /// An LogWriter instance that exposes a single instance
        /// </summary>
        public static LogWriter Instance
        {
            get
            {
                return instance;
            }
        }

        private string _log = "";

        public string Log
        {
            get { return _log; }
            set { SetField(ref _log, value); OnPropertyChanged("Log"); }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { SetField(ref status, value); OnPropertyChanged("Status"); }
        }
        /// <summary>
        /// The single instance method that writes to the log file
        /// </summary>
        /// <param name="message">The message to write to the log</param>
        public void WriteToLog(string message)
        {
            // Lock the queue while writing to prevent contention for the log file
            lock (LogQueue)
            {
                // Create the entry and push to the Queue
                LogEntry logEntry = new LogEntry(message);
                LogQueue.Enqueue(logEntry);
                //Log += String.Format("[{0:HH:mm:ss}] {1}", DateTime.Now, message + Environment.NewLine);
                string logHere = String.Format("[{0:HH:mm:ss}] {1}", DateTime.Now, message + Environment.NewLine);
                Status = message;
                for (int i = 0; i < logHere.Length; i++)
                {
                    Log += logHere[i];
                    SleepFunction.Sleep(5);
                }
                //Log += logHere;
                // If we have reached the Queue Size then flush the Queue
                if (LogQueue.Count >= QueueSize || DoPeriodicFlush())
                    FlushLog();
            }
        }



        /// <summary>
        /// Does the periodic flush.
        /// </summary>
        /// <returns><c>true</c> if max age since last flush, <c>false</c> otherwise.</returns>
        private static bool DoPeriodicFlush()
        {
            TimeSpan logAge = DateTime.Now - _lastFlushed;
            if (logAge.TotalSeconds >= MaxLogAge)
            {
                _lastFlushed = DateTime.Now;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Flushes the Queue to the physical log file
        /// </summary>
        private static void FlushLog()
        {
            string logPath = Path.Combine(LogDir, LogFile); // Path.Combine(_logDir, string.Format("{0}_{1}", _logQueue.First().LogDate, _logFile));
            Directory.CreateDirectory(LogDir);
            using (var fs = File.Open(logPath, FileMode.Append, FileAccess.Write))
            {
                using (var log = new StreamWriter(fs))
                {
                    while (LogQueue.Count > 0)
                    {
                        LogEntry entry = LogQueue.Dequeue();
                        log.WriteLine(string.Format("{0}\t{1}", entry.LogTime, entry.Message));
                    }
                }
            }
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="LogWriter"/> class.
        /// Log will be flushed even those limits are not reached yet when program is shutting down.
        /// </summary>
        ~LogWriter()
        {
            FlushLog();
        }
    }

    /// <summary>
    /// A Log class to store the message and the Date and Time the log entry was created
    /// </summary>
    public class LogEntry
    {
        public string Message { get; set; }
        public string LogTime { get; set; }
        public string LogDate { get; set; }

        public LogEntry(string message)
        {
            Message = message;
            LogDate = DateTime.Now.ToString("yyyy-MM-dd");
            LogTime = DateTime.Now.ToString("HH:mm:ss.fff");
        }
    }
}
