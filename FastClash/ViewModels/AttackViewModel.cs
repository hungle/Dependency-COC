﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastClash.ViewModels
{
    public class AttackViewModel : BindableBase
    {
        internal static Properties.Settings AppSettings { get { return Properties.Settings.Default; } }
        //deployment area
        private int _numSide = AppSettings.NumSize;

        public int NumSide
        {
            get { return _numSide; }
            set
            {
                if (value > 4)
                {
                    value = 4;
                }
                if (value < 1)
                {
                    value = 1;
                }
                _numSide = value;
                OnPropertyChanged("NumSide");
            }
        }


        private bool _isRedline = AppSettings.IsRedline;

        public bool IsRedline
        {
            get { return _isRedline; }
            set
            {
                _isRedline = value;
                OnPropertyChanged("_isRedline");
            }
        }

        private int _unitDelay = AppSettings.UnitDelay;

        public int UnitDelay
        {
            get { return _unitDelay; }
            set
            {
                _unitDelay = value;
                OnPropertyChanged("UnitDelay");
            }
        }

        private int _waveDelay = AppSettings.WaveDelay;

        public int WaveDelay
        {
            get { return _waveDelay; }
            set
            {
                _waveDelay = value;
                OnPropertyChanged("WaveDelay");
            }
        }

        private bool _isRandomDelay = AppSettings.IsRandomDelay;

        public bool IsRandomDelay
        {
            get { return _isRandomDelay; }
            set
            {
                _isRandomDelay = value;
                OnPropertyChanged("IsRandomDelay");
            }
        }

        private bool _isUseKing = AppSettings.IsUseKing;

        public bool IsUseKing
        {
            get { return _isUseKing; }
            set
            {
                _isUseKing = value;
                OnPropertyChanged("IsUseKing");
            }
        }

        private bool _isUseQueen = AppSettings.IsUseQueen;

        public bool IsUseQueen
        {
            get { return _isUseQueen; }
            set
            {
                _isUseQueen = value;
                OnPropertyChanged("IsUseQueen");
            }
        }

        private bool _isUseCC = AppSettings.IsUseCC;

        public bool IsUseCC
        {
            get { return _isUseCC; }
            set
            {
                _isUseCC = value;
                OnPropertyChanged("IsUseCC");
            }
        }
        // end

        //end battle region

        private int _secondNoLootChange = AppSettings.SecondNoLootChange;

        public int SecondNoLootChange
        {
            get { return _secondNoLootChange; }
            set
            {
                _secondNoLootChange = value;
                OnPropertyChanged("SecondNoLootChange");
            }
        }

        private bool _isResouceBelow = AppSettings.IsResourceBelow;

        public bool IsResouceBelow
        {
            get { return _isResouceBelow; }
            set
            {
                _isResouceBelow = value;
                OnPropertyChanged("IsResouceBelow");
            }
        }


        private int _goldBelow = AppSettings.GBelow;

        public int GoldBelow
        {
            get { return _goldBelow; }
            set
            {
                _goldBelow = value;
                OnPropertyChanged("GoldBelow");
            }
        }

        private int _elixirBelow = AppSettings.EBelow;

        public int ElixirBelow
        {
            get { return _elixirBelow; }
            set
            {
                _elixirBelow = value;
                OnPropertyChanged("ElixirBelow");
            }
        }

        private int _darkBelow = AppSettings.DEBelow;

        public int DarkBelow
        {
            get { return _darkBelow; }
            set
            {
                _darkBelow = value;
                OnPropertyChanged("DarkBelow");
            }
        }

        //end
        //contructor
    }
}
