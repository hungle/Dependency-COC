﻿using FastClash.BotFunction.Donate;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClash.BotFunction.ReadText;
using FastClash.Misc.Enum;
using FastClashFunction;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Input;

namespace FastClash.ViewModels
{
    class DonateViewModel : BindableBase
    {
        internal static Properties.Settings AppSettings { get { return Properties.Settings.Default; } }
        private string textRequest = "any";

        public class DonationTroopModel
        {
            public int Id { get; set; }
            public string Keywords { get; set; }
            public string Name { get; set; }
            public bool CanDonate { get; set; }

            public DonationTroopModel(int id, string name, string keywords, bool canDonate)
            {
                Id = id;
                Name = name;
                Keywords = keywords;
                CanDonate = canDonate;
            }
        }

        public Point CCLocation { get; set; }
        public Command CCLocateCommand { get; set; }

        public string TextRequest
        {
            get { return textRequest; }
            set { SetField(ref textRequest, value); }
        }

        private bool isRequest = false;
        public bool IsRequest
        {
            get { return isRequest; }
            set { SetField(ref isRequest, value); }
        }

        private bool donateAll;

        public bool DonateAll
        {
            get { return donateAll; }
            set { SetField(ref donateAll, value); }
        }

        private string blacklist;

        public string Blacklist
        {
            get { return blacklist; }
            set { SetField(ref blacklist, value); }
        }

        private ObservableCollection<DonationTroopModel> troops;

        public ObservableCollection<DonationTroopModel> Troops
        {
            get { return troops; }
            set { SetField(ref troops, value); }
        }

        public enum DonatableTroopKind
        {
            Barbarian, Archer, Giant, Goblin, WallBreaker, Balloon, Wizard, Healer,
            Dragon, Pekka, Minion, HogRider, Valkyrie, Golem, Witch, LavaHound
        }
        public DonateViewModel()
        {
            DonateAll = AppSettings.DonateAll;
            Blacklist = AppSettings.Blacklist;

            CCLocateCommand = new Command(LocateCC);
            CCLocation = AppSettings.CCLocation;
            Troops = new ObservableCollection<DonationTroopModel>();
            int id = 0;
            foreach (var troop in Enum.GetNames(typeof(DonatableTroopKind)))
            {
                string keywords = AppSettings[troop + "Keywords"].ToString();
                bool canDonate = Boolean.Parse(AppSettings[troop + "CanDonate"].ToString());
                Troops.Add(new DonationTroopModel(id, troop, keywords, canDonate));
                ++id;
            }
        }

        public void LocateCC()
        {
            string log = "Place your mouse over your cc and press enter.";

            var result = System.Windows.MessageBox.Show(log);
            if (result == System.Windows.MessageBoxResult.OK)
            {
                Win32.POINT point;
                Win32.GetCursorPos(out point);
                Win32.ScreenToClient(BlueStacksHelper.GetBlueStacksWindowHandle(), ref point);
                if (point.X < 0 || point.Y < 0)
                {
                    log = "Point out of bounds.";
                    return;
                }
                CCLocation = point;
                log = String.Format("X={0},Y={1}", point.X, point.Y);
                LogFunction.SetLog(log);
            }
        }

        public void Donate()
        {
            if (DonateFunctions.IsHaveNews())
            {
                DonateFunctions.OpenDonateTab();
                OcrClanText ocr = new OcrClanText();
                //up 299,135,0xA0D204
                //down 298,660,0xADD405

                //First , bot need to go to top of clan text
                //Check to see have green arrow up 
                while (PixelTool.CheckColorAtPosition(299, 135, Color.FromArgb(0xA0D204), 20, true, false))
                {
                    ClickFunction.Click(299, 135); // See it, then click it
                    SleepFunction.Sleep(200);
                }

                var donate = true;

                while (donate)
                {
                    //find position of doante button
                    var donateButtonPoint = IMGTool.FindImginArea("donatebutton", 87, 154, 223 - 87, 670 - 154, 0.31D, false);
                    while (!donateButtonPoint.IsEmpty)
                    {
                        //-34
                        string requestText = ocr.GetClanText(donateButtonPoint.Y - 34);
                        var tropDonate = TroopFromRequest(requestText);
                        if (tropDonate != null)
                        {
                            DonateFunctions.DonateTroop(tropDonate.Value, donateButtonPoint.Y, 8);
                        }
                        SleepFunction.Sleep(200);

                        //find another donate button below
                        donateButtonPoint = IMGTool.FindImginArea("donatebutton", 87, donateButtonPoint.Y + 10, 223 - 87, 670 - (donateButtonPoint.Y + 10), 0.31D, false);
                    }
                    // now onscreen dont have any donate button
                    donate = false;

                    //Check to see have green arrow down 
                    if (PixelTool.CheckColorAtPosition(298, 660, Color.FromArgb(0xADD405), 20, true, false))
                    {
                        ClickFunction.Click(298, 660); // See it, then click it
                        SleepFunction.Sleep(200);
                        donate = true;
                    }

                }
            }
        }

        /// <summary>
        /// This function will find what troop to donate depending on requestText
        /// </summary>
        /// <param name="requestText"></param>
        /// <returns></returns>
        public TroopKindEnum? TroopFromRequest(string requestText)
        {
            foreach(var t in Troops)
            {
                if (t.Keywords.Contains(requestText))
                {
                    return (TroopKindEnum)t.Id;
                }
            }
            return null;
        }
    }
}
