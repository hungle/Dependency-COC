﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Windows;
using FastClash.BotFunction.MainScreen;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Train;
using FastClash.BotFunction.Village;
using FastClash.BotFunction.Attack;
using FastClash.BotFunction.ReadText;
using FastClash.BotFunction.Search;
using FastClashFunction;

namespace FastClash.ViewModels
{
    class MainViewModel : BindableBase
    {
        public LogWriter LogWriter
        {
            get { return LogWriter.Instance; }
        }
        public VillageResource VillageResource
        {
            get { return VillageResource.Instance; }
        }

        private TrainViewModel _trainViewModel;

        public TrainViewModel TrainViewModel
        {
            get { return _trainViewModel; }
            set
            {
                SetField(ref _trainViewModel, value);
            }
        }

        private SearchViewModel _searchViewModel;
        public SearchViewModel SearchViewModel
        {
            get
            {
                return _searchViewModel;
            }
            set
            {
                SetField(ref _searchViewModel, value);
            }
        }
        private AttackViewModel _attackViewModel;
        public AttackViewModel AttackViewModel
        {
            get { return _attackViewModel; }
            set
            {
                SetField(ref _attackViewModel, value);
            }
        }

        private DonateViewModel _donateViewModel;
        public DonateViewModel DonateViewModel
        {
            get { return _donateViewModel; }
            set { SetField(ref _donateViewModel, value); }
        }
        public bool IsRunning { get; set; }

        private string botVisibility;

        public string BotVisibility
        {
            get { return botVisibility; }
            set { SetField(ref botVisibility, value); }
        }

        public ObservableCollection<string> Developers { get; set; }
        #region Commands
        public Command RunCommand
        {
            get { return new Command(Run); }
        }
        public Command VisitCommand
        {
            get { return new Command(Visit); }
        }
        public Command StopCommand
        {
            get { return new Command(Stop); }
        }
        public Command HideCommand
        {
            get { return new Command(Hide); }
        }
        public Command MinimizeCommand
        {
            get { return new Command(Minimize); }
        }

        public Command ExitCommand
        {
            get { return new Command(Exit); }
        }
        #endregion
        internal static Properties.Settings AppSettings { get { return Properties.Settings.Default; } }
        public MainViewModel()
        {
            BotVisibility = "Hide Bluestacks";
            Developers = new ObservableCollection<string>();
            Developers.Add("HungLe");
            Developers.Add("Blaze");
            Developers.Add("Usman - Forum Admin");
            SearchViewModel = new SearchViewModel();

            AttackViewModel = new AttackViewModel();
            TrainViewModel = new TrainViewModel();
            DonateViewModel = new DonateViewModel();
        }

        private string _RunButtonText = "Run Bot";

        public string RunButtonText
        {
            get { return _RunButtonText; }
            set { SetField(ref _RunButtonText, value); }
        }


        private bool paused;
        CancellationTokenSource _cts;
        public void Stop()
        {
            if (_cts != null)
            {
                RunButtonText = "Run Bot";
                LogFunction.SetLog("BOT STOP");
                GlobalVariables.isFirstTime = true;
                _cts.Cancel();
            }
        }

        public void Visit()
        {
            System.Diagnostics.Process.Start("http://fastclash.com");
        }

        public void Run()
        {
            if (RunButtonText == "Run Bot")
            {
                if (_cts != null)
                {
                    LogFunction.SetLog("Bot Start");
                }
                _cts = new CancellationTokenSource();
                GlobalVariables.token = _cts.Token;
                GlobalVariables.currentProcessing = CurrentProcessing.CheckMainScreen;
                //GlobalVariables.currentProcessing = CurrentProcessing.WaitForFull;
                GlobalVariables.isFirstTime = true;
                Task mainTask = Task.Factory.StartNew(RunThread, GlobalVariables.token);
                RunButtonText = "Pause";
                paused = false;
                //t = new Thread(new ThreadStart(RunThread));
                //t.IsBackground = true;
                //t.Start();
            }
            else if (RunButtonText == "Pause")
            {
                if (paused == false)
                {
                    // This will acquire the lock on the syncObj,
                    // which, in turn will "freeze" the loop
                    // as soon as you hit a lock(syncObj) statement
                    RunButtonText = "Resume";
                    Monitor.Enter(GlobalVariables.syncObj);
                    paused = true;
                }
                //t.Join();
                //Thread.Sleep(Timeout.Infinite);
            }
            else if (RunButtonText == "Resume")
            {
                if (paused)
                {
                    paused = false;
                    RunButtonText = "Pause";
                    // This will allow the lock to be taken, which will let the loop continue
                    Monitor.Exit(GlobalVariables.syncObj);
                }
            }
        }

        private Village thisVillage;
        public void RunThread()
        {
            Camp camp = new Camp();
            camp.percentCamp = TrainViewModel.PercentCamp;
            thisVillage = new Village(camp);
            if (GlobalVariables.token.IsCancellationRequested) return;
            while (true)
            {
                switch (GlobalVariables.currentProcessing)
                {
                    case CurrentProcessing.CheckMainScreen:
                        new Mainscreen().FirstTimeBotRun();
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        ClickFunction.Click(1, 1);
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        SleepFunction.Sleep(1000);
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        GlobalVariables.currentProcessing = CurrentProcessing.VillageLoot;
                        break;
                    case CurrentProcessing.VillageLoot:
                        thisVillage.GetLootVillageInfo();
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        thisVillage.ReportLootVillage();
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        GlobalVariables.currentProcessing = CurrentProcessing.DetectBuilding;
                        break;
                    case CurrentProcessing.DetectBuilding:
                        thisVillage.DetectBuilding();
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        thisVillage.ReportBuilding();
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        thisVillage.CalcuateAtkBuildingLevel(true);
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        GlobalVariables.currentProcessing = CurrentProcessing.ReArm;
                        break;
                    case CurrentProcessing.ReArm:
                        thisVillage.ReArm(thisVillage.TownHall);
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        thisVillage.CollectResource(2);
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        thisVillage.GotLootRemaining();
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        GlobalVariables.currentProcessing = CurrentProcessing.WaitForFull;
                        break;
                    case CurrentProcessing.WaitForFull:
                        thisVillage.DropTrophies();

                        bool isFullCamp = thisVillage.IsCampFull(true);
                        if (GlobalVariables.token.IsCancellationRequested) return;

                        if (isFullCamp)
                        {
                            LogFunction.SetLog("Armycamp full!!!");
                            if (GlobalVariables.token.IsCancellationRequested) return;
                            GlobalVariables.currentProcessing = CurrentProcessing.Training;
                        }
                        else
                        {

                            thisVillage.Train(TrainType.Custom,Camp.TrainMode.Addon, TrainViewModel.Troops, false);
                            if (GlobalVariables.token.IsCancellationRequested) return;
                            GlobalVariables.currentProcessing = CurrentProcessing.WaitForFull;


                            ClickFunction.Click(GlobalVariables.pointAway);
                            if (GlobalVariables.token.IsCancellationRequested) return;
                            SleepFunction.Sleep(1000);
                            if (GlobalVariables.token.IsCancellationRequested) return;


                            thisVillage.CollectResource(2);
                            if (GlobalVariables.token.IsCancellationRequested) return;

                            ClickFunction.Click(GlobalVariables.pointAway);
                            if (GlobalVariables.token.IsCancellationRequested) return;

                            thisVillage.GetLootVillageInfo();
                            if (GlobalVariables.token.IsCancellationRequested) return;

                            for (int i = 0; i < 30; i++)
                            {
                                SleepFunction.Sleep(1000);
                                if (GlobalVariables.token.IsCancellationRequested) return;
                                DonateViewModel.Donate();
                            }
                            new Mainscreen().WaitMainScreen();
                        }
                        break;
                    case CurrentProcessing.Training:
                        thisVillage.Train(TrainType.Custom, Camp.TrainMode.Fresh, TrainViewModel.Troops, true);
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        GlobalVariables.currentProcessing = CurrentProcessing.Searching;
                        break;
                    //do a search and raid
                    case CurrentProcessing.Searching:
                        GlobalVariables.currentProcessing = CurrentProcessing.Attacking;
                        SearchEngine searchEngine = new SearchEngine(SearchViewModel, 0);
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        EnemyVillage enemyVillage = searchEngine.DidASearch();
                        if (GlobalVariables.token.IsCancellationRequested) return;
                        if (GlobalVariables.currentProcessing == CurrentProcessing.Attacking)
                        {
                            AttackSmart attackSmart = new AttackSmart(enemyVillage, AttackViewModel);
                            if (GlobalVariables.token.IsCancellationRequested) return;
                            attackSmart.Attack();
                            if (GlobalVariables.token.IsCancellationRequested) return;
                            GlobalVariables.currentProcessing = CurrentProcessing.WaitForFull;
                        }
                        break;
                }
            }
        }

        public void Hide()
        {
            if (BlueStacksHelper.IsBlueStacksHidden)
            {
                if (BlueStacksHelper.ShowBlueStacks())
                    BotVisibility = "Hide Bluestacks";
                //BlueStacksHelper.Click(395, 41);
            }
            else
            {
                if (BlueStacksHelper.HideBlueStacks())
                    BotVisibility = "Show Bluestacks";
                //BlueStacksHelper.Click(395, 41);
            }
        }

        /// <summary>
        /// Minimizes the application.
        /// </summary>
        private void Minimize()
        {
            Application.Current.MainWindow.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// Exits the application.
        /// </summary>
        private void Exit()
        {
            /*if (IsExecuting)
                Stop();

            SaveUserSettings();*/
            SaveConfig();
            Environment.Exit(Environment.ExitCode);
        }


        private void SaveConfig()
        {
            AppSettings.IsNeedRangeTrop = VillageResource.IsNeedRangeTrop;
            AppSettings.MaxTrophies = VillageResource.MaxTrop;
            AppSettings.MinTrophies = VillageResource.MinTrop;

            AppSettings.IsDeadBase = SearchViewModel.IsDeadbase;
            foreach (LootEntity lootEntity in SearchViewModel.LootMinConditions)
            {
                AppSettings["Min" + lootEntity.LootName] = lootEntity.MinValue;
                AppSettings["Decrease" + lootEntity.LootName] = lootEntity.DecreaseValue;
                AppSettings["IsMust" + lootEntity.LootName] = lootEntity.IsMust;
            }
            AppSettings.IsReduction = SearchViewModel.IsReduction;
            AppSettings.SearchCount = SearchViewModel.TimesSearch;

            AppSettings.IsRedline = AttackViewModel.IsRedline;
            AppSettings.NumSize = AttackViewModel.NumSide;
            AppSettings.UnitDelay = AttackViewModel.UnitDelay;
            AppSettings.WaveDelay = AttackViewModel.WaveDelay;
            AppSettings.IsRandomDelay = AttackViewModel.IsRandomDelay;
            AppSettings.IsUseCC = AttackViewModel.IsUseCC;
            AppSettings.IsUseKing = AttackViewModel.IsUseKing;
            AppSettings.IsUseQueen = AttackViewModel.IsUseQueen;
            AppSettings.SecondNoLootChange = AttackViewModel.SecondNoLootChange;
            AppSettings.IsResourceBelow = AttackViewModel.IsResouceBelow;
            AppSettings.GBelow = AttackViewModel.GoldBelow;
            AppSettings.EBelow = AttackViewModel.ElixirBelow;
            AppSettings.DEBelow = AttackViewModel.DarkBelow;


            foreach (var troop in TrainViewModel.Troops)
            {
                AppSettings[troop.Name + "Train"] = troop.Count;
            }

            AppSettings.DonateAll = DonateViewModel.DonateAll;
            AppSettings.Blacklist = DonateViewModel.Blacklist;
            foreach (var troop in DonateViewModel.Troops)
            {
                string name = troop.Name;
                AppSettings[name + "Keywords"] = troop.Keywords;
                AppSettings[name + "CanDonate"] = troop.CanDonate;
            }
            AppSettings.CCLocation = DonateViewModel.CCLocation;

            AppSettings.Save();
        }


    }
}
