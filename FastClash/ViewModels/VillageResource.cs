namespace FastClash.ViewModels
{
    public class VillageResource : BindableBase
    {
        internal static Properties.Settings AppSettings { get { return Properties.Settings.Default; } }
        private static readonly VillageResource instance = new VillageResource();
        private string _gold;
        private string _elixir;
        private string _darkElixir;
        private string _gems;
        private string _builders;
        private string _trophies;
        private int _troopCapacity;
        private int _totalArmy;
        private int _currentArmy;
        private int _minTrop = AppSettings.MinTrophies;
        private bool _isNeedRangeTrop = AppSettings.IsNeedRangeTrop;
        public bool IsNeedRangeTrop
        {
            get { return _isNeedRangeTrop; }
            set { SetField(ref _isNeedRangeTrop, value); }
        }
        public int MinTrop
        {
            get { return _minTrop; }
            set { SetField(ref _minTrop, value); }
        }
        private int _maxTrop = AppSettings.MaxTrophies;
        public int MaxTrop
        {
            get { return _maxTrop; }
            set { SetField(ref _maxTrop, value); }
        }

        public static VillageResource Instance
        {
            get
            {
                return instance;
            }
        }


        public string Gold
        {
            get { return Instance._gold; }
            set {
                SetField(ref _gold, value); }
        }

        public string Elixir
        {
            get { return Instance._elixir; }
            set {
                SetField(ref _elixir, value);}
        }

        public string DarkElixir
        {
            get { return Instance._darkElixir; }
            set {
                SetField(ref _darkElixir, value);}
        }

        public string Gems
        {
            get { return Instance._gems; }
            set {
                SetField(ref _gems, value);}
        }

        public string Builders
        {
            get { return Instance._builders; }
            set {
                SetField(ref _builders, value);}
        }

        public string Trophies
        {
            get { return Instance._trophies; }
            set {
                SetField(ref _trophies, value);}
        }
        public int TotalArmy
        {
            get { return Instance._totalArmy; }
            set
            {
                SetField(ref _totalArmy, value);
            }
        }

        public int TroopCapacity
        {
            get { return Instance._troopCapacity; }
            set
            {
                SetField(ref _troopCapacity, value);
            }
        }
        public int CurrentArmy
        {
            get { return Instance._currentArmy; }
            set
            {
                SetField(ref _currentArmy, value);
            }
        }
    }
}