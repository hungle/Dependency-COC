﻿using FastClash.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastClash.ViewModels
{
    public enum LootName { Gold, Elixir, DarkElixir }

    [Serializable]
    public class LootEntity {
        public int MinValue { get; set; }
        public int DecreaseValue { get; set; }
        public bool IsMust { get; set; }
        public LootName LootName { get; set; }
        public string DisplayText { get; set; }
        public LootEntity(LootName lootName,int minValue = 0, int decreaseValue = 0, bool isMust = false){
            DisplayText = String.Concat("Min ",lootName.ToString(),": ");
            LootName = lootName;
            MinValue = minValue;
            DecreaseValue = decreaseValue;
            IsMust = isMust;
        }
    }

    public class SearchViewModel : BindableBase
    {
        internal static Properties.Settings AppSettings { get { return Properties.Settings.Default; } }
        //values
        private ObservableCollection<LootEntity> lootMinConditions;

        public ObservableCollection<LootEntity> LootMinConditions
        {
            get { return lootMinConditions; }
            set
            {
                lootMinConditions = value;
                OnPropertyChanged("LootMinConditions");
            }
        }
        //end


        //condition region
        private bool _isDeadbase = AppSettings.IsDeadBase;
        public bool IsDeadbase
        {
            get { return _isDeadbase; }
            set
            {
                _isDeadbase = value;
                OnPropertyChanged("IsDeadbase");
            }
        }

        //end

        //search reduction region
        private bool _isReduction = AppSettings.IsReduction;
        public bool IsReduction
        {
            get { return _isReduction; }
            set
            {
                _isReduction = value;
                OnPropertyChanged("IsReduction");
            }
        }

        private int _timesSearch = AppSettings.SearchCount;
        public int TimesSearch
        {
            get { return _timesSearch; }
            set
            {
                _timesSearch = value;
                OnPropertyChanged("TimesSearch");
            }
        }

        //end

        //contructor
        public SearchViewModel()
        {
            LootMinConditions = new ObservableCollection<LootEntity>();
            LootMinConditions.Add(new LootEntity(LootName.Gold, Int32.Parse(AppSettings["Min" + LootName.Gold].ToString()), Int32.Parse(AppSettings["Decrease" + LootName.Gold].ToString()), bool.Parse(AppSettings["IsMust" + LootName.Gold].ToString())));
            LootMinConditions.Add(new LootEntity(LootName.Elixir, Int32.Parse(AppSettings["Min" + LootName.Elixir].ToString()), Int32.Parse(AppSettings["Decrease" + LootName.Elixir].ToString()), bool.Parse(AppSettings["IsMust" + LootName.Elixir].ToString())));
            LootMinConditions.Add(new LootEntity(LootName.DarkElixir, Int32.Parse(AppSettings["Min" + LootName.DarkElixir].ToString()), Int32.Parse(AppSettings["Decrease" + LootName.DarkElixir].ToString()), bool.Parse(AppSettings["IsMust" + LootName.DarkElixir].ToString())));
        }
    }


}
