﻿using FastClash.BotFunction.Train;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace FastClash.ViewModels
{
    class TrainViewModel : BindableBase
    {
        internal static Properties.Settings AppSettings { get { return Properties.Settings.Default; } }
        private ObservableCollection<TroopModel> _troops;
        public ObservableCollection<TroopModel> Troops
        {
            get { return _troops; }
            set
            {
                _troops = value;
                GotTotalSpaceTroops();
                OnPropertyChanged("Troops");
            }
        }

        private int _totalSpaceTroop = 0;
        public int TotalSpaceTroop
        {
            get { return _totalSpaceTroop; }
            set
            {
                SetField(ref _totalSpaceTroop, value);
            }
        }

        private int _percentCamp = AppSettings.PercentCamp;
        public int PercentCamp
        {
            get { return _percentCamp; }
            set
            {
                if (value > 100)
                {
                    value = 100;
                }
                if (value < 0)
                {
                    value = 0;
                }
                SetField(ref _percentCamp, value);
            }
        }



        private void GotTotalSpaceTroops()
        {
            int totalSpace = 0;
            if (Troops != null)
            {
                foreach (TroopModel troop in Troops)
                {
                    totalSpace += troop.Count * troop.Space;
                }
            }
            TotalSpaceTroop = totalSpace;
        }

        void Troops_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                foreach (TroopModel item in e.NewItems)
                    item.PropertyChanged += TroopModel_PropertyChanged;

            if (e.OldItems != null)
                foreach (TroopModel item in e.OldItems)
                    item.PropertyChanged -= TroopModel_PropertyChanged;
        }

        void TroopModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Count")
                GotTotalSpaceTroops();
        }



        public TrainViewModel()
        {
            if (Troops == null)
            {
                Troops = new ObservableCollection<TroopModel>();
                Troops.CollectionChanged += Troops_CollectionChanged;

                System.Drawing.Point point = new System.Drawing.Point(195, 296);
                Troops.Add(new TroopModel(0, 1, "Barbarian", point, 20, 1, AppSettings.BarbarianTrain));
                point.X += 107;
                Troops.Add(new TroopModel(1, 2, "Archer", point, 25, 1, AppSettings.ArcherTrain));
                point.X += 107;
                Troops.Add(new TroopModel(2, 3, "Giant", point, 120, 5, AppSettings.GiantTrain));
                point.X += 107;
                Troops.Add(new TroopModel(3, 4, "Goblin", point, 30, 1, AppSettings.GoblinTrain));
                point.X += 107;
                Troops.Add(new TroopModel(4, 5, "WallBreaker", point, 120, 2, AppSettings.WallBreakerTrain));
                point.X = 195;
                point.Y += 108;
                Troops.Add(new TroopModel(5, 6, "Balloon", point, 480, 5, AppSettings.BalloonTrain));
                point.X += 107;
                Troops.Add(new TroopModel(6, 7, "Wizard", point, 480, 4, AppSettings.WizardTrain));
                point.X += 107;
                Troops.Add(new TroopModel(7, 8, "Healer", point, 900, 14, AppSettings.HealerTrain)); // 15*60 = 900
                point.X += 107;
                Troops.Add(new TroopModel(8, 9, "Dragon", point, 1800, 20, AppSettings.DragonTrain)); // 30x60 = 1800
                point.X += 107;
                Troops.Add(new TroopModel(9, 10, "Pekka", point, 2700, 25, AppSettings.PekkaTrain)); // 45x60 = 2700
                GotTotalSpaceTroops();
            }
        }

    }
}
