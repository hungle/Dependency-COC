﻿using System;
using System.Diagnostics;
using System.Drawing;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClashFunction;

namespace FastClash.BotFunction.MainScreen
{
    public class Mainscreen
    {
        /// <summary>
        /// Call COC app inside BS ,Only call after BS started
        /// </summary>
        public void StartCoc()
        {
            Process process = new Process();
            process.StartInfo.FileName = BlueStacksHelper.GetBsDirectory() + "HD-RunApp.exe";
            process.StartInfo.Arguments = "Android com.supercell.clashofclans com.supercell.clashofclans.GameApp";
            process.Start();
        }
        public void RestartBS()
        {
            Process process = new Process();
            process.StartInfo.FileName = BlueStacksHelper.GetBsDirectory() + "HD-Restart.exe";
            process.StartInfo.Arguments = "Android";
            process.Start();
        }


        public void FirstTimeBotRun(bool isShowLog = true)
        {
            //Check BS avaiable
            if (isShowLog)
                LogFunction.SetLog("Check BS avaiable");
            if (BlueStacksHelper.IsBlueStacksFound)
            {
                if (isShowLog)
                    LogFunction.SetLog("BS is running!!!");
            }
            else
            {
                if (isShowLog)
                    LogFunction.SetLog("Starting BS ...");
                while (!BlueStacksHelper.IsBlueStacksFound)
                {
                    BlueStacksHelper.RunBlueStacks();
                    int i = 0;
                    while (BlueStacksHelper.GetBlueStacksWindowHandle() == IntPtr.Zero)
                    {
                        SleepFunction.Sleep(500);
                        i++;
                        if (i > 30)
                        {
                            break;
                        }
                    }
                }
                if (BlueStacksHelper.IsBlueStacksFound)
                {
                    if (isShowLog)
                        LogFunction.SetLog("BS started!!!");
                }
            }
            BlueStacksHelper.ActivateBlueStacks();
            //Check COC started
            if (PixelTool.CheckColorAtPosition(284, 28, Color.FromArgb(0x41B1CD), 20, true))
            {
                if (isShowLog)
                    LogFunction.SetLog("Clash of Clan located!!!");
            }
            else
            {
                if (!CheckObstacles())
                {
                    if (isShowLog)
                        LogFunction.SetLog("Opening COC");
                    StartCoc();
                }

                while (!PixelTool.CheckColorAtPosition(284, 28, Color.FromArgb(0x41B1CD), 20, true))
                {
                    WaitMainScreen();
                }
                if (PixelTool.CheckColorAtPosition(284, 28, Color.FromArgb(0x41B1CD), 20, true))
                {
                    if (isShowLog)
                        LogFunction.SetLog("Mainscreen located!!!");
                }
            }

            //Zoomout
            while (!ZoomOut())
            {

            }
        }

        public void WaitMainScreen()
        {
            LogFunction.SetLog("Waiting for Main Screen");
            for (int i = 0; i < 1050; i++)
            {
                if (!PixelTool.CheckColorAtPosition(284, 28, Color.FromArgb(0x41B1CD), 20, true))
                {
                    SleepFunction.Sleep(200);
                    if (CheckObstacles())
                    {
                        i = 0;
                    }
                }
                else
                {
                    while (!ZoomOut())
                    {

                    }
                    return;
                }
            }
            LogFunction.SetLog("Unable to load Clash Of Clans, Restarting...");
            RestartBS();
            SleepFunction.Sleep(10000);
            int iTried = 0;
            bool isWhile = true;
            while (BlueStacksHelper.GetBlueStacksWindowHandle() == IntPtr.Zero && isWhile)
            {
                if (iTried == 9)
                {
                    RestartBS();
                    SleepFunction.Sleep(10000);
                }
                if (iTried > 20)
                {
                    LogFunction.SetLog("Unable to Restart BS...");
                    isWhile = false;
                }
                SleepFunction.Sleep(5000);
                iTried += 1;
            }
            if (BlueStacksHelper.GetBlueStacksWindowHandle() == IntPtr.Zero)
            {
                LogFunction.SetLog("Stuck trying to Restart BS...");
            }
        }

        public bool ZoomOut()
        {
            var black = Color.Black.ToArgb();
            ScreenCapture screenCapture = new ScreenCapture();
            screenCapture.SnapShot(0, 0, 860, 2);
            if (PixelTool.GetColor(1, 1).ToArgb() == black && PixelTool.GetColor(850, 1).ToArgb() == black)
            {
                LogFunction.SetLog("Zoomed Out!");
                return true;
            }
            SleepFunction.Sleep(200);
            LogFunction.SetLog("Zooming Out!");
            int i = 0;
            screenCapture.SnapShot(0, 0, 860, 2);
            lock (screenCapture.BitMap)
            {
                var hwnd = BlueStacksHelper.GetBlueStacksWindowHandle();
                while (PixelTool.GetColor(1, 1).ToArgb() != black && PixelTool.GetColor(850, 1).ToArgb() != black)
                {
                    var vkDown = 0x28;
                    Win32.SendMessage(hwnd, Win32.WmKeydown, (IntPtr)vkDown, (IntPtr)0);
                    ++i;
                    if (i > 20) return false; //Takes atmost 14 loops on my computer, using 20 just in case
                    screenCapture.SnapShot(0, 0, 860, 2);
                    Win32.SendMessage(hwnd, Win32.WmKeyup, (IntPtr)vkDown, (IntPtr)0);
                    SleepFunction.Sleep(100);
                }
            }
            if (PixelTool.GetColor(1, 1).ToArgb() == black && PixelTool.GetColor(850, 1).ToArgb() == black)
            {
                LogFunction.SetLog("Zoomed Out!");
                Other.ClickFunction.Click(GlobalVariables.pointAway);
                return true;
            }
            return false;
        }

        public bool IsProblemAffect(bool needCapture)
        {
            if (!PixelTool.CheckColorAtPosition(253, 395, Color.FromArgb(0x282828), 10, needCapture)) return false;
            else if (!PixelTool.CheckColorAtPosition(373, 395, Color.FromArgb(0x282828), 10, needCapture)) return false;
            else if (!PixelTool.CheckColorAtPosition(473, 395, Color.FromArgb(0x282828), 10, needCapture)) return false;
            else if (!PixelTool.CheckColorAtPosition(283, 395, Color.FromArgb(0x282828), 10, needCapture)) return false;
            else if (!PixelTool.CheckColorAtPosition(320, 395, Color.FromArgb(0x282828), 10, needCapture)) return false;
            else if (!PixelTool.CheckColorAtPosition(594, 395, Color.FromArgb(0x282828), 10, needCapture)) return false;
            else if (PixelTool.CheckColorAtPosition(823, 32, Color.FromArgb(0xF8FCFF), 10, needCapture)) return false;
            return true;
        }

        #region Obstacles
        public bool CheckObstacles()
        {
            if (CheckConnectDevice())
            {
                LogFunction.SetLog("Another Device has connected, waiting " + TimeSpan.FromSeconds(GlobalVariables.sTimeWakeUp).ToString(@"hh\:mm\:ss"));
                SleepFunction.Sleep(GlobalVariables.sTimeWakeUp * 1000);
                Other.ClickFunction.PureClick(GlobalVariables.aReloadButton);
                return true;
            }
            if (TakeABreak())
            {
                LogFunction.SetLog("Village must take a break, wait ...");
                SleepFunction.Sleep(120000); // 2 minute
                Other.ClickFunction.PureClick(GlobalVariables.aReloadButton);
                return true;
            }
            Point stoppedPoint = COCStopped();
            if (!stoppedPoint.IsEmpty)
            {
                LogFunction.SetLog("CoC Has Stopped Error .....");
                Other.ClickFunction.PureClick(250 + stoppedPoint.X, 327 + stoppedPoint.Y);
                Other.ClickFunction.PureClick(126, 700, 1, 500);
                StartCoc();
                return true;
            }
            if (IsOutOfSync())
            {
                Other.ClickFunction.PureClick(GlobalVariables.aReloadButton);
                SleepFunction.Sleep(5000);
                return true;
            }
            if (VillageAttacked())
            {
                Other.ClickFunction.PureClick(429, 493);
                return true;
            }
            if (MainScreenGrayed())
            {
                Other.ClickFunction.PureClick(1, 1);
                return true;
            }
            if (ChatTab())
            {
                Other.ClickFunction.PureClick(331, 330);
                return true;
            }

            if (Victory0Defeat())
            {
                Other.ClickFunction.PureClick(429, 519);
                return true;
            }
            if (BattleScreen())
            {
                Other.ClickFunction.PureClick(428, 544);
                return true;
            }
            if (ReturnHome())
            {
                Other.ClickFunction.PureClick(65, 613);
                return true;
            }

            if (OneTimeSetupStupid())
            {
                StartCoc();
                return true;
            }

            return false;
        }

        public bool CheckConnectDevice()
        {
            return (!IMGTool.FindImginArea("device", 237, 321, 293, 346).IsEmpty);
        }

        public bool TakeABreak()
        {
            return !IMGTool.FindImg("takeabreak").Result.IsEmpty;
        }

        public Point COCStopped()
        {
            return IMGTool.FindImginArea("CocStopped", 250, 328, 618, 402);
        }

        public bool IsOutOfSync()
        {
            return !PixelTool.PixelSearch(457, 300, 1, 30, Color.FromArgb(0x33B5E5), 10).IsEmpty;
        }

        public bool VillageAttacked()
        {
            return PixelTool.CheckColorAtPosition(235, 209, Color.FromArgb(0x9E3826), 20, true);
        }

        public bool MainScreenGrayed()
        {
            return PixelTool.CheckColorAtPosition(284, 28, Color.FromArgb(0x215B69), 20, true);
        }

        public bool ChatTab()
        {
            return PixelTool.CheckColorAtPosition(331, 330, Color.FromArgb(0xF0A03B), 20, true);
        }
        public bool Victory0Defeat()
        {
            return PixelTool.CheckColorAtPosition(429, 519, Color.FromArgb(0xB8E35F), 20, true);
        }
        public bool BattleScreen()
        {
            return PixelTool.CheckColorAtPosition(72, 530, Color.FromArgb(0xB8E35F), 20, true);
        }
        public bool ReturnHome()
        {
            //9,561   ---> 128,669
            return !IMGTool.FindImginArea("returnhome", 9, 561, 120, 108).IsEmpty;
        }
        public bool OneTimeSetupStupid()
        {
            //9,561   ---> 128,669
            return !IMGTool.FindImginArea("OneTimeSetUp", 180, 216, 181, 50).IsEmpty;
        }

        #endregion
    }
}
