﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.Misc.Enum;

namespace FastClash.BotFunction.ReadText
{

    public class OcrTrainText : ReadTextBase
    {
        protected string Army = "coc-army";
        protected string ArmyCamp = "coc-armycamp";
        protected string ArmyCampBarrack = "coc-army-barrack";
        protected string ArmyBarrackTroop = "coc-train";
        /// <summary>
        /// Gets the camp troop count in the form "trained#capacity" from the army overview page.
        /// </summary>
        /// <returns></returns>
        public string GetCampCountFraction()
        {
            try
            {
                return GetText(212, 144, 66, 14, Army);
            }
            catch (Exception ex)
            {
                return "0#0";
            }
        }

        /// <summary>
        /// Gets the queue info of the current barrack in the form "current#total".
        /// </summary>
        /// <returns></returns>
        public string GetBarrackQueueFraction()
        {
            Point pointStart = new Point(234, 147);
            Point pointEnd = new Point(280, 162);
            try
            {
                return GetText(pointStart.X, pointStart.Y, pointEnd.X - pointStart.X, pointEnd.Y - pointStart.Y, ArmyCampBarrack).Replace(" ", string.Empty);
                //ScreenCapture.BitMap.Save(@"D://test/armycamp2-" + position + ".jpg");
                //return cx;
            }
            catch (Exception ex)
            {
                return "0#0";
            }
        }


        /// <summary>
        /// Gets the number of a specific troop in camp.
        /// </summary>
        /// <param name="position">Position of the troop in the army overview.</param>
        /// <returns></returns>
        public int GetCampCount(int position)
        {
            //157,169   ---   198,182
            //204 -146 = 58 per troops   , space = 3px
            // start = 146  


            //ocr number: + 10 , width = 40 , height = 182 - 169 = 13

            // yTop = 169 , yBottom = 182
            int xStart = 146;
            int widthSlot = 62;
            int yTop = 169;
            int yBottom = 182;
            try
            {
                return int.Parse(GetText(xStart + widthSlot * position + 10, yTop, 40, yBottom - yTop, ArmyCamp).Replace(" ", string.Empty));
                //ScreenCapture.BitMap.Save(@"D://test/armycamp2-" + position + ".jpg");
                //return cx;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Get the training count of a troop in the current barracks page.
        /// </summary>
        /// <param name="column">Represents the x coordinate of the troop in the barracks page.</param>
        /// <param name="row">Represents the y coordinate of the troop in the barracks page.</param>
        public int GetTroopTrainingCount(int column,int row)
        {
            int yTop = 296;
            yTop += row * 108;
            string s = GetText(175 + 107 * column, yTop, 52, 16, ArmyBarrackTroop).Replace(" ", string.Empty);
            if(s == string.Empty)
            {
                return 0;
            }
            return int.Parse(s);
        }

        /// <summary>
        /// Gets the training count for all troops in the barrack.
        /// </summary>
        /// <param name="array">Array in which the 10 values will be stored</param>
        public void GetAllTroopsTrainingCount(out int[] array)
        {
            array = new int[10];
            for (int j = 0; j < 2; ++j)
            {
                for (int i = 0; i < 5; ++i)
                {
                    array[j*5 + i] = GetTroopTrainingCount(i, j);
                }
            }
        }
    }

}
