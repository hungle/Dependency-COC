﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.BotFunction.Pixel;

namespace FastClash.BotFunction.ReadText
{

    public class OcrEnemyText : ReadTextBase
    {
        protected string ArmyAtk = "coc-attack-troop";
        protected string ArmyAtkLarge = "coc-atk-large";
        protected string Enemy_Gold = "coc-v-g";
        protected string Enemy_Elixir = "coc-v-e";
        protected string Enemy_Dark = "coc-v-de";
        protected string Enemy_Trophie = "coc-v-t";
        public long GetGolds()
        {
            try
            {
                return long.Parse(GetText(48, 69, 90, 16, Enemy_Gold).Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public long GetElixirs()
        {
            try
            {
                return long.Parse(GetText(48, 69 + 29, 90, 16, Enemy_Elixir).Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public long GetDarks()
        {
            try
            {
                if (PixelTool.CheckColorAtPosition(30, 142, Color.FromArgb(0x07010D), 10, true))
                {
                    return long.Parse(GetText(48, 69 + 57, 75, 16, Enemy_Dark).Replace(" ", string.Empty));
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public long GetTrophies()
        {
            try
            {
                if (PixelTool.CheckColorAtPosition(30, 142, Color.FromArgb(0x07010D), 10, true))
                {
                    return long.Parse(GetText(48, 69 + 99, 75, 16, Enemy_Trophie).Replace(" ", string.Empty));
                }
                return long.Parse(GetText(48, 138, 75, 16, Enemy_Trophie).Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int GetAttackTroop(int position, int positionChoosed, int xStartAtk)
        {
            //int xStart = 2;
            //int xStart = 33;
            int widthSlot = 73;
            int yTop = 582;
            int yBottom = 597;
            try
            {
                if (position == positionChoosed)
                {
                    yTop = 577;
                    yBottom = 594;
                    string testHere = GetText(xStartAtk + widthSlot * position + 10, yTop, 50, yBottom - yTop, ArmyAtkLarge);
                    return int.Parse(testHere.Replace(" ", string.Empty));
                }
                string test = GetText(xStartAtk + widthSlot * position + 15, yTop, 45, yBottom - yTop, ArmyAtk);
                return int.Parse(test.Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

}
