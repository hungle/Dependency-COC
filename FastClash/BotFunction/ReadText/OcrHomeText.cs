﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.BotFunction.Pixel;

namespace FastClash.BotFunction.ReadText
{

    public class OcrHomeText : ReadTextBase
    {
        protected string Homebase = "coc-ms";
        protected string Builder = "coc-Builders";
        public string GetGolds()
        {
            try
            {
                return GetText(710, 23, 95, 16, Homebase);
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        public string GetElixirs()
        {
            try
            {
                return GetText(710, 74, 95, 16, Homebase);
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        public string GetDarks()
        {
            try
            {
                if (PixelTool.CheckColorAtPosition(812, 141, Color.FromArgb(0x000000), 10, true))
                {
                    return GetText(731, 123, 95, 16, Homebase);
                }
                return "0";
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        public string GetGems()
        {
            try
            {
                if (PixelTool.CheckColorAtPosition(812, 141, Color.FromArgb(0x000000), 10, true))
                {
                    return GetText(740, 171, 95, 16, Homebase).Replace(" ", string.Empty);
                }
                return GetText(719, 123, 95, 16, Homebase);
            }
            catch (Exception)
            {
                return "0";
            }
        }
        public string GetBuilderFree()
        {
            try
            {
                return GetText(324, 21, 40, 18, Builder).Replace('#', '/');
            }
            catch (Exception)
            {
                return "0/0";
            }
        }
        public string GetTrophies()
        {
            try
            {
                return GetText(65, 74, 50, 16, Homebase);
            }
            catch (Exception)
            {
                return "0";
            }
        }
    }
}
