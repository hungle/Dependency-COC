﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastClash.BotFunction.ReadText
{
    public class OcrClanText : ReadTextBase
    {
        protected string ClanTextLatin = "coc-latinA";
        public string GetClanText(int y)
        {
            try
            {
                return GetText(30, y, 200, 18, ClanTextLatin);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
