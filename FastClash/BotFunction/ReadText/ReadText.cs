﻿using System;
using System.Drawing;
using FastClash.BotFunction.Pixel;
using FastClash.Misc.IFace;
using FastClash.OCR;
using FastClash.ViewModels;
using FastClashFunction;

namespace FastClash.BotFunction.ReadText
{
    public abstract class ReadTextBase : IReadbase
    {
        private readonly Ocr ocr = new Ocr();
        public virtual string GetText(int left, int top, int width, int height, string sourceString)
        {
            try
            {
                ScreenCapture screenCapture = new ScreenCapture(false);
                screenCapture.SnapShot(left, top, width, height);
                if (GlobalVariables.isDebug)
                {
                    screenCapture.BitMap.Save(GlobalVariables.ocrDebug + DateTime.Now.Ticks + ".jpg");
                }
                FastBitmap fb = new FastBitmap(screenCapture.BitMap);
                fb.LockBitmap();
                string textHere = ocr.recognize(fb, sourceString);
                fb.UnlockBitmap();
                fb.Dispose();
                screenCapture.BitMap.Dispose();
                return textHere;

            }
            catch (Exception ex)
            {
                    
                return "";
            }
        }
    }

}
