﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Threading.Tasks;
using FastClash.BotFunction.Attack;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClash.BotFunction.ReadText;
using FastClash.BotFunction.Search;
using FastClash.BotFunction.Train;
using FastClash.ViewModels;

namespace FastClash.BotFunction.Village
{
    public enum TrainType
    {
        Custom, Barrack
    }

    public class Village : VillageResource
    {
        private List<Point> _listElixirPoint;
        private List<Point> _listMinePoint;
        private List<Point> _listDarkPoint;
        private Camp _camp;
        private Point _townHall;
        private Point _barrack;

        public Village(Camp camp)
        {
            _camp = camp;
        }

        public List<Point> ListElixirPoint
        {
            get { return _listElixirPoint; }
        }
        public List<Point> ListMinePoint
        {
            get { return _listMinePoint; }
        }
        public List<Point> ListDarkPoint
        {
            get { return _listDarkPoint; }
        }
        public Point TownHall
        {
            get { return _townHall; }
        }

        public void DetectBuilding()
        {
            ScreenCapture screenCapture = new ScreenCapture();
            screenCapture.SnapShot();
            if (_townHall.IsEmpty)
            {
                LogFunction.SetLog("Detecting Building");
                _townHall = IMGTool.GetPostionTownHall(screenCapture.BitMap, 0.6);
                _barrack = IMGTool.GetPostionBarrack(screenCapture.BitMap, 0.7);
                _listElixirPoint = IMGTool.GetElixirEx(screenCapture.BitMap, 0.6);
                _listMinePoint = IMGTool.GetMineEx(screenCapture.BitMap, 0.6);
                _listDarkPoint = IMGTool.GetDarkEx(screenCapture.BitMap, 0.6);
                LogFunction.SetLog("Finish Detect Building");
            }
        }

        public void GotLootRemaining()
        {
            SleepFunction.Sleep(200);
            ScreenCapture screenCapture = new ScreenCapture();
            screenCapture.SnapShot();
            Point pLootRemaining = IMGTool.FindImg(screenCapture.BitMap, "GoldPop").Result;
            int i = 0;
            while (!pLootRemaining.IsEmpty && i < 3)
            {
                ClickFunction.Click(pLootRemaining);
                SleepFunction.Sleep(200);
                screenCapture.SnapShot();
                pLootRemaining = IMGTool.FindImg(screenCapture.BitMap, "GoldPop").Result;
                i++;
            }

            pLootRemaining = IMGTool.FindImg(screenCapture.BitMap, "ElixirPop").Result;
            i = 0;
            while (!pLootRemaining.IsEmpty && i < 3)
            {
                ClickFunction.Click(pLootRemaining);
                SleepFunction.Sleep(200);
                screenCapture.SnapShot();
                pLootRemaining = IMGTool.FindImg(screenCapture.BitMap, "ElixirPop").Result;
                i++;
            }

            pLootRemaining = IMGTool.FindImg(screenCapture.BitMap, "DarkPop").Result;
            i = 0;
            while (!pLootRemaining.IsEmpty && i < 3)
            {
                ClickFunction.Click(pLootRemaining);
                SleepFunction.Sleep(200);
                screenCapture.SnapShot();
                pLootRemaining = IMGTool.FindImg(screenCapture.BitMap, "DarkPop").Result;
                i++;
            }
            screenCapture.BitMap.Dispose();
        }


        public void ReportBuilding()
        {
            LogFunction.SetLog("Townhall Position : [" + _townHall.X + "," + _townHall.Y + "]");
            LogFunction.SetLog("Barrack Position : [" + _barrack.X + "," + _barrack.Y + "]");
            LogFunction.SetLog("Total Elixir extractor : " + _listElixirPoint.Count);
            LogFunction.SetLog("Total Mine extractor : " + _listMinePoint.Count);
            LogFunction.SetLog("Total Dark extractor : " + _listDarkPoint.Count);
            ClickFunction.Click(_barrack);
            SleepFunction.Sleep(2000);
            ClickFunction.Click(GlobalVariables.pointAway);
            SleepFunction.Sleep(1000);
        }
        public void GetLootVillageInfo()
        {
            SleepFunction.Sleep(1000);
            OcrHomeText ocrHomeText = new OcrHomeText();
            Instance.Gold = ocrHomeText.GetGolds();
            Instance.Elixir = ocrHomeText.GetElixirs();
            Instance.DarkElixir = ocrHomeText.GetDarks();
            Instance.Trophies = ocrHomeText.GetTrophies();
            Instance.Gems = ocrHomeText.GetGems();
            Instance.Builders = ocrHomeText.GetBuilderFree();
        }

        public void ReportLootVillage()
        {
            SleepFunction.Sleep(1000);
            string reporttext = "Golds : " + Gold;
            reporttext += " - Elixirs : " + Elixir;
            if (DarkElixir != "0")
            {
                reporttext += " - Darks : " + DarkElixir;
            }
            LogFunction.SetLog(reporttext);
            reporttext = "Trophies : " + Trophies;
            LogFunction.SetLog(reporttext);
            reporttext = "Gems : " + Gems;
            LogFunction.SetLog(reporttext);
            reporttext = "Builder : " + Builders;
            LogFunction.SetLog(reporttext);
            SleepFunction.Sleep(1000);
        }


        private void GetCampInfo(bool isNeedOpenCamp)
        {
            if (isNeedOpenCamp)
            {
                _camp.OpenCamp();
            }
            _camp.GetCampInfo();
            Instance.CurrentArmy = _camp.CurrentArmy;
            string reporttext = "CurrentArmy : " + _camp.CurrentArmy;
            LogFunction.SetLog(reporttext);
            Instance.TotalArmy = _camp.TotalArmy;
            reporttext = "TotalArmy : " + _camp.TotalArmy;
            LogFunction.SetLog(reporttext);
        }


        public void CollectResource(int time = 1)
        {
            LogFunction.SetLog("Collecting your resource ...");
            for (int j = 0; j < time; j++)
            {
                if (_listElixirPoint.Count > 0)
                {
                    for (int i = 0; i < _listElixirPoint.Count; i++)
                    {
                        Other.ClickFunction.Click(_listElixirPoint[i].X, _listElixirPoint[i].Y, 1, 50);
                    }
                }
                if (_listMinePoint.Count > 0)
                {
                    for (int i = 0; i < _listMinePoint.Count; i++)
                    {
                        Other.ClickFunction.Click(_listMinePoint[i].X, _listMinePoint[i].Y, 1, 50);
                    }
                }
                if (_listDarkPoint.Count > 0)
                {
                    for (int i = 0; i < _listDarkPoint.Count; i++)
                    {
                        Other.ClickFunction.Click(_listDarkPoint[i].X, _listDarkPoint[i].Y, 1, 50);
                    }
                }
            }
            LogFunction.SetLog("Collected your resource!!! ");
        }


        public void CalcuateAtkBuildingLevel(bool isNeedOpenCamp)
        {
            if (isNeedOpenCamp)
            {
                _camp.OpenCamp();
            }
            _camp.CaculateAtkBuildingLevel();
        }

        public void CalcuateBoostedBuildings(bool isNeedOpenCamp)
        {
            if (isNeedOpenCamp)
            {
                _camp.OpenCamp();
            }
            _camp.CalculateBoostedBuildings();
        }

        public void Train(TrainType trainType, Camp.TrainMode tm, ObservableCollection<TroopModel> troops, bool isNeedOpenCamp)
        {
            if (isNeedOpenCamp)
            {
                _camp.OpenCamp();
            }
            if (trainType == TrainType.Custom)
            {
                _camp.CustomTrain(troops, tm);
            }
        }

        public void ReArm(Point THPoint)
        {
            ClickFunction.Click(GlobalVariables.pointAway);
            SleepFunction.Sleep(1000);
            ClickFunction.Click(THPoint.X, THPoint.Y);
            SleepFunction.Sleep(1000);
            //242,560   --->     632,634

            ScreenCapture screenCapture = new ScreenCapture(false);
            screenCapture.SnapShot(242, 560, 400, 74);
            Point trapPoint = IMGTool.FindImg(screenCapture.BitMap, (Bitmap)Properties.Resources.ResourceManager.GetObject("traps"));
            Point xbowPoint = IMGTool.FindImg(screenCapture.BitMap, (Bitmap)Properties.Resources.ResourceManager.GetObject("xbows"));
            Point infernoPoint = IMGTool.FindImg(screenCapture.BitMap, (Bitmap)Properties.Resources.ResourceManager.GetObject("inferno"));


            //412,368    ---> 600,432
            if (!trapPoint.IsEmpty)
            {
                Other.ClickFunction.Click(trapPoint.X + 242, trapPoint.Y + 560);
                screenCapture.SnapShot(412, 368, 188, 64);
                while (IMGTool.FindImg(screenCapture.BitMap, (Bitmap)Properties.Resources.ResourceManager.GetObject("okey")).IsEmpty)
                {
                    SleepFunction.Sleep(100);
                    screenCapture.SnapShot(412, 368, 188, 64);
                }
                Other.ClickFunction.Click(515, 400);
                LogFunction.SetLog("Traps armed");
                SleepFunction.Sleep(500);
            }


            if (!xbowPoint.IsEmpty)
            {
                Other.ClickFunction.Click(xbowPoint.X + 242, xbowPoint.Y + 560);

                screenCapture.SnapShot(412, 368, 188, 64);
                while (IMGTool.FindImg(screenCapture.BitMap, (Bitmap)Properties.Resources.ResourceManager.GetObject("okey")).IsEmpty)
                {
                    SleepFunction.Sleep(100);
                    screenCapture.SnapShot(412, 368, 188, 64);
                }
                Other.ClickFunction.Click(515, 400);
                LogFunction.SetLog("Xbows armed");
                SleepFunction.Sleep(500);
            }

            if (!infernoPoint.IsEmpty)
            {
                Other.ClickFunction.Click(infernoPoint.X + 242, infernoPoint.Y + 560);

                screenCapture.SnapShot(412, 368, 188, 64);
                while (IMGTool.FindImg(screenCapture.BitMap, (Bitmap)Properties.Resources.ResourceManager.GetObject("okey")).IsEmpty)
                {
                    SleepFunction.Sleep(100);
                    screenCapture.SnapShot(412, 368, 188, 64);
                }
                Other.ClickFunction.Click(515, 400);
                LogFunction.SetLog("InfernoPoint armed");
                SleepFunction.Sleep(500);
            }
            Other.ClickFunction.Click(GlobalVariables.pointAway);
            screenCapture.BitMap.Dispose();
        }

        public bool IsCampFull(bool isNeedOpenCamp)
        {
            GetCampInfo(isNeedOpenCamp);
            return _camp.IsFullCamp();
        }

        public void DropTrophies()
        {
            if (Instance.IsNeedRangeTrop && Int16.Parse(Instance.Trophies) > Instance.MaxTrop
                && Instance.MaxTrop > 0 && Instance.MinTrop > 0)
            {
                while (Int16.Parse(Instance.Trophies) > Instance.MinTrop)
                {
                    SearchEngine searchEngine = new SearchEngine();
                    searchEngine.FirstEnemyBase();
                    AttackSmart attack = new AttackSmart();
                    attack.DropTrophies();
                    Instance.Trophies = new OcrHomeText().GetTrophies();
                }
            }
        }

    }
}
