﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClash.BotFunction.ReadText;
using FastClash.Misc.Enum;

namespace FastClash.BotFunction.Donate
{
    public static class DonateFunctions
    {
        public static void OpenDonateTab()
        {
            ClickFunction.ClickDonate();
        }

        /// <summary>
        /// This function will return true if have any news on clan chat. 
        /// </summary>
        public static bool IsHaveNews()
        {
            // Detects red number on arrow
            return PixelTool.CheckColorAtPosition(23, 322, Color.FromArgb(0xF7EEE7), 20, true, false);
        }

        /// <summary>
        /// This function donates troops using 3 parameters
        /// </summary>
        /// <param name="troopKind">Troop to donate</param> 
        /// <param name="y">Y coordinate of donate button</param>
        /// <param name="number">Number of troops to donate. Use -1 for donating till maximum possible.</param>
        public static void DonateTroop(TroopKindEnum troopKind, int y, int number = -1)
        {
            // Data figured using screenshot inspection. 
            //
            // Donate Button Point - Barbarian Point
            // 115,179 - 262,89
            // 115,332 - 262,242
            // 115,583 - 262,493
            // Conclusion : x+= 147 y-= 90 and x is always 115+147 = 262
            //
            // Column shift : x+= 81 followed by 81,80,81,81,80 - 7 Troops in a row , totally 16 troops
            // => if iteration % 3 = 0 , x -= 1 
            // Row shift : y+= 98 followed by y+= 97 
            // => 1 extra pixel for 1st to 2nd column because coc draws an extra shadow on the top of each troop for 2nd column
            int row = (int)troopKind / 7;
            int column = (int)troopKind % 7;

            y -= 90;
            y += 98 * row;
            if (column == 2)
                y -= 1;
            int x = 262 + 81 * row;
            if ((row + 1) % 3 == 0)
                x -= 1;

            Point p = new Point(x, y);

            int iCount = 0;
            while (!PixelTool.CheckColorAtPoint(p, Color.FromArgb(0xADADAD), 20, true, false) && number != 0 && iCount < 20)
            {
                ClickFunction.Click(p, 1, 50);
                --number;
                ++iCount;
            }
        }
    }
}
