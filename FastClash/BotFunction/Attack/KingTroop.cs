﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.BotFunction.Other;
using FastClash.Misc;
using FastClash.Misc.Enum;
using FastClash.Misc.IFace;

namespace FastClash.BotFunction.Attack
{
    class KingTroop : AttackTroop
    {
        public Point HealthBarPoint { get; set; }
        public Color ColorHealthBar { get; set; }
        public void Drop(Point pointDrop, int _delay)
        {
            if (Num == 0) return;
            ClickFunction.Click(pointDrop, 1 , _delay);
        }
        public void DidAPower()
        {
            Select();
        }
        public KingTroop(TroopKindEnum troopKind, Point point) : base(troopKind,1,point)
        {
        }
    }
}
