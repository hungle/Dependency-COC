﻿using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClash.BotFunction.Search;
using FastClash.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.Misc;
using FastClash.Misc.Enum;
using FastClash.Misc.IFace;

namespace FastClash.BotFunction.Attack
{
    class Deployment
    {
        private List<AttackTroop> ListAttackTroops { get; set; }
        private List<KingTroop> ListKingTroops { get; set; }
        private List<SpellTroop> ListSpellTroops { get; set; }

        private List<HashSet<Point>> ListRedPoints { get; set; }
        private AttackViewModel AttackViewModel { get; set; }
        private bool _isEndOfBattle = false;

        public ITroop AttackTroopSelected { get; set; }

        public Deployment(List<AttackTroop> listAttackTroops, List<KingTroop> listKingTroops,
            List<SpellTroop> listSpellTroops, List<HashSet<Point>> listRedPoints, AttackViewModel attackViewModel)
        {
            ListAttackTroops = listAttackTroops;
            ListKingTroops = listKingTroops;
            ListSpellTroops = listSpellTroops;
            ListRedPoints = listRedPoints;
            AttackViewModel = attackViewModel;
        }

        public void DropTroops(TroopKindEnum tropkind, int numOfSide, int totalWaze, int mSecond, bool isGroup = false)
        {
            bool isHaveTroop = false;
            AttackTroop attackTroop = new AttackTroop();
            foreach (AttackTroop att in ListAttackTroops)
            {
                if (att.GetName() == tropkind.ToString())
                {
                    attackTroop = att;
                    isHaveTroop = true;
                    break;
                }
            }
            if (!isHaveTroop) return;
            DropTroops(attackTroop, numOfSide, totalWaze, mSecond, isGroup);
        }

        public void DropTroops(AttackTroop attackTroop, int totalSide, int totalWaze, int mSecond, bool isGroup = false)
        {
            if (totalSide < 1) return;
            int numTroopNeedDrop = (int)Math.Ceiling((decimal)attackTroop.Num / totalSide / totalWaze);
            if (attackTroop.Num < totalSide)
            {
                numTroopNeedDrop = attackTroop.Num;
            }
            for (int i = 0; i < totalSide; i++)
            {
                if (isGroup)
                {
                    SelectTroop(attackTroop);
                    Point cPoint = CenterPoint(ListRedPoints[i]);
                    if (cPoint.IsEmpty)
                    {
                        if (i == 0)  // top left
                        {
                            cPoint = new Point(250, 166);
                        }
                        else if (i == 1)  // bottom right
                        {
                            cPoint = new Point(618, 457);
                        }
                        else if (i == 2)  // bottom left
                        {
                            cPoint = new Point(237, 458);
                        }
                        else if (i == 3)  // top right
                        {
                            cPoint = new Point(610, 170);
                        }
                    }
                    attackTroop.Drop(numTroopNeedDrop, cPoint, mSecond);
                }
                else
                {
                    HashSet<Point> PixelListHere = ListRedPoints[i];
                    int iCount = 0;
                    if (PixelListHere.Count > numTroopNeedDrop)
                    {
                        int troopPerPoint = 1;
                        //HashSet<Point> PixelListAccurate = ListPointsNearPointTarget(PixelListHere, 3, numTroopNeedDrop);
                        HashSet<Point> PixelListAccurate = GetListPointsTarget(PixelListHere, 10, (int)Math.Round((double)numTroopNeedDrop / troopPerPoint));

                        SelectTroop(attackTroop);
                        foreach (Point drPoint in PixelListAccurate)
                        {
                            attackTroop.Drop(troopPerPoint, drPoint, mSecond);
                            iCount++;
                            if (iCount >= numTroopNeedDrop)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        SelectTroop(attackTroop);
                        int troopPerSlot = (int)Math.Round((decimal)numTroopNeedDrop / PixelListHere.Count);
                        iCount = 0;
                        foreach (Point drPoint in PixelListHere)
                        {
                            attackTroop.Drop(troopPerSlot, drPoint, mSecond);
                            iCount++;
                            if (iCount >= numTroopNeedDrop)
                            {
                                break;
                            }
                        }
                    }
                    //distant troop is 3px
                    //caculate how many troop can drop
                    //caculate how many troop drop per pixel
                }

            }
        }

        private void SelectTroop(ITroop att)
        {
            AttackTroopSelected = att;
            if (att == null)
            {
                return;
            }
            att.Select();
        }



        private HashSet<Point> GetListPointsTarget(HashSet<Point> hashSetPoint, int maxPixelDistance, int numberPoint = 5)
        {
            if (hashSetPoint.Count < numberPoint) return hashSetPoint;
            HashSet<Point> listPointsTarget = new HashSet<Point>();
            int xMin = hashSetPoint.Min(o => o.X);
            int xMax = hashSetPoint.Max(o => o.X);
            double distance = xMax - xMin;
            double distanceEach = distance / (numberPoint - 1);
            for (int i = 0; i < numberPoint; i++)
            {
                foreach (Point point in hashSetPoint)
                {
                    if (GetPointNear(point, xMin + distanceEach * i, maxPixelDistance))
                    {
                        if (!listPointsTarget.Contains(point))
                        {
                            listPointsTarget.Add(point);
                            break;
                        }
                    }
                }
            }
            return listPointsTarget;
        }

        private bool GetPointNear(Point point, double below, int maxPixelDistance)
        {
            int pixelDistance = 1;
            while (pixelDistance <= maxPixelDistance)
            {
                if (point.X >= below && point.X < below + maxPixelDistance)
                {
                    return true;
                }
                pixelDistance++;
            }
            return false;
        }

        private Point CenterPoint(HashSet<Point> hashSetPoint)
        {
            int xMax = hashSetPoint.Max(o => o.X);
            int xMin = hashSetPoint.Min(o => o.X);
            int xCenter = (xMax + xMin) / 2;
            int distance = 3;
            while (distance < 10)
            {
                foreach (Point point in hashSetPoint)
                {
                    if (Math.Abs(point.X - xCenter) < distance)
                    {
                        return point;
                    }
                }
                distance++;
            }
            return new Point();
        }

        private Point getRandomPoint(HashSet<Point> listPixel)
        {
            if (listPixel.Count == 0)
            {
                return new Point(188, 433);
            }
            Random randomizer = new Random();
            Point[] asArray = listPixel.ToArray();
            return asArray[randomizer.Next(asArray.Length)];
        }

        public void DropKing(int numOfSide)
        {
            if (ListKingTroops.Count > 0)
            {
                foreach (var atk in ListKingTroops)
                {
                    int i = new Random().Next(0, numOfSide);
                    Point pointForDrop = getRandomPoint(ListRedPoints[i]);
                    if ((AttackViewModel.IsUseKing && atk.GetName() == TroopKindEnum.King.ToString())
                        || (AttackViewModel.IsUseQueen && atk.GetName() == TroopKindEnum.Queen.ToString()))
                    {
                        SelectTroop(atk);
                        atk.Drop(pointForDrop, 1000);
                        Task.Factory.StartNew(() => DidAPower(atk));
                    }
                }
                //tDropKing = new Thread(() => DidAPower(kingList));
                //tDropKing.Start();
            }
        }
        public void DropCC(AttackTroop ccTroop, int numOfSide)
        {
            if (ccTroop!= null)
            {
                int i = new Random().Next(0, numOfSide);
                Point pointForDrop = getRandomPoint(ListRedPoints[i]);
                SelectTroop(ccTroop);
                ccTroop.Drop(1, pointForDrop, 1000);
            }
        }

        public void DidAPower(KingTroop kingTroop)
        {
            int iCount = 0;
            int timeWeak = 0;
            bool isHeroWeak = false;
            while (!isHeroWeak && !_isEndOfBattle && iCount < 1000)
            {
                isHeroWeak = IsHeroWeak(kingTroop);
                if (isHeroWeak)
                {
                    timeWeak++;
                }
                else
                {
                    timeWeak = 0;
                }
                if (timeWeak < 2)
                {
                    isHeroWeak = false;
                }
                SleepFunction.Sleep(200);
                iCount++;
            }
            kingTroop.DidAPower();

        }

        public void GoldElixirChange(EnemyVillage enymeVillage)
        {
            while (enymeVillage.GoldElixirChange())
            {
                for (int i = 0; i < AttackViewModel.SecondNoLootChange; i++)
                {
                    if (AttackViewModel.IsResouceBelow)
                    {
                        if (enymeVillage.Gold <= AttackViewModel.GoldBelow && enymeVillage.Elixir <= AttackViewModel.ElixirBelow && enymeVillage.Dark <= AttackViewModel.DarkBelow)
                        {

                            _isEndOfBattle = true;
                            return;
                        }

                    }
                    if (enymeVillage.IsEndBattle())
                    {
                        _isEndOfBattle = true;
                        return;
                    }
                    SleepFunction.Sleep(1000);
                }
            }

            _isEndOfBattle = true;
            LogFunction.SetLog("-- No Loots Change --");
        }


        public bool IsHeroWeak(KingTroop atk)
        {
            if (atk.HealthBarPoint == Point.Empty)
            {
                int halfHealBar = 45;
                int position = GetTroopPosition(atk.Point);
                int xHealthBar = GetXPosOfArmySlot(position, halfHealBar);
                Point healthBar = new Point(xHealthBar, 572);
                atk.HealthBarPoint = healthBar;
            }
            if (atk.ColorHealthBar == Color.Empty)
            {
                SleepFunction.Sleep(1000);
                atk.ColorHealthBar = PixelTool.GetColor(atk.HealthBarPoint.X, atk.HealthBarPoint.Y, true, false);
            }
            return !PixelTool.CheckColorAtPoint(atk.HealthBarPoint, atk.ColorHealthBar, 50, true, false);

        }

        private int GetTroopPosition(Point pointTroop)
        {
            if (pointTroop.IsEmpty)
            {
                return 0;
            }
            return (int)Math.Floor((decimal)(pointTroop.X - 2) / 73);
        }

        private int GetXPosOfArmySlot(int position, int xOffset)
        {
            int xStart = 2;
            int widthSlot = 73;

            if (GetTotalTroopKind() > 11)
            {
                return xOffset + xStart + (position * widthSlot);
            }
            xStart = 33;
            return xOffset + xStart + (position * widthSlot);
        }
        private int GetTotalTroopKind()
        {
            return ListAttackTroops.Count + ListKingTroops.Count + ListSpellTroops.Count;
        }

    }
}
