﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.BotFunction.Other;
using FastClash.Misc;
using FastClash.Misc.Enum;
using FastClash.Misc.IFace;

namespace FastClash.BotFunction.Attack
{
    class SpellTroop : ITroop
    {
        public TroopKindEnum TropKind { get; set; }

        public int Num { get; set; }

        public Point Point { get; set; }

        public string GetName()
        {
            return TropKind.ToString();
        }

        public void Select()
        {
            ClickFunction.Click(Point);
            SleepFunction.Sleep(50);
        }
        public void Drop(Point pointDrop, int _delay)
        {
            if (Num == 0) return;
            ClickFunction.Click(pointDrop, 1, _delay);
        }
        public SpellTroop(TroopKindEnum troopKind, int num, Point point)
        {
            TropKind = troopKind;
            Num = num;
            Point = point;
        }
    }
}
