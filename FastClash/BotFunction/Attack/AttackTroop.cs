﻿using System.Drawing;
using FastClash.BotFunction.Other;
using FastClash.Misc;
using FastClash.Misc.Enum;
using FastClash.Misc.IFace;

namespace FastClash.BotFunction.Attack
{

    class AttackTroop : ITroop
    {
        public TroopKindEnum TropKind { get; set; }

        public int Num { get; set; }

        public Point Point { get; set; }

        public string GetName()
        {
            return TropKind.ToString();
        }

        public void Select()
        {
            ClickFunction.Click(Point);
            SleepFunction.Sleep(50);
        }

        public void Drop(int number, Point pointDrop, int _delay)
        {
            if (Num == 0) return;
            ClickFunction.Click(pointDrop, number, _delay);
        }

        public AttackTroop() { }
        public AttackTroop(TroopKindEnum troopKind, int num, Point point)
        {
            TropKind = troopKind;
            Num = num;
            Point = point;
        }
    }
}
