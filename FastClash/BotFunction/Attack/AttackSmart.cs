﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using FastClash.BotFunction.MainScreen;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClash.BotFunction.ReadText;
using FastClash.BotFunction.Search;
using FastClash.Misc;
using FastClash.Misc.Enum;
using FastClash.Misc.IFace;
using FastClash.ViewModels;
using FastClashFunction;
using ImgProcessing.CV.PInvoke;

namespace FastClash.BotFunction.Attack
{
    class AttackSmart
    {
        private EnemyVillage _enemyVillage;
        private List<AttackTroop> _listAttackTroops = new List<AttackTroop>();
        private List<KingTroop> _listKingTroops = new List<KingTroop>();
        private List<SpellTroop> _listSpellTroops = new List<SpellTroop>();
        private AttackTroop _ccTroop;
        private AttackViewModel _attackViewModel;

        public AttackSmart(EnemyVillage enemyVillage, AttackViewModel attackViewModel)
        {
            _enemyVillage = enemyVillage;
            _attackViewModel = attackViewModel;
        }
        public AttackSmart()
        {
        }

        public void DropTrophies()
        {
            Win32.EmptyWorkingSet(BlueStacksHelper.GetBlueStacksWindowHandle());
            IdentityTroopAttack(null, false);
            bool isHaveKing = false;
            foreach (var attackTroop in _listAttackTroops)
            {
                if (attackTroop.GetName() == TroopKindEnum.King.ToString() || attackTroop.GetName() == TroopKindEnum.Queen.ToString())
                {
                    attackTroop.Select();
                    attackTroop.Drop(1, new Point(22, 306), 10);
                    isHaveKing = true;
                    break;
                }
            }
            if (!isHaveKing)
            {
                foreach (var attackTroop in _listAttackTroops)
                {
                    if (attackTroop.GetName() == TroopKindEnum.Archer.ToString() || attackTroop.GetName() == TroopKindEnum.Barbarian.ToString())
                    {
                        attackTroop.Select();
                        attackTroop.Drop(1, new Point(22, 306), 10);
                        break;
                    }
                }

            }
            ReturnHome();
        }
        public void Attack()
        {
            Win32.EmptyWorkingSet(BlueStacksHelper.GetBlueStacksWindowHandle());
            List<HashSet<Point>> listPoints = new List<HashSet<Point>>();
            if (_attackViewModel.IsRedline)
            {
                listPoints = _enemyVillage.GetRedArea(1, 5, 40);
            }
            else
            {
                listPoints = _enemyVillage.GetLineArea();
            }
            IdentityTroopAttack(null);
            int side = _attackViewModel.NumSide;
            if (side > 4) side = 4;
            if (side < 1) side = 1;
            Deployment deployment;

            int delayUnit = 15;
            int delayWave = 250;

            if (_attackViewModel.IsRandomDelay)
            {
                delayUnit = delayUnit * new Random().Next(1, 10);
                delayWave = delayWave * new Random().Next(1, 10);
            }
            else
            {
                delayUnit = delayUnit * _attackViewModel.UnitDelay;
                delayWave = delayWave * _attackViewModel.WaveDelay;
            }


            deployment = new Deployment(_listAttackTroops, _listKingTroops, _listSpellTroops, listPoints, _attackViewModel);


            deployment.DropTroops(TroopKindEnum.Giant, side, 1, delayUnit * 2, true);
            deployment.DropTroops(TroopKindEnum.Giant, side, 1, 10, true);

            deployment.DropTroops(TroopKindEnum.WallBreaker, side, 1, delayUnit, true);
            deployment.DropTroops(TroopKindEnum.WallBreaker, side, 1, 10, true);

            deployment.DropTroops(TroopKindEnum.Barbarian, side, 2, delayUnit);


            deployment.DropTroops(TroopKindEnum.Archer, side, 2, delayUnit);

            SleepFunction.Sleep(delayWave);

            deployment.DropTroops(TroopKindEnum.Barbarian, side, 2, delayUnit);

            deployment.DropTroops(TroopKindEnum.Goblin, side, 1, delayUnit);
            deployment.DropTroops(TroopKindEnum.Goblin, side, 1, delayUnit);

            deployment.DropTroops(TroopKindEnum.Archer, side, 2, delayUnit);

            deployment.DropKing(4);

            if (_attackViewModel.IsUseCC)
            {
                deployment.DropCC(_ccTroop, side);
            }
            int iCount = 0;
            while (IsTroopRemain() && iCount < 5)
            {
                IdentityTroopNumber(_listAttackTroops, deployment.AttackTroopSelected, false);

                foreach (AttackTroop atk in _listAttackTroops.Where(atk => atk.Num>0))
                {
                    deployment.DropTroops(atk, 4, 1, 50, true);
                }

                iCount++;
                IdentityTroopNumber(_listAttackTroops, deployment.AttackTroopSelected, false);
                SleepFunction.Sleep(5000);
            }

            deployment.GoldElixirChange(_enemyVillage);
            SleepFunction.Sleep(500);
            ReturnHome();
        }


        private bool IsTroopRemain()
        {
            return _listAttackTroops.Any(troop => troop.Num > 0);
        }


        private void ReportTroops(IEnumerable<ITroop> listAttackTroops)
        {
            foreach (ITroop att in listAttackTroops.Where(att => att.Num > 0))
            {
                LogFunction.SetLog(att.GetName() + " : " + att.Num);
            }
        }


        private void ReturnHome()
        {
            LogFunction.SetLog("--  GO HOME  --");
            ClickFunction.Click(GlobalVariables.aSurrenderPoint);
            SleepFunction.Sleep(500);

            ClickFunction.Click(GlobalVariables.aConfirmSurrenderPoint);
            SleepFunction.Sleep(1500);

            ScreenCapture screen = new ScreenCapture(false);
            screen.SnapShot();
            string logPath = Path.Combine(GlobalVariables.EndAttackedDir, DateTime.Now.ToString("MM_dd_hh_mm") + ".jpg");
            Directory.CreateDirectory(GlobalVariables.EndAttackedDir);
            screen.BitMap.Save(logPath);
            screen.BitMap.Dispose();

            ClickFunction.Click(GlobalVariables.aReturnHomePoint);
            new Mainscreen().WaitMainScreen();
        }


        public void IdentityTroopAttack(ITroop troopSelected, bool isReport = true)
        {
            //[11:29:03] HogRider : 0
            //[11:29:03] Minion : 0
            //[11:29:04] Goblin : 0
            //[11:29:04] Dragon : 0
            //0,571   ->>>    668 , 860

            Point pointStart = new Point(2, 571);
            Point pointEnd = new Point(860, 670);

            ScreenCapture screenCapture = new ScreenCapture(false);
            screenCapture.SnapShot(pointStart, pointEnd);
            CVImageSBCv imgByOpenCv = new CVImageSBCv(screenCapture.BitMap);
            foreach (TroopKindEnum troopKind in Enum.GetValues(typeof(TroopKindEnum)))
            {
                Bitmap bTemplate =
                    (Bitmap)Properties.Resources.ResourceManager.GetObject("TroopATK_" + troopKind.ToString());
                if (bTemplate == null)
                {
                    continue;
                }
                //Point thisPointTroop = IMGTool.FindImg(screenCapture.BitMap, bTemplate);
                Point thisPointTroop = imgByOpenCv.GetPointMathchImage(bTemplate, 0.7);
                if (thisPointTroop.IsEmpty)
                {
                    bTemplate.Dispose();
                    bTemplate =
                        (Bitmap)Properties.Resources.ResourceManager.GetObject("TroopATK_" + troopKind.ToString() + "_Large");
                    //thisPointTroop = IMGTool.FindImg(screenCapture.BitMap, bTemplate);
                    if (bTemplate == null)
                    {
                        continue;
                    }
                    thisPointTroop = imgByOpenCv.GetPointMathchImage(bTemplate, 0.7);
                    bTemplate.Dispose();
                }
                if (!thisPointTroop.IsEmpty)
                {
                    Point realPointThisTroop = new Point(thisPointTroop.X + 2, thisPointTroop.Y + 571);
                    if (troopKind == TroopKindEnum.Queen || troopKind == TroopKindEnum.King)
                    {
                        _listKingTroops.Add(new KingTroop(troopKind, realPointThisTroop));
                        continue;
                    }
                    if (troopKind.ToString().Contains("Spell"))
                    {
                        _listSpellTroops.Add(new SpellTroop(troopKind, 0, realPointThisTroop));
                        continue;
                    }
                    if (troopKind == TroopKindEnum.ClanCastle)
                    {
                        _ccTroop = new AttackTroop(troopKind, 1, realPointThisTroop);
                        continue;
                    }
                    _listAttackTroops.Add(new AttackTroop(troopKind, 0, realPointThisTroop));
                }
            }
            IdentityTroopNumber(_listAttackTroops, troopSelected, isReport);
            IdentityTroopNumber(_listSpellTroops, troopSelected, isReport);
            screenCapture.BitMap.Dispose();
        }

        private void IdentityTroopNumber(IEnumerable<ITroop> listTroops, ITroop troopSelected, bool isReport = true)
        {
            var ListTroops = listTroops.ToList();
            if (ListTroops.Any())
            {
                ListTroops = ListTroops.OrderBy(x => x.Point.X).ToList();
                var ocrEnemyText = new OcrEnemyText();
                int startXAtk = 33;
                if (GetTotalTroopKind() > 11)
                {
                    startXAtk = 2;
                }

                for (int i = ListTroops.Count - 1; i >= 0; i--)
                {
                    int positionHere = GetTroopPosition(ListTroops[i].Point);
                    int numTroop = 0;
                    numTroop = ocrEnemyText.GetAttackTroop(positionHere, troopSelected == null ? GetTroopPosition(new Point()) : GetTroopPosition(troopSelected.Point), startXAtk);
                    ListTroops[i].Num = numTroop;
                }
            }
            if (isReport)
            {
                ReportTroops(ListTroops);
            }
        }

        private int GetTotalTroopKind()
        {
            return _listAttackTroops.Count + _listKingTroops.Count + _listSpellTroops.Count;
        }

        private int GetTroopPosition(Point pointTroop)
        {
            if (pointTroop.IsEmpty)
            {
                return 0;
            }
            return (int)Math.Floor((decimal)(pointTroop.X - 2) / 73);
        }
    }
}
