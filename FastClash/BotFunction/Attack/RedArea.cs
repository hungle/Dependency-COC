﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FastClash.BotFunction.Pixel;
using FastClash.ViewModels;
using FastClashFunction;
using FastClashFunction.RedArea;

namespace FastClash.BotFunction.Attack
{
    class RedArea
    {
        public List<HashSet<Point>> GetRedArea(int xSkip, int ySkip, int iColorVariation)
        {
            ScreenCapture screenCapture = new ScreenCapture();
            Bitmap bmp = screenCapture.SnapShot();
            List<HashSet<Point>> listPixelBySide = searchRedArea(bmp, xSkip, ySkip, iColorVariation);

            return listPixelBySide;
        }
        public List<HashSet<Point>> GetLineArea()
        {
            ScreenCapture screenCapture = new ScreenCapture();
            Bitmap bmp = screenCapture.SnapShot();
            List<HashSet<Point>> listPixelBySide = searchLineArea();

            return listPixelBySide;
        }

        private List<HashSet<Point>> searchLineArea()
        {
            List<HashSet<Point>> listPixelBySide = new List<HashSet<Point>>();
            HashSet<Point> PixelTopLeft = new HashSet<Point>();
            HashSet<Point> PixelBottomRight = new HashSet<Point>();
            HashSet<Point> PixelBottomLeft = new HashSet<Point>();
            HashSet<Point> PixelTopRight = new HashSet<Point>();
            {
                int xStartMin = 82;
                int yStartMin = 275;
                int xStartMax = 392;
                int yStartMax = 59;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y <= yStartMin && y >= yStartMax)
                {
                    PixelTopLeft.Add(new Point(x, y));
                    x = x + 4;
                    y = y - 3;
                }
            }

            {
                int xStartMin = 464;
                int yStartMin = 38;
                int xStartMax = 738;
                int yStartMax = 328;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y >= yStartMin && y <= yStartMax)
                {
                    PixelTopRight.Add(new Point(x, y));
                    x = x + 4;
                    y = y + 3;
                }
            }

            {
                int xStartMin = 96;
                int yStartMin = 359;
                int xStartMax = 370;
                int yStartMax = 553;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y >= yStartMin && y <= yStartMax)
                {
                    PixelBottomLeft.Add(new Point(x, y));
                    x = x + 4;
                    y = y + 3;
                }
            }

            {
                int xStartMin = 530;
                int yStartMin = 538;
                int xStartMax = 751;
                int yStartMax = 355;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y <= yStartMin && y >= yStartMax)
                {
                    PixelBottomRight.Add(new Point(x, y));
                    x = x + 4;
                    y = y - 3;
                }
            }
            listPixelBySide.Add(PixelTopLeft);
            listPixelBySide.Add(PixelBottomRight);
            listPixelBySide.Add(PixelBottomLeft);
            listPixelBySide.Add(PixelTopRight);
            return listPixelBySide;
        }

        private List<HashSet<Point>> searchRedArea(Bitmap bmp, int xSkip, int ySkip, int iColorVariation)
        {
            FastBitmap lockBitmap = new FastBitmap(bmp);
            lockBitmap.LockBitmap();
            DetectRedArea detectRedArea = new DetectRedArea(lockBitmap);

            Color color = ColorTranslator.FromHtml("#CD752E");
            Color colorRedNextOutZone = ColorTranslator.FromHtml("#C44F20");

            int iColorVariationNextOutZone = iColorVariation + 20;
            int CenterX = 430;
            int CenterY = 320;
            Point pixelTop = new Point(CenterX, 25);
            Point pixelLeft = new Point(60, CenterY);
            Point pixelCenter = new Point(CenterX, CenterY);
            Point pixelBottom = new Point(CenterX, 590);
            Point pixelRight = new Point(805, 312);
            List<Color> listColorToMatch = new List<Color> { ColorTranslator.FromHtml("#97AF47"), ColorTranslator.FromHtml("#ADC14C"), ColorTranslator.FromHtml("#6E8A31"), ColorTranslator.FromHtml("#FFFFFF") };

            HashSet<Point> PixelTopLeft = detectRedArea.PixelSearchRedAreaByX(color, iColorVariation, xSkip, ySkip, DetectRedArea.VectorType.eVectorLeftTop, pixelTop.X, pixelTop.Y, pixelTop.X, pixelCenter.Y);
            //  PixelTopLeft.UnionWith(detectRedArea.PixelSearchRedAreaByY(color, iColorVariation, ySkip, xSkip, DetectRedArea.VectorType.eVectorLeftTop, pixelTop.X, pixelTop.Y, pixelTop.X, pixelCenter.Y));

            HashSet<Point> PixelBottomRight = detectRedArea.PixelSearchRedAreaByX(color, iColorVariation, xSkip, ySkip, DetectRedArea.VectorType.eVectorRightBottom, pixelCenter.X, pixelCenter.Y, pixelRight.X, pixelBottom.Y);
            //   PixelBottomRight.UnionWith(detectRedArea.PixelSearchRedAreaByX(color, iColorVariation, ySkip, xSkip, DetectRedArea.VectorType.eVectorRightBottom, pixelCenter.X, pixelCenter.Y, pixelRight.X, pixelBottom.Y));

            HashSet<Point> PixelBottomLeft = detectRedArea.PixelSearchRedAreaByX(color, iColorVariation, xSkip, ySkip, DetectRedArea.VectorType.eVectorLeftBottom, pixelLeft.X, pixelLeft.Y, pixelCenter.X, pixelBottom.Y);
            //   PixelBottomLeft.UnionWith(detectRedArea.PixelSearchRedAreaByY(color, iColorVariation, ySkip, xSkip, DetectRedArea.VectorType.eVectorLeftBottom, pixelLeft.X, pixelLeft.Y, pixelCenter.X, pixelBottom.Y));

            HashSet<Point> PixelTopRight = detectRedArea.PixelSearchRedAreaByX(color, iColorVariation, xSkip, ySkip, DetectRedArea.VectorType.eVectorRightTop, pixelTop.X, pixelTop.Y, pixelTop.X, pixelCenter.Y);
            //   PixelTopRight.UnionWith(detectRedArea.PixelSearchRedAreaByY(color, iColorVariation, ySkip, xSkip, DetectRedArea.VectorType.eVectorRightTop, pixelTop.X, pixelTop.Y, pixelTop.X, pixelCenter.Y));

            PixelTopLeft = detectRedArea.removeBadPixel(PixelTopLeft, listColorToMatch, 10);
            PixelBottomRight = detectRedArea.removeBadPixel(PixelBottomRight, listColorToMatch, 10);
            PixelBottomLeft = detectRedArea.removeBadPixel(PixelBottomLeft, listColorToMatch, 10);
            PixelTopRight = detectRedArea.removeBadPixel(PixelTopRight, listColorToMatch, 10);



            HashSet<Point> PixelTopLeftNextOutZone = detectRedArea.PixelSearchRedArea(colorRedNextOutZone, iColorVariationNextOutZone, xSkip, ySkip, DetectRedArea.VectorType.eVectorLeftTop, 420, 30, 30, 300, 40, 1);
            HashSet<Point> PixelBottomLeftNextOutZone = detectRedArea.PixelSearchRedArea(colorRedNextOutZone, iColorVariationNextOutZone, xSkip, ySkip, DetectRedArea.VectorType.eVectorLeftBottom, 30, 310, 0, 550, 40, -1);
            HashSet<Point> PixelTopRightNextOutZone = detectRedArea.PixelSearchRedArea(colorRedNextOutZone, iColorVariationNextOutZone, xSkip, ySkip, DetectRedArea.VectorType.eVectorRightTop, 420, 50, 0, 310, 40, -1);
            HashSet<Point> PixelBottomRightNextOutZone = detectRedArea.PixelSearchRedArea(colorRedNextOutZone, iColorVariationNextOutZone, xSkip, ySkip, DetectRedArea.VectorType.eVectorRightBottom, 790, 310, 0, 550, 40, 1);

            PixelTopLeftNextOutZone = detectRedArea.VectorReadAreaNextOutZone(PixelTopLeftNextOutZone, DetectRedArea.VectorType.eVectorLeftTop);
            PixelBottomLeftNextOutZone = detectRedArea.VectorReadAreaNextOutZone(PixelBottomLeftNextOutZone, DetectRedArea.VectorType.eVectorLeftBottom);
            PixelTopRightNextOutZone = detectRedArea.VectorReadAreaNextOutZone(PixelTopRightNextOutZone, DetectRedArea.VectorType.eVectorRightTop);
            PixelBottomRightNextOutZone = detectRedArea.VectorReadAreaNextOutZone(PixelBottomRightNextOutZone, DetectRedArea.VectorType.eVectorRightBottom);

            PixelTopLeft.UnionWith(PixelTopLeftNextOutZone);
            PixelBottomLeft.UnionWith(PixelBottomLeftNextOutZone);
            PixelBottomRight.UnionWith(PixelBottomRightNextOutZone);
            PixelTopRight.UnionWith(PixelTopRightNextOutZone);

            List<HashSet<Point>> listPixelBySide = new List<HashSet<Point>>();
            if (PixelTopLeft.Count == 0)
            {
                int xStartMin = 82;
                int yStartMin = 275;
                int xStartMax = 392;
                int yStartMax = 59;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y <= yStartMin && y >= yStartMax)
                {
                    PixelTopLeft.Add(new Point(x, y));
                    x = x + 4;
                    y = y - 3;
                }
            }

            if (PixelTopRight.Count == 0)
            {
                int xStartMin = 464;
                int yStartMin = 38;
                int xStartMax = 738;
                int yStartMax = 328;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y >= yStartMin && y <= yStartMax)
                {
                    PixelTopRight.Add(new Point(x, y));
                    x = x + 4;
                    y = y + 3;
                }
            }

            if (PixelBottomLeft.Count == 0)
            {
                int xStartMin = 96;
                int yStartMin = 359;
                int xStartMax = 370;
                int yStartMax = 553;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y >= yStartMin && y <= yStartMax)
                {
                    PixelBottomLeft.Add(new Point(x, y));
                    x = x + 4;
                    y = y + 3;
                }
            }

            if (PixelBottomRight.Count == 0)
            {
                int xStartMin = 530;
                int yStartMin = 538;
                int xStartMax = 751;
                int yStartMax = 355;
                int x = xStartMin;
                int y = yStartMin;
                while (x >= xStartMin && x <= xStartMax && y <= yStartMin && y >= yStartMax)
                {
                    PixelBottomRight.Add(new Point(x, y));
                    x = x + 4;
                    y = y - 3;
                }
            }
            listPixelBySide.Add(PixelTopLeft);
            listPixelBySide.Add(PixelBottomRight);
            listPixelBySide.Add(PixelBottomLeft);
            listPixelBySide.Add(PixelTopRight);
            /*foreach (HashSet<Point> hashet in listPixelBySide)
            {
                foreach (Point p in hashet)
                {
                    lockBitmap.SetPixel(p.X, p.Y, Color.Black);
                }
            }*/
            lockBitmap.UnlockBitmap();
            // cloneBitmap.Save(@"C:\result.png");
            return listPixelBySide;
        }

    }
}
