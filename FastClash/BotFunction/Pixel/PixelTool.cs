﻿using System;
using System.Data.Common;
using System.Drawing;
using FastClash.ViewModels;
using FastClashFunction;

namespace FastClash.BotFunction.Pixel
{
    public static class PixelTool
    {
        public static bool CheckColor(Color c1, Color c2, int variance)
        {
            bool[] result = new bool[3]; //R,G,B
            result[0] = Math.Abs(c1.R - c2.R) < variance;
            result[1] = Math.Abs(c1.G - c2.G) < variance;
            result[2] = Math.Abs(c1.B - c2.B) < variance;

            return result[0] && result[1] && result[2];
        }


        /// <summary>
        /// this function for Global bitmap value
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="needCapture"></param>
        /// <returns></returns>
        public static Color GetColor(int x, int y, bool needCapture = false,bool isGlobal = true)
        {
            if (needCapture)
            {
                ScreenCapture screenCapture = new ScreenCapture(isGlobal);
                screenCapture.SnapShot(x,y,1,1);
                FastBitmap fbm = new FastBitmap(screenCapture.BitMap);
                fbm.LockBitmap();
                var cl = fbm.GetPixel(0, 0);
                fbm.UnlockBitmap();
                if (!isGlobal)
                {
                    screenCapture.BitMap.Dispose();                    
                }
                return cl;
            }
            FastBitmap fbmp = new FastBitmap(GlobalVariables.BitMapGlobal);
            fbmp.LockBitmap();
            var clr = fbmp.GetPixel(x, y);
            fbmp.UnlockBitmap();
            return clr;
        }

        public static bool CheckColorAtPosition(int x, int y, Color c1, int variance, bool needCapture = false, bool isGlobal = true)
        {
            return CheckColor(c1, GetColor(x, y, needCapture, isGlobal), variance);
        }

        public static bool CheckColorAtPoint(Point point, Color c1, int variance, bool needCapture = false, bool isGlobal = true)
        {
            return CheckColor(c1, GetColor(point.X, point.Y, needCapture, isGlobal), variance);
        }

        public static Point PixelSearch(int left, int top, int width, int height, Color color, int variance, bool isGlobal = true)
        {
            ScreenCapture screenCapture = new ScreenCapture(isGlobal);
            screenCapture.SnapShot(left, top, width, height);
            FastBitmap fbmp = new FastBitmap(screenCapture.BitMap);
            fbmp.LockBitmap();
            for (int i = left; i < left + width; i++)
            {
                for (int j = top; j < top + height; j++)
                {
                    if (CheckColor(fbmp.GetPixel(i - left, j - top), color, variance))
                    {
                        fbmp.UnlockBitmap();
                        if (!isGlobal)
                        {
                            screenCapture.BitMap.Dispose();
                        }
                        return new Point(i, j);
                    }
                }
            }
            fbmp.UnlockBitmap();
            if (!isGlobal)
            {
                screenCapture.BitMap.Dispose();
            }
            return new Point();
        }
    }
}
