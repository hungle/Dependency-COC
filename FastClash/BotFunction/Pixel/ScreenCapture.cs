﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using FastClash.ViewModels;
using FastClashFunction;

namespace FastClash.BotFunction.Pixel
{
    public class ScreenCapture
    {
        private bool _isGlobal = true;
        public ScreenCapture(bool isGlobal = true)
        {
            _isGlobal = isGlobal;
            Dispose();
        }

        public void Dispose()
        {
            FreeCurrentImage();
        }

        private Bitmap _bitmap;
        public Bitmap BitMap
        {
            get
            {
                if (_isGlobal) return GlobalVariables.BitMapGlobal;
                else return _bitmap;
            }
            private set
            {
                if (_isGlobal) GlobalVariables.BitMapGlobal = value;
                else _bitmap = value;
            }
        }
        private IntPtr _hBitmap = IntPtr.Zero;

        private void FreeCurrentImage()
        {
            if (_isGlobal)
            {
                if (GlobalVariables.BitMapGlobal != null)
                {
                    GlobalVariables.BitMapGlobal.Dispose();
                    GlobalVariables.BitMapGlobal = null;
                }
            }
            else
            {
                if (BitMap != null)
                {
                    BitMap.Dispose();
                    Win32.DeleteObject(_hBitmap);
                    BitMap = null;
                    _hBitmap = IntPtr.Zero;
                }
            }
        }

        // Full client area variant of BackgroundSnapShot
        public Bitmap SnapShot()
        {
            return SnapShot(GetBsArea());
        }

        public Bitmap SnapShot(Rectangle rect)
        {
            return SnapShot(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        /// <summary>
        /// This capture algorithm should behave as on the background mode of Autoit Coc Bot (as at end of january 2015)
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public Bitmap SnapShot(int left, int top, int width, int height)
        {
            FreeCurrentImage();
            IntPtr hWnd = BlueStacksHelper.GetMainControlOfBlueStacksHandle();
            if (hWnd == IntPtr.Zero)
                return null;
            IntPtr hCaptureDc = Win32.GetWindowDC(hWnd);
            IntPtr hMemDc = Win32.CreateCompatibleDC(hCaptureDc);
            _hBitmap = Win32.CreateCompatibleBitmap(hCaptureDc, width, height);
            IntPtr hObjOld = Win32.SelectObject(hMemDc, _hBitmap);

            bool result = true;


            result = Win32.PrintWindow(hWnd, hMemDc, 0);
            //if (result)
                Win32.SelectObject(hMemDc, _hBitmap);

            //if (result)
                result = Win32.BitBlt(hMemDc, 0, 0, width, height, hCaptureDc, left, top, Win32.TernaryRasterOperations.Srccopy);
            //if (result)
                BitMap = Bitmap.FromHbitmap(_hBitmap);
            Win32.DeleteDC(hMemDc);
            Win32.SelectObject(hMemDc, hObjOld);
            Win32.ReleaseDC(hWnd, hCaptureDc);
            Win32.DeleteObject(_hBitmap);
            //Debug.Assert(result);
            return BitMap;
        }

        public Bitmap SnapShot(Point start, Point end)
        {
            return SnapShot(start.X, start.Y, end.X - start.X, end.Y - start.Y);
        }

        private bool OffsetToBsClientScrrenCoord(ref Rectangle rect)
        {
            if (!BlueStacksHelper.IsBlueStacksRunning) return false;
            Win32.POINT origin = new Win32.POINT(0, 0);
            if (Win32.ClientToScreen(BlueStacksHelper.GetBlueStacksWindowHandle(), ref origin)) return false;
            rect.Offset(origin.X, origin.Y);
            return true;
        }

        private Rectangle GetBsArea()
        {
            if (!BlueStacksHelper.IsBlueStacksRunning) return Rectangle.Empty;
            Win32.Rect win32Rect;
            if (!Win32.GetClientRect(BlueStacksHelper.GetBlueStacksWindowHandle(), out win32Rect))
                return Rectangle.Empty;
            return Rectangle.FromLTRB(win32Rect.Left, win32Rect.Top, win32Rect.Right, win32Rect.Bottom);
        }


        public bool SaveCurrentSnapShotToFile(string fileName)
        {
            if (BitMap == null) return false;
            try
            {
                BitMap.Save(fileName);
            }
            catch (Exception e)
            {
                Debug.Assert(false, e.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Simple capture implementation using C# high level functions. 
        /// It should only work when BlueStacks is fully visible (NOT background mode). 
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public Bitmap DotNetSnapShot(Rectangle rect)
        {
            FreeCurrentImage();
            if (!OffsetToBsClientScrrenCoord(ref rect)) return null;

            //Create a new bitmap.
            BitMap = new Bitmap(rect.Width, rect.Height, PixelFormat.Format32bppArgb);

            // Create a graphics object from the bitmap.
            using (var gfxScreenshot = Graphics.FromImage(BitMap))
            {
                // Take the screenshot from the upper left corner to the right bottom corner.
                gfxScreenshot.CopyFromScreen(rect.Left, rect.Top, 0, 0, rect.Size, CopyPixelOperation.SourceCopy);
            }
            return BitMap;
        }

        public Bitmap DotNetSnapShot(int left, int top, int width, int height)
        {
            return DotNetSnapShot(new Rectangle(left, top, width, height));
        }
    }
}
