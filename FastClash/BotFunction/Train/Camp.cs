﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using FastClash.BotFunction.Attack;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using FastClash.BotFunction.ReadText;
using FastClash.Misc;
using FastClash.Misc.Enum;
using FastClash.ViewModels;
using ImgProcessing.CV.PInvoke;

namespace FastClash.BotFunction.Train
{
    public class Camp
    {
        public int percentCamp { get; set; }
        private int totalArmy = 0;
        private int currentArmy = 0;
        private int boostedBarracksCount = 0;
        private int boostedDBarracksCount = 0;
        private int[] barracksLevels;
        private int[] barracksBoosted;
        private int[] dbarracksLevels;
        private int[] dbarracksBoosted;
        private int[] sfLevels;
        private int[] barracksTime = new int[4];
        int[,] troopDistribution = new int[4, 10];
        int[] troopTrainingCount = new int[10];
        IEnumerable<TroopModel> selectedTroops;

        public Camp()
        {
        }

        public void OpenCamp()
        {
            Other.ClickFunction.Click(GlobalVariables.pointAway);
            SleepFunction.Sleep(1000);
            Other.ClickFunction.ClickCamp();
            SleepFunction.Sleep(1000);
        }

        private string GetCamp()
        {
            return new OcrTrainText().GetCampCountFraction();
        }

        public int TotalArmy
        {
            get { return totalArmy; }
        }
        public int CurrentArmy
        {
            get { return currentArmy; }
        }


        public void GetCampInfo()
        {
            int iCount = 0;
            string armyInfo = GetCamp();
            while (armyInfo == "0#0" && iCount < 10)
            {
                SleepFunction.Sleep(200);
                iCount++;
                armyInfo = GetCamp();
            }
            if (armyInfo.Trim() == "")
            {
                armyInfo = "0#0";
            }
            currentArmy = int.Parse(armyInfo.Split('#')[0]);
            totalArmy = int.Parse(armyInfo.Split('#')[1]);
        }

        public bool IsFullCamp()
        {
            if (totalArmy == 0) return false;
            if (currentArmy == 0) return false;
            if (currentArmy >= (TotalArmy * percentCamp) / 100)
            {
                return true;
            }
            return false;
        }

        private List<object[]> IdentityTroopCamp()
        {
            List<object[]> listTroopCamp;
            Point pointStart = new Point(143, 168);
            Point pointEnd = new Point(711, 251);
            listTroopCamp = new List<object[]>();
            ScreenCapture screenCapture = new ScreenCapture();
            if (!PixelTool.CheckColorAtPosition(175, 200, Color.FromArgb(0xD6D5CC), 10, true))
            {
                screenCapture.SnapShot(pointStart, pointEnd);
                CVImageSBCv imgByOpenCv = new CVImageSBCv(screenCapture.BitMap);
                foreach (TroopKindEnum troopKind in Enum.GetValues(typeof(TroopKindEnum)))
                {
                    Bitmap bTemplate =
                        (Bitmap)Properties.Resources.ResourceManager.GetObject("TroopCamp_" + troopKind.ToString());
                    if (bTemplate == null) continue;
                    //Point thisPointTroop = IMGTool.FindImg(screenCapture.BitMap, bTemplate);
                    Point thisPointTroop = imgByOpenCv.GetPointMathchImage(bTemplate, 0.7);
                    bTemplate.Dispose();
                    if (!thisPointTroop.IsEmpty)
                    {
                        listTroopCamp.Add(new object[] { thisPointTroop.X, troopKind, 0 });
                    }
                }

                if (listTroopCamp.Count > 0)
                {
                    listTroopCamp = listTroopCamp.OrderBy(x => x[0]).ToList();
                    OcrTrainText readTrainText = new OcrTrainText();
                    for (int i = 0; i < listTroopCamp.Count; i++)
                    {
                        int numTroop = readTrainText.GetCampCount(i);
                        listTroopCamp[i][2] = numTroop;
                    }
                }
            }
            for (int i = 0; i < listTroopCamp.Count; i++)
            {
                LogFunction.SetLog(listTroopCamp[i][1] + " : " + listTroopCamp[i][2]);
            }
            return listTroopCamp;
        }

        private Dictionary<string, int> GetCampComposition(List<object[]> listTroopCamp)
        {
            Dictionary<string, int> newDictionary = new Dictionary<string, int>();

            if (listTroopCamp.Count > 0)
            {
                for (int i = 0; i < listTroopCamp.Count; i++)
                {
                    newDictionary.Add(listTroopCamp[i][1].ToString(), (int)listTroopCamp[i][2]);
                }
            }
            return newDictionary;
        }

        public void GetTroopsTrainingCount()
        {
            LogFunction.SetLog("Getting training troops count.");
            OcrTrainText tt = new OcrTrainText();
            troopTrainingCount = new int[10];
            for (int i = 0; i < 4; ++i)
            {
                ClickNext();
                for (int row = 0; row < 2; ++row)
                {
                    for (int j = 0; j < 5; ++j)
                    {
                        troopTrainingCount[row * 5 + j] += tt.GetTroopTrainingCount(j, row);
                    }
                }
            }
            ClickArmyOverview();
            LogFunction.SetLog("Done.");
            /*
            for (int i = 0; i < 4; ++i)
            {
                string log = string.Format("Barrack {0} : ", i + 1);
                LogFunction.SetLog(log);
                for (int j = 0; j < 10; ++j)
                {
                    if (array[i, j] != 0)
                    {
                        //log = ((TroopKindEnum)j).ToString();
                        log = string.Format("{0} : {1}",j,array[i,j]);
                        LogFunction.SetLog(log);
                    }
                }
            }*/
        }

        private void ClearTroop()
        {
            int iCount = 0;
            while (!PixelTool.CheckColorAtPoint(GlobalVariables.PointClearTroops, Color.FromArgb(0xD1D0C2), 20, true) && iCount < 4)
            {
                Other.ClickFunction.Click(GlobalVariables.PointClearTroops, 40, 5);
                SleepFunction.Sleep(200);
                iCount++;
            }
        }

        public enum TrainMode
        {
            Fresh = 0,
            Addon = 1
        }
        private void CalulateCustomTrain(ObservableCollection<TroopModel> troops, TrainMode tm)
        {

            selectedTroops = troops.Where(t => t.Count > 0);
            if (selectedTroops.Count() == 0)
            {
                return;
            }

            ClickArmyOverview();
            Dictionary<String, int> currentCampComposition = new Dictionary<String, int>();
            if (tm == TrainMode.Fresh)
            {
                barracksTime = new int[4];
                troopDistribution = new int[4, 10];
                troopTrainingCount = new int[10];
                if(GlobalVariables.isFirstTime && !IsFullCamp())
                {
                    currentCampComposition = GetCampComposition(IdentityTroopCamp());
                }
            }
            else
            {
                GetTroopsTrainingCount();
                currentCampComposition = GetCampComposition(IdentityTroopCamp());
            }
            selectedTroops = selectedTroops.OrderByDescending(t => t.TrainTime);
            foreach (var troop in selectedTroops)
            {
                int level = troop.Level; //barrack level required to train
                int trainableBarracks = 0;
                for (int i = 0; i < 4; ++i)
                {
                    if (barracksLevels[i] >= level)
                    {
                        troop.CanTrain[i] = true;
                        trainableBarracks++;
                    }
                }

                int maxTime = barracksTime.Max();
                var timeDiff = barracksTime.Select(t => maxTime - t).ToArray();
                var time = troop.TrainTime;
                var id = troop.Id;

                int count = tm == TrainMode.Fresh? troop.Count : troop.Count - troopTrainingCount[id];
                int inCamp;
                if (currentCampComposition.TryGetValue(troop.Name, out inCamp))
                {
                    count -= inCamp;
                }
                if (count <= 0)
                {
                    continue;
                }

                int[] prelimDistribution = new int[4] { 0, 0, 0, 0 }; // Preliminary distribution
                int[] netTime = new int[4] { 0, 0, 0, 0 }; // net time taken for barracks to train a specific troop considering boost
                int countRequired = 0; //Number of this troop required to balance barracks
                int equivalentBarracksCount = 0; //Number of equivalent barracks. 1 boosted barrack = boost_multiplier * normal barracks
                for (int i = 0; i < 4; ++i)
                {
                    netTime[i] = time / barracksBoosted[i]; //Barracks boosted contains the multiplier i.e. 4 if boosted, 1 if not
                    if (troop.CanTrain[i])
                    {
                        equivalentBarracksCount += barracksBoosted[i]; //Only count barrack if it can train
                        int prelimCount = timeDiff[i] / netTime[i];
                        prelimDistribution[i] = prelimCount;
                        countRequired += prelimCount;
                    }
                }

                double factor;
                if (countRequired != 0) // There may be no time difference between barracks resulting in countRequired = 0
                {
                    //This factor is used to find the actual number of troops to be assigned first from the troops remaining to train
                    factor = (double)count / countRequired;
                    factor = factor > 1 ? 1 : factor; //If there are more troops to train than the troops required to balance, first balance in this loop
                    for (int i = 0; i < 4; ++i)
                    {
                        int distributedCount = (int)(prelimDistribution[i] * factor);
                        troopDistribution[i, id] += distributedCount;
                        barracksTime[i] += netTime[i] * distributedCount;
                        timeDiff[i] -= netTime[i] * distributedCount;
                        count -= distributedCount;
                    }
                }

                factor = (double)count / equivalentBarracksCount; // After balancing the time, split troops equally
                for (int i = 0; i < 4; ++i)
                {
                    int distributedCount = (int)(barracksBoosted[i] * factor);
                    troopDistribution[i, id] += distributedCount;
                    barracksTime[i] += netTime[i] * distributedCount;
                    timeDiff[i] -= netTime[i] * distributedCount;
                    count -= distributedCount;
                }

                //This loop is to ensure assigning of the remaining troops < equivalentBarracksCount which were left out due to integral divisions
                double delta = 0;
                while (count > 0)
                {
                    for (int i = 0; i < 4 && count > 0; ++i)
                    {
                        var num = (double)timeDiff[i] / netTime[i];
                        if (num > 0.75 - delta)
                        {
                            troopDistribution[i, id]++;
                            barracksTime[i] += netTime[i];
                            timeDiff[i] -= netTime[i];
                            count--;
                        }
                    }
                    delta += 0.25;
                }

                //Logging the distribution
                string log = String.Format("{0} has been split up as {1},", troop.Name, troopDistribution[0, id]);
                for (int i = 1; i < 4; ++i)
                {
                    log += troopDistribution[i, id] + ",";
                }
                LogFunction.SetLog(log);
                #region old code
                /*while (assigned && count != 0)
                {
                    var orderedTimeDiff = timeDiff.OrderByDescending(t => t);
                    for (int i = 0; i < 4; ++i)
                    {
                        int index = Array.IndexOf(timeDiff, orderedTimeDiff.ElementAt(i));
                        if (troop.CanTrain[index])
                        {
                            var num = (double)timeDiff[index] / netTime[index];
                            num = num - (int)num > 0.75 ? (int)num + 1 : (int)num;
                            if (num > 0)
                            {
                                int distributedCount = num < count ? Convert.ToInt32(num) : count;
                                troopDistribution[index, id] += distributedCount;
                                barracksTime[index] += netTime[index] * distributedCount;
                                timeDiff[index] -= netTime[index] * distributedCount;
                                count -= distributedCount;
                                if (count == 0)
                                {
                                    break;
                                }
                                assigned = true;
                                continue;
                            }
                        }
                        assigned = false;
                    }
                }
                for (int i = 0; count > 0; ++i)
                {
                    bool isEven = i % 2 == 0;
                    for (int index = 0; index < 4 && count > 0; ++index)
                    {
                        if (troop.CanTrain[index])
                        {
                            if (isEven)
                            {
                                troopDistribution[index, id]++;
                                barracksTime[index] += (time / barracksBoosted[index]);
                                count--;
                            }
                            else
                            {
                                int toTrain = count < (barracksBoosted[index] * 2 - 1) ? count : (barracksBoosted[index] * 2 - 1);
                                troopDistribution[index, id] += toTrain;
                                barracksTime[index] += (time / barracksBoosted[index]) * toTrain;
                                count -= toTrain;
                            }
                        }
                    }
                }*/
                #endregion
            }
        }

        public void CustomTrain(ObservableCollection<TroopModel> troops, TrainMode tm)
        {

            CalculateBoostedBuildings();
            if (GlobalVariables.isFirstTime)
            {
                tm = TrainMode.Fresh;
            }
            CalulateCustomTrain(troops, tm);
            /*foreach (var item in barracksTime)
            {
                LogFunction.SetLog(item.ToString());
            }*/
            if (selectedTroops.Count() == 0)
            {
                return;
            }
            for (int i = 0; i < 4; ++i)
            {
                if (barracksLevels[i] == 0)
                {
                    break;
                }
                ClickNext();
                string log = String.Format("Currently training in Barrack {0}", i);
                LogFunction.SetLog(log);
                OcrTrainText tt = new OcrTrainText();

                int diff = 0;


                if (tm == TrainMode.Fresh || IsFullCamp())
                {
                    ClearTroop();
                    log = "Clearing troops.";
                    LogFunction.SetLog(log);
                }

                //Maximum 1 second waiting after next click successfull
                for (int iCount = 0; iCount < 10; ++iCount)
                {
                    var queue = tt.GetBarrackQueueFraction().Split('#');
                    if (queue.Length == 2)
                    {
                        int totalCapacity = Convert.ToInt32(queue[1]);
                        if (totalCapacity != 0)
                        {
                            int training = Convert.ToInt32(queue[0]);
                            diff = totalCapacity - training;
                            log = String.Format("{0} troop capacity left in Barrack {1}", diff, i + 1);
                            LogFunction.SetLog(log);
                            break;
                        }
                    }
                    SleepFunction.Sleep(100);
                }

                foreach (var troop in selectedTroops)
                {
                    if (diff == 0)
                    {
                        break;
                    }
                    var id = troop.Id;
                    var space = troop.Space;
                    var name = troop.Name;
                    if (troopDistribution[i, id] == 0)
                    {
                        continue;
                    }
                    int totalTrain = troopDistribution[i, id];
                    int toTrain = totalTrain * space < diff ? totalTrain : diff / space;
                    log = string.Format("Training {0} of {1} out of {2} remaining", toTrain, name, totalTrain);
                    LogFunction.SetLog(log);
                    troop.Train(toTrain);
                    troopDistribution[i, id] -= toTrain;
                    diff -= toTrain;
                }
            }
            ClickArmyOverview();
            GlobalVariables.isFirstTime = false;
            SleepFunction.Sleep(500);
        }

        #region Building Properties
        //public void GetAllBuildingLevels(out int[] barracksLevels, out int[] dbarracksLevels, out int[] sfLevels)
        //{
        //    barracksLevels = GetBuildingLevels(Building.Barrack);
        //    dbarracksLevels = GetBuildingLevels(Building.DarkBarrack);
        //    sfLevels = GetBuildingLevels(Building.SpellFactory);
        //    ClickArmyOverview();
        //}

        private void GetBoosted()
        {
            //248,523 - barracks start
            //514,523 - dbarracks start
            //color - 0x4A750C
            //step -> x+=60px
            ClickArmyOverview();
            Color clr = Color.FromArgb(0x4A750C);
            int x = 248, y = 523;
            barracksBoosted = new int[4] { 1, 1, 1, 1 };
            dbarracksBoosted = new int[2] { 1, 1 };

            for (int i = 0; i < 4; ++i)
            {
                if (PixelTool.CheckColorAtPosition(x + 60 * i, y, clr, 20, true))
                {
                    barracksBoosted[i] = 4;
                }
            }
            boostedBarracksCount = barracksBoosted.Count(b => b == 4);

            x = 514;
            for (int i = 0; i < 2; ++i)
            {
                if (PixelTool.CheckColorAtPosition(x + 60 * i, y, clr, 20, true))
                {
                    dbarracksBoosted[i] = 4;
                }
            }
            boostedDBarracksCount = dbarracksBoosted.Count(b => b == 4);
        }

        private int[] GetBuildingLevels(Building building)
        {
            //191,295 #484438
            //DB - 365,112 - black
            //B - 331,112 - black
            //SF - 317,112 - black
            int x = 0, y = 0, rows = 2;
            Color clr = Color.Black;
            int[] buildingLevels;
            switch (building)
            {
                case Building.Barrack:
                    buildingLevels = new int[4] { 0, 0, 0, 0 };
                    x = 211;
                    y = 329;
                    clr = Color.FromArgb(0x082E40);
                    break;
                case Building.DarkBarrack:
                    buildingLevels = new int[2] { 0, 0 };
                    x = 217;
                    y = 336;
                    clr = Color.FromArgb(0x0C0D1B);
                    break;
                case Building.SpellFactory:
                    buildingLevels = new int[2] { 0, 0 };
                    x = 223;
                    y = 154;
                    clr = Color.FromArgb(0x020202);
                    rows = 1;
                    break;
                default:
                    buildingLevels = null;
                    return null;
            }
            int iCount = 0;
            while (!PixelTool.CheckColorAtPosition(x, y, clr, 10, true) && iCount < 8)
            {
                ClickNext();
                iCount++;
            }
            int i = 0;
            while (PixelTool.CheckColorAtPosition(x, y, clr, 10, true) && i < buildingLevels.Count())
            {
                buildingLevels[i] = GetLevel(rows);
                ClickNext();
                ++i;
            }

            return buildingLevels;
        }
        private enum Building
        {
            Barrack, DarkBarrack, SpellFactory
        }
        private int GetLevel(int rows = 2)
        {
            Color clr = ColorTranslator.FromHtml("#484438");
            int level = 0;
            bool exit = false;
            int x = 191, y = 295;
            for (int j = 0; j < rows && exit == false; ++j)
            {
                for (int k = 0; k < 5; ++k)
                {
                    if (PixelTool.CheckColorAtPosition(x, y, clr, 20, true))
                    {
                        exit = true;
                        break;
                    }
                    level++;
                    x += 107;
                }
                x = 191;
                y += 106;
            }
            return level;
        }

        public void CaculateAtkBuildingLevel()
        {
            barracksLevels = GetBuildingLevels(Building.Barrack);
            string log = "Barracks Levels are " + barracksLevels[0];
            for (int i = 1; i < 4; ++i)
            {
                var level = barracksLevels[i];
                log += String.Format(",{0}", level);
            }
            LogFunction.SetLog(log);

            dbarracksLevels = GetBuildingLevels(Building.DarkBarrack);
            log = "Dark Barracks Levels are " + dbarracksLevels[0];
            for (int i = 1; i < 2; ++i)
            {
                var level = dbarracksLevels[i];
                log += String.Format(",{0}", level);
            }
            LogFunction.SetLog(log);

            sfLevels = GetBuildingLevels(Building.SpellFactory);
            log = "Spell Factory Levels are " + sfLevels[0];
            for (int i = 1; i < 2; ++i)
            {
                var level = sfLevels[i];
                log += String.Format(",{0}", level);
            }
            LogFunction.SetLog(log);

        }

        public void CalculateBoostedBuildings()
        {
            GetBoosted();
            string log = "Number of boosted barracks: " + boostedBarracksCount;
            LogFunction.SetLog(log);

            log = "Number of boosted dark barracks: " + boostedDBarracksCount;
            LogFunction.SetLog(log);
        }
        private void GetBarracksCapacity(int[] barracksLevels, out int[] barracksCapacity)
        {
            barracksCapacity = barracksLevels.Select(l => Constants.BarracksCapacity[l]).ToArray();
        }
        #endregion
        #region Clicks
        private void ClickNext()
        {
            int iCount = 0;
            Other.ClickFunction.Click(770, 345, 1, 150);
            while (!PixelTool.CheckColorAtPosition(250, 313, Color.White, 10, true) && iCount < 20)
            {
                SleepFunction.Sleep(50);
                iCount++;
            }
        }

        private void ClickArmyOverview()
        {
            int iCount = 0;
            Other.ClickFunction.Click(150, 575, 1, 150);
            while (!PixelTool.CheckColorAtPosition(346, 118, Color.White, 10, true) && iCount < 20)
            {
                SleepFunction.Sleep(50);
                iCount++;
            }
        }
        #endregion
    }
}
