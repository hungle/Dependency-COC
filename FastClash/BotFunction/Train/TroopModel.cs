﻿using System.Drawing;
using FastClash.BotFunction.Other;
using FastClash.ViewModels;

namespace FastClash.BotFunction.Train
{
    public class TroopModel : BindableBase
    {
        //private Color color;
        private Point location;


        public int Id { get; set; }
        public int Level { get; set;}
        public int Space { get; set; }
        public int TrainTime { get; set; }
        public bool[] CanTrain { get; set; }

        private int count;
        public int Count
        {
            get { return count; }
            set { SetField(ref count, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { SetField(ref name, value); }
        }

        /// <summary>
        /// Constructor of TroopModel
        /// </summary>
        /// <param name="name">Name of the troop.</param>
        /// <param name="location">Position of troop in the train tab relative to client.</param>
        /// <param name="traintime">Time taken to train in seconds.</param>
        /// <param name="space">Army camp space occupied by the troop.</param>
        /// <param name="count">Number of troop to train.</param>
        public TroopModel(int id,int level, string name, /*Color color,*/ Point location, int traintime, int space = 1, int count = 0)
        {
            Id = id;
            Level = level;
            Name = name;
            Count = count;
            Space = space;
            //this.color = color;
            TrainTime = traintime;
            this.location = location;
            CanTrain = new bool[4];
        }

        public void Train(int quantity)
        {
            //ScreenCapture.SnapShot();
            //FastBitmap fb = new FastBitmap(ScreenCapture.BitMap);
            //fb.LockBitmap();
            for (int i = 0; i < quantity; )
            {
                //if (fb.GetPixel(location).ToArgb() == color.ToArgb())
                //{
                Other.ClickFunction.Click(location);
                SleepFunction.Sleep(50);
                ++i;
                //} 
            }
            //fb.UnlockBitmap();
        }

        /*public void Train()
        {
            for (int i = 0; i < 4; ++i)
            {
                Other.ClickFunction.PureClick(770, 345, 1, 300);
                Train(BarracksWise[i]);
            }
        }*/
    }
}
