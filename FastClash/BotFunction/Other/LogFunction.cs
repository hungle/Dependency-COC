using FastClash.ViewModels;

namespace FastClash.BotFunction.Other
{
    public static class LogFunction
    {
        public static void SetLog(string txtLog)
        {
            lock (GlobalVariables.syncObj)
            {
                if (GlobalVariables.token.IsCancellationRequested)
                {
                    return;
                }
                LogWriter.Instance.WriteToLog(txtLog);
            }
        }
    }
}