﻿using System.Diagnostics;
using System.Drawing;
using FastClash.BotFunction.MainScreen;
using FastClash.ViewModels;
using FastClash.BotFunction.Pixel;
using FastClashFunction;

namespace FastClash.BotFunction.Other
{
    public static class ClickFunction
    {
        /// <summary>
        /// Click will always check problem first then do the click
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="time"></param>
        /// <param name="speed"></param>
        public static void Click(int x, int y, int time = 1, int speed = 0)
        {
            lock (GlobalVariables.syncObj)
            {
                for (int i = 0; i < time; i++)
                {
                    if (GlobalVariables.token.IsCancellationRequested)
                    {
                        return;
                    }
                    if (new Mainscreen().IsProblemAffect(true))
                    {
                        new Mainscreen().FirstTimeBotRun();
                    }
                    BlueStacksHelper.Click(x, y);
                    SleepFunction.Sleep(speed);
                }
            }
        }

        public static void Click(Point point, int time = 1, int speed = 0)
        {
            Click(point.X, point.Y, time, speed);
        }
        /// <summary>
        /// PureClick only do a click, dont check any
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="time"></param>
        /// <param name="speed"></param>
        public static void PureClick(int x, int y, int time = 1, int speed = 0)
        {
            for (int i = 0; i < time; i++)
            {
                BlueStacksHelper.Click(x, y);
                SleepFunction.Sleep(speed);
            }
        }
        public static void PureClick(Point point, int time = 1, int speed = 0)
        {
            PureClick(point.X, point.Y, time, speed);
        }


        public static void ClickCamp()
        {
            Click(GlobalVariables.pointCamp);
            SleepFunction.Sleep(1000);
        }

        public static void ClickDonate()
        {
            Click(GlobalVariables.pointDonate);
            SleepFunction.Sleep(500);
        }
    }
}
