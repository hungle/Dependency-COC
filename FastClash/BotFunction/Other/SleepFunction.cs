using System.Diagnostics;
using System.Threading;

namespace FastClash.BotFunction.Other
{
    public static class SleepFunction
    {
        /// <summary>
        /// This function make a sleep for bot
        /// </summary>
        /// <param name="time"></param>
        public static void Sleep(int time)
        {
            lock (GlobalVariables.syncObj)
            {
                if (GlobalVariables.token.IsCancellationRequested)
                {
                    return;
                }
                System.Threading.Thread.Sleep(time);
            }
        }
    }
}