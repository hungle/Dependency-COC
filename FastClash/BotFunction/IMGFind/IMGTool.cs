﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using FastClash.BotFunction.Pixel;
using FastClash.ViewModels;
using ImgProcessing;
using ImgProcessing.CV.PInvoke;

namespace FastClash.BotFunction.IMGFind
{
    public static class IMGTool
    {
        private static readonly string[] sTowHall = { "TH5", "TH6", "TH7", "TH8", "TH9", "TH10" };
        private static readonly string[] sElixierEx = { "elixir_extractor_1", "elixir_extractor_2", "elixir_extractor_3", "elixir_extractor_4", "elixir_extractor_5",
                                                          "elixir_extractor_6","elixir_extractor_7","elixir_extractor_8","elixir_extractor_9","elixir_extractor_10",
                                                      "elixir_extractor_11","elixir_extractor_12","elixir_extractor_13"
                                                      };

        private static readonly string[] sMineEx = { "mine_extractor_1", "mine_extractor_2", "mine_extractor_3", "mine_extractor_4", "mine_extractor_5",
                                                          "mine_extractor_6","mine_extractor_7","mine_extractor_8","mine_extractor_9","mine_extractor_10",
                                                      "mine_extractor_11","mine_extractor_12","mine_extractor_13"
                                                      };

        private static readonly string[] sDarkEx = { "DRILL_LV_1", "DRILL_LV_2", "DRILL_LV_3", "DRILL_LV_4", "DRILL_LV_5", "DRILL_LV_6", "DRILL_LV_7" };

        private static readonly string[] sBarrack = { "BarrackLv1_1", "BarrackLv1_2", "BarrackLv10_1", "BarrackLv10_2", "BarrackLv10_3", "BarrackLv2_1",
                                                        "BarrackLv2_2","BarrackLv3_1.bmp","BarrackLv3_2.bmp","BarrackLv4_1.bmp","BarrackLv4_2.bmp","BarrackLv4_3.bmp"
                                                    ,"BarrackLv5_1.bmp","BarrackLv5_2.bmp","BarrackLv5_3.bmp","BarrackLv6_1.bmp","BarrackLv6_2.bmp","BarrackLv6_3.bmp"
                                                    ,"BarrackLv7_1.bmp","BarrackLv7_2.bmp","BarrackLv7_3.bmp","BarrackLv8_1.bmp","BarrackLv8_2.bmp","BarrackLv8_3.bmp"
                                                    ,"BarrackLv9_1.bmp","BarrackLv9_2.bmp","BarrackLv9_3.bmp"};

        public static Point GetPostionTownHall(Bitmap bSource, double tolerance)
        {
            List<Point> listPosition = GetPostionItem(bSource, sTowHall, 1, tolerance);
            if (listPosition.Count >= 1)
            {
                return listPosition[0];
            }
            return new Point();
        }
        public static Point GetPostionBarrack(Bitmap bSource, double tolerance)
        {
            List<Point> listPosition = GetPostionItem(bSource, sBarrack, 1, tolerance);
            if (listPosition.Count >= 1)
            {
                return listPosition[0];
            }
            return new Point();
        }

        public static List<Point> GetElixirEx(Bitmap bSource, double tolerance)
        {
            return GetPostionItem(bSource, sElixierEx, 6, tolerance);
        }

        public static List<Point> GetMineEx(Bitmap bSource, double tolerance)
        {
            return GetPostionItem(bSource, sMineEx, 6, tolerance);
        }

        public static List<Point> GetDarkEx(Bitmap bSource, double tolerance)
        {
            return GetPostionItem(bSource, sDarkEx, 2, tolerance);
        }



        /// <summary>
        /// Tolerance from 0.1 ---> 1
        /// </summary>
        /// <param name="bSource"></param>
        /// <param name="templateName"></param>
        /// <param name="maxItem"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        private static List<Point> GetPostionItem(Bitmap bSource, string[] templateName, int maxItem, double tolerance)
        {
            CVImageSBCv imgTool = new CVImageSBCv(bSource);
            List<Point> listPosition = new List<Point>();
            int itemFound = 0;
            for (int i = 0; i < templateName.Length; i++)
            {
                Bitmap bSearchBitmap = (Bitmap)Properties.Resources.ResourceManager.GetObject(templateName[i]);
                if (bSearchBitmap == null)
                {
                    continue;
                }
                HashSet<Point> listLocation = imgTool.GetPixelMatchImage(bSearchBitmap, tolerance);
                bSearchBitmap.Dispose();

                if (listLocation.Count > 0)
                {
                    foreach (Point p in listLocation)
                    {
                        if (!isAlreadyExist(p, listPosition))
                        {
                            listPosition.Add(p);
                            itemFound++;
                        }
                    }
                }

                if (itemFound >= maxItem)
                {
                    break;
                }
            }
            return listPosition;
        }

        /// <summary>
        /// find img in located Bitmap
        /// </summary>
        /// <param name="bSource"></param>
        /// <param name="bTemplate"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static Point FindImg(Bitmap bSource, Bitmap bTemplate, double tolerance = 0.31)
        {
            CvImageS imgTool = new CvImageS(bSource);
            return imgTool.searchBitmap(bTemplate, tolerance);
        }
        /// <summary>
        /// find img in located Bitmap
        /// </summary>
        /// <param name="bSource"></param>
        /// <param name="bTemplate"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static async Task<Point> FindImg(Bitmap bSource, string sTemplate, double tolerance = 0.31)
        {
            CvImageS imgTool = new CvImageS(bSource);
            using (Bitmap bTemplate = (Bitmap)Properties.Resources.ResourceManager.GetObject(sTemplate))
            {
                return await imgTool.searchBitmapAsync(bTemplate, tolerance);
            }
        }

        /// <summary>
        /// find img in fullbase
        /// </summary>
        /// <param name="bSource"></param>
        /// <param name="bTemplate"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static async Task<Point> FindImg(Bitmap bTemplate, double tolerance = 0.31)
        {
            ScreenCapture screenCapture = new ScreenCapture();
            screenCapture.SnapShot();
            CvImageS imgTool = new CvImageS(screenCapture.BitMap);
            return await imgTool.searchBitmapAsync(bTemplate, tolerance);
        }
        public static async Task<Point> FindImg(string sTemplate, double tolerance = 0.31)
        {
            using (Bitmap bTemplate = (Bitmap)Properties.Resources.ResourceManager.GetObject(sTemplate))
            {
                ScreenCapture screenCapture = new ScreenCapture();
                screenCapture.SnapShot();
                CvImageS imgTool = new CvImageS(screenCapture.BitMap);
                Point pReturn = await imgTool.searchBitmapAsync(bTemplate, tolerance);
                bTemplate.Dispose();
                return pReturn;
            }
        }

        public static async Task<Point> FindImg(List<string> ListsTemplate, double tolerance = 0.31)
        {
            ScreenCapture screenCapture = new ScreenCapture();
            screenCapture.SnapShot();
            if (GlobalVariables.isDebug)
            {
                screenCapture.BitMap.Save(GlobalVariables.allbaseDebug + DateTime.Now.Ticks + ".jpg");
            }
            CvImageS imgTool = new CvImageS(screenCapture.BitMap);
            foreach (string sTemplate in ListsTemplate)
            {
                using (Bitmap bTemplate = (Bitmap)Properties.Resources.ResourceManager.GetObject(sTemplate))
                {
                    Point pReturn = await imgTool.searchBitmapAsync(bTemplate, tolerance);
                    if (!pReturn.IsEmpty)
                    {
                        return pReturn;
                    }
                }
            }
            return new Point();
        }

        /// <summary>
        /// find img in area
        /// </summary>
        /// <param name="bSource"></param>
        /// <param name="bTemplate"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static Point FindImginArea(Bitmap bTemplate, int left, int top, int width, int height, double tolerance = 0.31, bool isGlobal = true)
        {
            ScreenCapture screenCapture = new ScreenCapture(isGlobal);
            screenCapture.SnapShot(left, top, width, height);
            CvImageS imgTool = new CvImageS(screenCapture.BitMap);
            Point pReturn = imgTool.searchBitmap(bTemplate, tolerance);
            if (!isGlobal)
            {
                screenCapture.BitMap.Dispose();
            }
            if (!pReturn.IsEmpty) return new Point(pReturn.X + left, pReturn.Y + top);
            return new Point();
        }

        public static Point FindImginArea(string sTemplate, int left, int top, int width, int height, double tolerance = 0.31, bool isGlobal = true)
        {
            Bitmap bTemplate = (Bitmap)Properties.Resources.ResourceManager.GetObject(sTemplate);
            ScreenCapture screenCapture = new ScreenCapture(isGlobal);
            screenCapture.SnapShot(left, top, width, height);
            CvImageS imgTool = new CvImageS(screenCapture.BitMap);
            Point pReturn = imgTool.searchBitmap(bTemplate, tolerance);
            bTemplate.Dispose();
            if (!isGlobal)
            {
                screenCapture.BitMap.Dispose();
            }
            if (!pReturn.IsEmpty) return new Point(pReturn.X + left, pReturn.Y + top);
            return new Point();
        }



        public static bool isAlreadyExist(Point position, List<Point> listPosition)
        {
            bool alreadyExist = false;
            foreach (Point point in listPosition)
            {

                if (Math.Abs(position.X - point.X) < 20 && Math.Abs(position.Y - point.Y) < 20)
                {
                    alreadyExist = true;
                    break;
                }
            }
            return alreadyExist;
        }
    }
}
