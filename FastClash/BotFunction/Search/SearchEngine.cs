﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.Pixel;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using FastClash.ViewModels;

namespace FastClash.BotFunction.Search
{
    class SearchEngine
    {
        private SearchViewModel _searchViewModel;
        private int baseskip = 0;

        public SearchEngine(SearchViewModel searchViewModel, int baseSkip = 0)
        {
            this._searchViewModel = searchViewModel;
            this.baseskip = baseSkip;
        }

        public SearchEngine()
        {
        }
        private void PrepareSearch()
        {
            Other.ClickFunction.Click(GlobalVariables.pointAway);
            SleepFunction.Sleep(500);
            LogFunction.SetLog("Going to Attack");
            Other.ClickFunction.Click(GlobalVariables.pointAtkButton);
            SleepFunction.Sleep(1000);
            Other.ClickFunction.Click(GlobalVariables.pointMatchButton);
            int iCount = 0;
            // make sure find a match page openned
            while (IMGTool.FindImginArea("findmatch", 129, 476, 200, 85).IsEmpty && iCount < 10)
            {
                SleepFunction.Sleep(200);
                iCount++;
            }
            //check for break shield
            Point pOkey = IMGTool.FindImginArea("okey", 397, 358, 220, 80);
            iCount = 0;
            while (pOkey.IsEmpty && iCount < 5)
            {
                SleepFunction.Sleep(200);
                pOkey = IMGTool.FindImginArea("okey", 397, 358, 220, 80);
                iCount++;
            }
            if (!pOkey.IsEmpty)
            {
                Other.ClickFunction.Click(pOkey.X + 45, pOkey.Y + 25);
            }
        }

        public EnemyVillage FirstEnemyBase()
        {
            PrepareSearch();
            EnemyVillage enemyVillage = new EnemyVillage();
            enemyVillage.GetInfo();
            return enemyVillage;
        }
        public static T DeepClone<T>(T obj)
        {
            T objResult;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                ms.Position = 0;
                objResult = (T)bf.Deserialize(ms);
            }
            return objResult;
        }

        public EnemyVillage DidASearch()
        {
            PrepareSearch();
            List<LootEntity> lootMinConditionsOld = _searchViewModel.LootMinConditions.ToList();
            List<LootEntity> lootMinConditions = DeepClone(lootMinConditionsOld);
            while (true)
            {
                baseskip++;
                if (_searchViewModel.IsReduction)
                {
                    if (baseskip % _searchViewModel.TimesSearch == 0)
                    {
                        string target = "Target ";
                        foreach (LootEntity lootEntity in lootMinConditions)
                        {
                            if (lootEntity.MinValue > 0) lootEntity.MinValue -= lootEntity.DecreaseValue;
                            target += " - " + lootEntity.LootName.ToString() + ": " + lootEntity.MinValue;
                        }
                        LogFunction.SetLog(target);
                    }

                }
                EnemyVillage enemyVillage = new EnemyVillage();
                enemyVillage.GetInfo();
                if (enemyVillage.Gold == 0 && enemyVillage.Elixir == 0)
                {
                    GlobalVariables.currentProcessing = CurrentProcessing.Searching;
                    return null;
                }
                enemyVillage.ReportInfo(baseskip);

                bool isConditionTrue = false;
                foreach (LootEntity lootEntity in lootMinConditions)
                {
                    if (enemyVillage.GetLootByName(lootEntity.LootName.ToString()) >= lootEntity.MinValue &&
                        lootEntity.MinValue > 0) isConditionTrue = true;
                    if (lootEntity.IsMust)
                    {
                        if (enemyVillage.GetLootByName(lootEntity.LootName.ToString()) < lootEntity.MinValue)
                        {
                            isConditionTrue = false;
                            break;
                        }
                    }

                }

                if (isConditionTrue)
                {
                    if (_searchViewModel.IsDeadbase)
                    {
                        ScreenCapture screen = new ScreenCapture();
                        if (CheckDeadbase(enemyVillage))
                        {
                            LogFunction.SetLog("Find enemy match!!!");
                            return enemyVillage;
                        }
                        LogFunction.SetLog("Not a DeadBase");
                    }
                    else
                    {
                        LogFunction.SetLog("Find enemy match!!!");
                        return enemyVillage;
                    }
                }
                Other.ClickFunction.Click(GlobalVariables.pointNextButton);
                SleepFunction.Sleep(500);
            }
            //return new EnemyVillage();
        }

        private bool CheckDeadbase(EnemyVillage enemy)
        {
            return enemy.IsDeadBase();
        }
    }
}
