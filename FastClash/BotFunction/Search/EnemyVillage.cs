﻿using System.Collections.Generic;
using System.Drawing;
using FastClash.BotFunction.Attack;
using FastClash.BotFunction.IMGFind;
using FastClash.BotFunction.MainScreen;
using FastClash.BotFunction.Other;
using FastClash.BotFunction.ReadText;

namespace FastClash.BotFunction.Search
{
    public class EnemyVillage
    {
        private long gold;
        public long Gold
        {
            get { return gold; }
        }
        private long elixir;
        public long Elixir
        {
            get { return elixir; }
        }
        private long dark;
        public long Dark
        {
            get { return dark; }
        }
        private long trophie;
        public long Trophie
        {
            get { return trophie; }
        }

        public long GetLootByName(string lootName)
        {
            if (lootName.ToLower() == "gold")
            {
                return Gold;
            }
            else if (lootName.ToLower() == "elixir")
            {
                return Elixir;
            }
            else if (lootName.ToLower() == "dark")
            {
                return Dark;
            }
            else if (lootName.ToLower() == "trophie")
            {
                return Trophie;
            }
            return 0;
        }

        public void GetInfo()
        {
            OcrEnemyText ocrEnemyText = new OcrEnemyText();
            gold = ocrEnemyText.GetGolds();
            int iCount = 0;
            while (gold == 0)
            {
                SleepFunction.Sleep(500);
                gold = ocrEnemyText.GetGolds();
                iCount++;
                if (iCount>60 || new Mainscreen().IsProblemAffect(true))
                {
                    new Mainscreen().WaitMainScreen();
                    elixir = 0;
                    dark = 0;
                    trophie = 0;
                    break;
                }
            }
            elixir = ocrEnemyText.GetElixirs();
            dark = ocrEnemyText.GetDarks();
            trophie = ocrEnemyText.GetTrophies();
        }

        public bool GoldElixirChange()
        {
            OcrEnemyText ocrEnemyText = new OcrEnemyText();
            long goldHere = ocrEnemyText.GetGolds();
            long elixirHere = ocrEnemyText.GetElixirs();
            long darkHere = ocrEnemyText.GetDarks();
            LogFunction.SetLog("Loot remaining - Gold : " + goldHere + " Elixir : " + elixirHere + " Dark : " + darkHere);
            if (goldHere != gold || elixirHere != elixir || darkHere != dark)
            {
                gold = goldHere;
                elixir = elixirHere;
                dark = darkHere;
                return true;
            }
            return false;
        }
        public bool IsEndBattle()
        {
            OcrEnemyText ocrEnemyText = new OcrEnemyText();
            long goldHere = ocrEnemyText.GetGolds();
            long elixirHere = ocrEnemyText.GetElixirs();
            long darkHere = ocrEnemyText.GetDarks();
            if (goldHere == 0 && elixirHere == 0 && darkHere == 0)
            {
                LogFunction.SetLog("-- Battle End --");
                return true;
            }
            return false;
        }

        public void ReportInfo(int icount=0)
        {
            LogFunction.SetLog(icount + " - G: " + gold +" -- E: " + elixir +" -- D: " + dark +" -- T: " + trophie);
        }
        public bool IsDeadBase()
        {
            //if (!IMGTool.FindImginArea("DbTro", 6, 12, 40, 30).IsEmpty)
            //{
            //    return true;
            //}
            List<string> ListsTemplate = new List<string>();
            for (int i = 1; i < 26; i++)
            {
                ListsTemplate.Add("e" + i);
            }
            if (!IMGTool.FindImg(ListsTemplate).Result.IsEmpty) return true;
            return false;
        }
        public List<HashSet<Point>> GetRedArea(int xSkip, int ySkip, int iColorVariation)
        {
            RedArea getRedArea = new RedArea();
            return getRedArea.GetRedArea(xSkip, ySkip, iColorVariation);
        }
        public List<HashSet<Point>> GetLineArea()
        {
            RedArea getRedArea = new RedArea();
            return getRedArea.GetLineArea();
        }

    }
}
