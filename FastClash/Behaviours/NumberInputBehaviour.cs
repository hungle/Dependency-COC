﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace FastClash.Behaviours
{
    public class NumberInputBehaviour
    {
        public static int GetMinValue(DependencyObject obj)
        {
            return (int)obj.GetValue(MinValueProperty);
        }

        public static void SetMinValue(DependencyObject obj, int value)
        {
            obj.SetValue(MinValueProperty, value);
        }

        // Using a DependencyProperty as the backing store for MinValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.RegisterAttached("MinValue", typeof(int), typeof(NumberInputBehaviour), new PropertyMetadata(0));



        public static int GetMaxValue(DependencyObject obj)
        {
            return (int)obj.GetValue(MaxValueProperty);
        }

        public static void SetMaxValue(DependencyObject obj, int value)
        {
            obj.SetValue(MaxValueProperty, value);
        }

        // Using a DependencyProperty as the backing store for MaxValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.RegisterAttached("MaxValue", typeof(int), typeof(NumberInputBehaviour), new PropertyMetadata(int.MaxValue));



        public static bool GetNumberInput(DependencyObject obj)
        {
            return (bool)obj.GetValue(NumberInputProperty);
        }

        public static void SetNumberInput(DependencyObject obj, bool value)
        {
            obj.SetValue(NumberInputProperty, value);
        }

        // Using a DependencyProperty as the backing store for NumberInput.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberInputProperty =
            DependencyProperty.RegisterAttached("NumberInput", typeof(bool), typeof(NumberInputBehaviour), new PropertyMetadata(false, NumberInputPropertyChanged));


        private static void NumberInputPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                bool numberInput = (e.NewValue != null) && (bool)e.NewValue;
                if (numberInput)
                {
                    tb.PreviewTextInput += CheckDigit;
                    tb.TextChanged += LimitValue;
                }
                else
                {
                    tb.PreviewTextInput -= CheckDigit;
                    tb.TextChanged -= LimitValue;
                }
            }

        }

        private static void CheckDigit(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private static void LimitValue(object sender, TextChangedEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb != null)
            {
                try
                {
                    int value = Convert.ToInt32(tb.Text);
                    int minValue = GetMinValue(tb);
                    int maxValue = GetMaxValue(tb);

                    value = value > maxValue ? maxValue : value;
                    value = value < minValue ? minValue : value;
                    tb.Text = value.ToString();
                    tb.CaretIndex = tb.Text.Length;
                }
                catch
                {

                }
            }
        }
    }
}
