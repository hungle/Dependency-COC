﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FastClash
{
    public enum CurrentProcessing
    {
        CheckMainScreen, DetectBuilding, CalcuateAtkBuildingLevel, WaitForFull, VillageLoot, ReArm ,Training,
        Searching, Attacking
    };
    public static class GlobalVariables
    {
        #region lockregion

        public static readonly object syncObj = new object(); // For locking...
        public static CancellationToken token;

        #endregion

        #region processing
        public static CurrentProcessing currentProcessing = CurrentProcessing.CheckMainScreen;
        public static bool isFirstTime = true;
        #endregion

        #region clickpoint
        public static Point pointAway = new Point(1, 1);
        public static Point pointAtkButton = new Point(60, 614);
        public static Point pointCamp = new Point(38, 527);
        public static Point pointMatchButton = new Point(217, 510);
        public static Point pointNextButton = new Point(825, 527);
        public static Point aReloadButton = new Point(416, 399);
        public static Point aSurrenderPoint = new Point(70, 545);
        public static Point aConfirmSurrenderPoint = new Point(512, 394);
        public static Point aReturnHomePoint = new Point(428, 544);
        public static Point PointClearTroops = new Point(479, 195);  //0xD1D0C2
        public static Point pointDonate = new Point(24,350);
        #endregion

        #region bitmap
        public static Bitmap BitMapGlobal;

        #endregion

        #region Properties

        internal static string AppPath
        {
            get { return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); }
        }

        /// <summary>
        /// Gets the LogWriter.
        /// Note: All the messages in the Output are already logged.
        /// Use this if you need to only write to the log file skipping the Output.
        /// </summary>
        /// <value>The LogWriter.</value>
        internal static ViewModels.LogWriter Log
        {
            get { return ViewModels.LogWriter.Instance; }
        }

        internal static string LogPath
        {
            get { return Path.Combine(AppPath, "Logs"); }
        }
        internal static string EndAttackedDir
        {
            get { return Path.Combine(AppPath, "Attacked"); }
        }

        internal static string ScreenshotZombieAttacked
        {
            get { return Path.Combine(AppPath, @"Screenshots\Zombies Attacked"); }
        }

        internal static string ScreenshotZombieSkipped
        {
            get { return Path.Combine(AppPath, @"Screenshots\Zombies Skipped"); }
        }

        internal static string BsTitle
        {
            get { return "BlueStacks App Player"; }
        }


        #endregion
        /// <summary>
        /// time return when have another conect device
        /// </summary>
        public static int sTimeWakeUp = 60;

        public static IntPtr HwnD;

        public static bool isDebug = false;
        public static string allbaseDebug = @"D://Test/newDB/";
        public static string ocrDebug = @"D://Test/ocr/";
       
        /*
        public static Bitmap hBitmap;
        public static Bitmap hHBitmap;

        public static IntPtr HWnD;

        public static int hLogFileHandle;
        public static bool restart = false;
        public static bool runState = false;
        public static bool backgroundMode = true;

        public static List<Point> barrackPos = new List<Point>();
        public static string[] barrackTroop = new string[] { "Barbarian", "Archer", "Giant", "Goblin", "Wallbreaker" };*/
    }
}
