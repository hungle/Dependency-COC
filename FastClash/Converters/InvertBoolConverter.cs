﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace FastClash.Converters
{
    [MarkupExtensionReturnType(typeof(IValueConverter))]
    class InvertBoolConverter : MarkupExtension , IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !(bool)value;
        }

        private InvertBoolConverter converter;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (converter == null)
            {
                converter = new InvertBoolConverter();
            }
            return converter;
        }

        public InvertBoolConverter()
        {

        }
    }
}
