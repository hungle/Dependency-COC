﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace FastClash.Converters
{
    [MarkupExtensionReturnType(typeof(IValueConverter))]
    class ObservableCollectionToStringConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var obs = value as ObservableCollection<string>;
            string result = "";
            foreach(var s in obs)
            {
                result += s + ",";
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var str = value as string;
            if(str != null)
            {
                var array = str.Split(',');
                return new ObservableCollection<string>(array);
            }
            return new ObservableCollection<string>();
        }

        private ObservableCollectionToStringConverter converter;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (converter == null)
            {
                converter = new ObservableCollectionToStringConverter();
            }
            return converter;
        }

        public ObservableCollectionToStringConverter()
        {

        }
    }
}
