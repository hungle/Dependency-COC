﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using FastClashResource;
using FastClashFunction;
using FastClashFunction.OCR;

namespace FastClash.OCR
{
    public class Ocr
    {
        private static double tolerance = 50;



        public String recognize(String bmpStr, String language)
        {
            Bitmap bmp = new Bitmap(bmpStr);
            FastBitmap fastBitmap = new FastBitmap(bmp);
            fastBitmap.LockBitmap();
            String result = recognize(fastBitmap, language);

            fastBitmap.UnlockBitmap();
            fastBitmap.Dispose();
            return result;
        }
        public String recognize(FastBitmap bmp, String language)
        {
            //string xmlContent =  (string)FastClash.Properties.Resources.ResourceManager.GetObject(prefixeSymbols + "_" + language.Replace('-','_'));
            string xmlContent = FCResource.Do.getResource(language);
            xmlContent = EncryptUtils.Decrypt(xmlContent, "AAECAwQFBgcICQoLDA0ODw==");
            List<Letter> listSymbol = SerializeUtils.DeSerialize<List<Letter>>(xmlContent);
            //List<Letter> listSymbol = listSymbolByLanguage[language];
            listSymbol.ForEach(letter => letter.reset());
            StringBuilder strBuilder = new StringBuilder();
            Dictionary<Point, Letter> listPointName = new Dictionary<Point, Letter>();
            int divHeight = 4;
            if (language == "coc-latinA")
            {
                divHeight = 2;
            }
            foreach (Letter symbol in listSymbol)
            {
                for (int y = 0; y < bmp.Height / divHeight; y++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {

                        if (ColorUtils.isMatchColor(bmp.GetPixel(x, y), Color.FromArgb(symbol.getFirstPoint().Value), tolerance))
                        {
                            Point p = new Point(x, y);
                            if (isMatchWithSymbol(bmp, p, symbol))
                            {
                                bool addSymbol = true;
                                int nbSymbolRemoved = 0;
                                List<KeyValuePair<Point, Letter>> listSymbolNearest = getSymbolPosNearestLetterX(listPointName, symbol, p);
                                if (listSymbolNearest.Count > 0)
                                {
                                    foreach (KeyValuePair<Point, Letter> symbolNearest in listSymbolNearest)
                                    {
                                        if (symbol.Width > symbolNearest.Value.Width || (symbol.Width == symbolNearest.Value.Width && symbol.Height > symbolNearest.Value.Height))
                                        {
                                            listPointName.Remove(symbolNearest.Key);
                                            nbSymbolRemoved++;

                                        }



                                    }
                                    addSymbol = (nbSymbolRemoved == listSymbolNearest.Count);
                                }
                                if (addSymbol)
                                {
                                    listPointName.Add(p, symbol);
                                }


                            }
                        }
                    }
                }
            }

            List<Point> listPointSorted = listPointName.Keys.OrderBy(p => p.X).ToList();

            KeyValuePair<Point, Letter> oldSymbolPoint = default(KeyValuePair<Point, Letter>);
            foreach (Point p in listPointSorted)
            {



                if (!oldSymbolPoint.Equals(default(KeyValuePair<Point, Letter>)))
                {
                    int distance = p.X - (oldSymbolPoint.Key.X + oldSymbolPoint.Value.Width);

                    if (distance > 3)
                        strBuilder.Append(" ");

                }
                strBuilder.Append(listPointName[p].Name);
                oldSymbolPoint = new KeyValuePair<Point, Letter>(p, listPointName[p]);
            }

            return strBuilder.ToString();

        }


        private bool isMatchWithSymbol(FastBitmap bmp, Point startPoint, Letter letter)
        {
            return isMatchWithSymbol(bmp, startPoint, letter, tolerance, 6);
        }

        private bool isMatchWithSymbol(FastBitmap bmp, Point startPoint, Letter letter, double tolerance, double maxMatchScore)
        {
            bool match = true;
            letter.PosLetter = startPoint;
            double matchScore = 0;

            foreach (KeyValuePair<Point, int> keyValue in letter.ListPointColor)
            {
                int x = startPoint.X + keyValue.Key.X;
                int y = startPoint.Y + keyValue.Key.Y;
                double matchScoreTemp;


                if ((x >= bmp.Width || y >= bmp.Height) || !((matchScoreTemp = ColorUtils.differenceColor(bmp.GetPixel(x, y), Color.FromArgb(keyValue.Value))) <= tolerance))
                {
                    match = false;
                    break;

                }

                matchScore += matchScoreTemp;


            }
            matchScore = matchScore / letter.ListPointColor.Count;

            if (match && matchScore < maxMatchScore)
            {
                letter.ListPointMatchScore.Add(startPoint, matchScore);
            }

            return (match && matchScore < maxMatchScore);
        }

        public List<KeyValuePair<Point, Letter>> getSymbolPosNearestLetterX(Dictionary<Point, Letter> listPointName, Letter letter, Point p)
        {
            List<KeyValuePair<Point, Letter>> pointLetter = new List<KeyValuePair<Point, Letter>>();
            foreach (KeyValuePair<Point, Letter> keyValue in listPointName)
            {
                bool nearestFound = p.X == keyValue.Key.X;

                if (!nearestFound && p.X >= keyValue.Key.X && (p.X <= (keyValue.Key.X + keyValue.Value.Width)))
                {
                    nearestFound = true;

                }
                if (!nearestFound && keyValue.Key.X >= p.X && (keyValue.Key.X <= (p.X + letter.Width)))
                {
                    nearestFound = true;

                }

                if (nearestFound)
                {
                    pointLetter.Add(keyValue);
                }


            }
            return pointLetter;
        }

        public List<KeyValuePair<Point, Letter>> getSymbolPosNearestLetterXY(Dictionary<Point, Letter> listPointName, Letter letter, Point p)
        {
            List<KeyValuePair<Point, Letter>> pointLetter = new List<KeyValuePair<Point, Letter>>();
            foreach (KeyValuePair<Point, Letter> keyValue in listPointName)
            {
                bool nearestFound = p.X == keyValue.Key.X && p.Y == keyValue.Key.Y;

                if (!nearestFound && (p.X >= keyValue.Key.X && (p.X <= (keyValue.Key.X + keyValue.Value.Width)) && p.Y >= keyValue.Key.Y && (p.Y <= (keyValue.Key.Y + keyValue.Value.Height))))
                {
                    nearestFound = true;
                }
                if (!nearestFound && (keyValue.Key.X >= p.X && (keyValue.Key.X <= (p.X + letter.Width)) && keyValue.Key.Y >= p.X && (keyValue.Key.Y <= (p.Y + letter.Height))))
                {
                    nearestFound = true;
                }

                if (nearestFound)
                {
                    pointLetter.Add(keyValue);
                }


            }
            return pointLetter;
        }


    }
}
