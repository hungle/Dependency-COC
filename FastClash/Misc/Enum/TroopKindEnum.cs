﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.Misc.IFace;

namespace FastClash.Misc.Enum
{
    public enum TroopKindEnum
    {
        Barbarian, Archer, Giant, Goblin, WallBreaker, Balloon, Wizard, Healer,
        Dragon
            , Pekka, Minion, HogRider, Valkyrie, Golem, Witch, LavaHound, King, Queen, ClanCastle, LightningSpell, HealingSpell, RageSpell,
        JumpSpell
            , EarthquakeSpell, FreezeSpell, HasteSpell, PoisonSpell
    }


}
