﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastClash.Misc.IFace
{
    public interface IReadbase
    {
        string GetText(int left, int top, int width, int height, string sourceString);
    }
}
