﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastClash.Misc.Enum;

namespace FastClash.Misc.IFace
{
    public interface ITroop
    {
        TroopKindEnum TropKind { get; set; }
        int Num { get; set; }
        Point Point { get; set; }
        string GetName();
        void Select();
    }
}
