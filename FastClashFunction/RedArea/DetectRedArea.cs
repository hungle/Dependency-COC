﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace FastClashFunction.RedArea
{
    public class DetectRedArea
    {
        FastBitmap lockBitmap;

        public DetectRedArea(FastBitmap lockBitmap)
        {
            this.lockBitmap = lockBitmap;

        }

        public enum VectorType { eVectorLeftTop, eVectorRightBottom, eVectorLeftBottom, eVectorRightTop };

        public HashSet<Point> PixelSearchRedAreaByX(Color iColor, int colorVariation, int xSkip, int ySkip, VectorType vectorType, int xMin, int yMin, int xMax, int yMax)
        {
            HashSet<Point> listPixelTemp = new HashSet<Point>();
            HashSet<Point> listPixel = new HashSet<Point>();

            bool finish = false;
            int y = yMin;
            double xMinSave = xMin;
            double xMaxSave = xMax;

            while (!finish)
            {

                if (y > yMax)
                {
                    finish = true;
                }

                for (int x = xMin; x < xMax; x += xSkip)
                {
                    // lockBitmap.SetPixel(x, y, Color.Black);
                    FindVectorsRedArea(ref listPixelTemp, x, y, iColor, colorVariation);
                }

                if (vectorType == VectorType.eVectorLeftTop)
                {
                    xMinSave += -1 * ySkip * 1.4;
                    xMin = (int)Math.Round(xMinSave, 0);
                }
                else if (vectorType == VectorType.eVectorRightTop)
                {
                    xMaxSave += 1 * ySkip * 1.4;
                    xMax = (int)Math.Round(xMaxSave, 0);
                }
                else if (vectorType == VectorType.eVectorLeftBottom)
                {
                    xMinSave += 1 * ySkip * 1.4;
                    xMin = (int)Math.Round(xMinSave, 0);
                }
                else if (vectorType == VectorType.eVectorRightBottom)
                {
                    xMaxSave += -1 * ySkip * 1.4;
                    xMax = (int)Math.Round(xMaxSave, 0);
                }

                y += ySkip;


            }
            for (int i = 0; i < listPixelTemp.Count - 1; i++)
            {
                listPixel.Add(GetOffestPixelRedArea2(listPixelTemp.ElementAt(i), vectorType, 3));
                // listPixel[i] = GetOffestPixelRedArea(listPixel[i]);
            }
            return listPixel;
        }

        public HashSet<Point> PixelSearchRedAreaByY(Color iColor, int colorVariation, int xSkip, int ySkip, VectorType vectorType, int xMin, int yMin, int xMax, int yMax)
        {
            HashSet<Point> listPixelTemp = new HashSet<Point>();
            HashSet<Point> listPixel = new HashSet<Point>();


            bool finish = false;
            int x = xMin;

            while (!finish)
            {
                if (x > xMax)
                {
                    finish = true;
                }
                for (int y = yMin; y < yMax; y += ySkip)
                {
                    // lockBitmap.SetPixel(x, y, Color.Black);
                    FindVectorsRedArea(ref listPixelTemp, x, y, iColor, colorVariation);

                }


                if (vectorType == VectorType.eVectorLeftTop)
                {
                    yMin += (int)Math.Round((double)-1 * xSkip * 3 / 4, 0);
                }
                else if (vectorType == VectorType.eVectorRightTop)
                {
                    yMin += (int)Math.Round((double)1 * xSkip * 3 / 4, 0);
                }
                else if (vectorType == VectorType.eVectorLeftBottom)
                {
                    yMax += (int)Math.Round((double)1 * xSkip * 3 / 4, 0);
                }
                else if (vectorType == VectorType.eVectorRightBottom)
                {
                    yMax += (int)Math.Floor((double)-1 * xSkip * 3 / 6);
                }

                x += xSkip;


            }
            for (int i = 0; i < listPixelTemp.Count - 1; i++)
            {
                listPixel.Add(GetOffestPixelRedArea2(listPixelTemp.ElementAt(i), vectorType, 3));

            }
            return listPixel;
        }

        public HashSet<Point> PixelSearchRedArea(Color iColor, int colorVariation, int xSkip, int ySkip, VectorType vectorType, int xMin, int yMin, int xMax, int yMax, int xSize = -1, int offsetSign = 1)
        {
            HashSet<Point> listPixelTemp = new HashSet<Point>();
            HashSet<Point> listPixel = new HashSet<Point>();
            double xMinSave = xMin;
            double xMaxSave = xMax;
            bool finish = false;
            int y = yMin;
            if (xSize > -1)
            {
                xMax = xSize;
            }

            while (!finish)
            {

                if (y > yMax)
                {
                    finish = true;
                }
                for (int x = xMin; x < xMax; x += xSkip)
                {
                    // lockBitmap.SetPixel(x, y, Color.Blue);  
                    FindVectorsRedArea(ref listPixelTemp, x, y, iColor, colorVariation + 10);

                }

                if (y > 300 && xSize == -1)
                {
                    offsetSign = -1;
                }

                xMinSave += offsetSign * -1 * ySkip * 1.4;
                xMin = (int)Math.Round(xMinSave, 0);
                if (xSize == -1)
                {
                    xMaxSave += (int)Math.Round((double)offsetSign * 1 * ySkip * 1.4, 0);
                }
                else
                {
                    xMax = xMin + xSize;
                }

                y += ySkip;


            }
            for (int i = 0; i < listPixel.Count - 1; i++)
            {
                //GetOffestPixelRedArea($PosTemp, $PosTemp[2], $PosTemp[3], $hBitmapFirst)
                listPixel.Add(GetOffestPixelRedArea2(listPixelTemp.ElementAt(i), vectorType, 3));

            }
            return listPixel;
        }

        public void FindVectorsRedArea(ref HashSet<Point> listPixel, int x, int y, Color color, int colorVariation)
        {
            int xSign = -1;
            int ySign = -1;
            bool found = false;

            List<object[]> vectorLeftRightBottomTop = new List<object[]> { new object[] { color, +4, -3 }, new object[] { color, +8, -6 }, new object[] { color, +12, -9 } };
            List<object[]> vectorLeftRightTopBottom = new List<object[]> { new object[] { color, +4, +3 }, new object[] { color, +8, +6 }, new object[] { color, +12, +9 } };
            List<object[]> vectorRightLeftTopBottom = new List<object[]> { new object[] { color, -4, +3 }, new object[] { color, -8, +6 }, new object[] { color, -12, +9 } };
            List<object[]> vectorRightLeftBottomTop = new List<object[]> { new object[] { color, -4, -3 }, new object[] { color, 8, -6 }, new object[] { color, -12, -9 } };

            Point pixelCheck = new Point(x, y);


            //!listPixel.Contains(pixelCheck) &&
            if (isColorMatch(lockBitmap.GetPixel(x, y), color, colorVariation))
            {
                found = false;
                if (MultiPixelSearch2(x, y, color, vectorLeftRightBottomTop, colorVariation))
                {
                    found = true;
                    xSign = 4;
                    ySign = -3;
                }
                else if (MultiPixelSearch2(x, y, color, vectorLeftRightTopBottom, colorVariation))
                {
                    found = true;
                    xSign = 4;
                    ySign = 3;
                }
                else if (MultiPixelSearch2(x, y, color, vectorRightLeftTopBottom, colorVariation))
                {
                    found = true;
                    xSign = -4;
                    ySign = 3;
                }
                else if (MultiPixelSearch2(x, y, color, vectorRightLeftBottomTop, colorVariation))
                {
                    found = true;
                    xSign = -4;
                    ySign = -3;
                }
                if (found)
                {
                    Point pixel = new Point(x, y);
                    HashSet<Point> vector = FindVectorRedArea(pixel, xSign, ySign, color, colorVariation);
                    // Local $vector = FindVectorRedArea($pixel, $xSign, $ySign, $iColorRed, $iColorVariation + 10, $hBitmapFirst)


                    foreach (Point pixelVector in vector)
                    {
                        listPixel.Add(pixelVector);
                    }

                }
            }
        }

        public bool isColorMatch(Color color1, Color color2, int colorVaration)
        {
            int Red1, Red2, Blue1, Blue2, Green1, Green2;

            Red1 = color1.R;
            Blue1 = color1.B;
            Green1 = color1.G;

            Red2 = color2.R;
            Blue2 = color2.B;
            Green2 = color2.G;


            if (Math.Abs(Blue1 - Blue2) > colorVaration) return false;
            if (Math.Abs(Green1 - Green2) > colorVaration) return false;
            if (Math.Abs(Red1 - Red2) > colorVaration) return false;


            return true;

        }

        public HashSet<Point> FindVectorRedArea(Point pixel, int xSign, int ySign, Color color, int colorVariation)
        {
            HashSet<Point> listPixel = new HashSet<Point>();
            HashSet<Point> vector = new HashSet<Point>();
            int x;
            int y;
            int xsave;
            int ysave;
            bool finish = false;
            int sizeOnCase = 7;
            for (int i = -1; i < 2; i += 2)
            {
                x = pixel.X;
                y = pixel.Y;

                finish = false;

                while (!finish)
                {
                    finish = true;

                    while (isColorMatch(lockBitmap.GetPixel(x, y), color, colorVariation))
                    {
                        Point pixelToAdd = new Point(x, y);

                        vector.Add(pixelToAdd);

                        x += xSign * i;
                        y += ySign * i;
                        if (x < 0 || y < 0)
                        {
                            break;
                        }
                    }


                    xsave = x;
                    ysave = y;
                    for (int k = -1; k < 1; k += 2)
                    {
                        for (int j = 1; j < 4; j++)
                        {
                            x = xsave + (int)Math.Round((double)j * k * xSign / 4 * 4 / 3 * sizeOnCase, 0);
                            y = ysave + (int)Math.Round((double)j * -1 * k * ySign / 3 * sizeOnCase, 0);

                            if (x < 0 || y < 0)
                            {
                                break;
                            }
                            if (isColorMatch(lockBitmap.GetPixel(x, y), color, colorVariation))
                            {
                                finish = false;
                                break;
                            }


                        }
                    }


                }

            }

            return vector;

        }

        public bool MultiPixelSearch2(int x, int y, Color firstColor, List<object[]> offColors, int colorVariation)
        {
            bool allchecked = false;
            if (isColorMatch(lockBitmap.GetPixel(x, y), firstColor, colorVariation))
            {

                allchecked = true;
                foreach (object[] offColor in offColors)
                {

                    if (!isColorMatch(lockBitmap.GetPixel(x + (int)offColor[1], y + (int)offColor[2]), (Color)offColor[0], colorVariation))
                    {
                        allchecked = false;
                        break;
                    }
                }
            }
            return allchecked;
        }

        public Point GetOffestPixelRedArea(Point pixel, VectorType vectorType)
        {

            int xOffset = 0;
            int yOffset = 0;
            Point pixelOffest = pixel;
            if (vectorType == VectorType.eVectorLeftTop)
            {
                yOffset = -3;
            }
            else if (vectorType == VectorType.eVectorLeftBottom)
            {
                xOffset = -3;
            }
            else if (vectorType == VectorType.eVectorRightTop)
            {
                xOffset = 3;
            }
            else if (vectorType == VectorType.eVectorRightBottom)
            {
                yOffset = 3;
            }


            Color color1 = lockBitmap.GetPixel(pixel.X + xOffset, pixel.Y + yOffset);
            Color color2 = lockBitmap.GetPixel(pixel.X + xOffset * -1, pixel.Y + yOffset * -1);


            if (color1.R > color2.R)
            {
                pixelOffest.X -= xOffset;
                pixelOffest.Y -= yOffset;
            }
            else
            {
                pixelOffest.X += xOffset;
                pixelOffest.Y += yOffset;
            }


            // Not select pixel in menu of troop
            if (pixelOffest.Y > 547)
            {
                pixelOffest.Y = 547;
            }
            return pixelOffest;
        }

        public Point GetOffestPixelRedArea2(Point pixel, VectorType vectorType, int offset = 3)
        {

            Point pixelOffest = pixel;

            if (vectorType == VectorType.eVectorLeftTop)
            {
                pixelOffest.X = (int)Math.Round((double)pixel.X - offset * 4 / 3, 0);
                pixelOffest.Y = pixel.Y - offset;
            }
            else if (vectorType == VectorType.eVectorRightBottom)
            {
                pixelOffest.X = (int)Math.Round((double)pixel.X + offset * 4 / 3, 0);
                pixelOffest.Y = pixel.Y + offset;
            }
            else if (vectorType == VectorType.eVectorLeftBottom)
            {
                pixelOffest.X = (int)Math.Round((double)pixel.X - offset * 4 / 3, 0);
                pixelOffest.Y = pixel.Y + offset;
            }
            else if (vectorType == VectorType.eVectorRightTop)
            {
                pixelOffest.X = (int)Math.Round((double)pixel.X + offset * 4 / 3, 0);
                pixelOffest.Y = pixel.Y - offset;
            }

            // Not select pixel in menu of troop
            if (pixelOffest.Y > 547)
            {
                pixelOffest.Y = 547;
            }

            return pixelOffest;

        }

        public HashSet<Point> removeBadPixel(HashSet<Point> listPixel, List<Color> listColorToMatch, int colorVariation)
        {

            HashSet<Point> listNewPixel = new HashSet<Point>();
            foreach (Point pixel in listPixel)
            {
                bool colorFound = false;
                Color color = lockBitmap.GetPixel(pixel.X, pixel.Y);
                foreach (Color colorToMatch in listColorToMatch)
                {
                    if (isColorMatch(color, colorToMatch, colorVariation))
                    {
                        colorFound = true;
                        break;
                    }
                }
                if (colorFound)
                {
                    listNewPixel.Add(pixel);
                }
            }
            return listNewPixel;
        }

        public HashSet<Point> VectorReadAreaNextOutZone(HashSet<Point> listPixel, VectorType vectorType)
        {
            HashSet<Point> listPixelNextOutZone = new HashSet<Point>();
            int y, xMin, xMax, yMin, yMax, xStep, yStep, xOffset, yOffset;

            if (vectorType == VectorType.eVectorLeftTop)
            {
                xMin = 37;
                xMax = 425;
                yMin = 313;
                yMax = 20;
                xStep = 4;
                yStep = -3;
                yOffset = -25;
                xOffset = yOffset;
            }
            else if (vectorType == VectorType.eVectorRightTop)
            {
                xMin = 417;
                xMax = 800;
                yMin = 39;
                yMax = 320;
                xStep = 4;
                yStep = 3;
                yOffset = -25;
                xOffset = yOffset * -1;
            }
            else if (vectorType == VectorType.eVectorLeftBottom)
            {
                xMin = 75;
                xMax = 451;
                yMin = 298;
                yMax = 580;
                xStep = 4;
                yStep = 3;
                yOffset = 25;
                xOffset = yOffset * -1;
            }
            else
            {
                xMin = 433;
                xMax = 800;
                yMin = 570;
                yMax = 300;
                xStep = 4;
                yStep = -3;
                yOffset = 25;
                xOffset = yOffset;
            }
            foreach (Point pixel in listPixel)
            {
                y = yMin;
                for (int x = xMin; x < xMax; x += xStep)
                {
                    if (vectorType == VectorType.eVectorRightBottom && y > yMax && pixel.X > x && pixel.Y > y)
                    {
                        Point pixelTemp = new Point(x + xOffset, y + yOffset);
                        listPixelNextOutZone.Add(pixelTemp);
                    }
                    else if (vectorType == VectorType.eVectorLeftBottom && y < yMax && pixel.X < x && pixel.Y > y)
                    {
                        Point pixelTemp = new Point(x + xOffset, y + yOffset);
                        listPixelNextOutZone.Add(pixelTemp);

                    }
                    else if (vectorType == VectorType.eVectorLeftTop && y > yMax && pixel.X < x && pixel.Y < y)
                    {
                        Point pixelTemp = new Point(x + xOffset, y + yOffset);
                        listPixelNextOutZone.Add(pixelTemp);
                    }
                    else if (vectorType == VectorType.eVectorRightTop && y < yMax && pixel.X > x && pixel.Y < y)
                    {
                        Point pixelTemp = new Point(x + xOffset, y + yOffset);
                        listPixelNextOutZone.Add(pixelTemp);
                    }

                    y += yStep;
                }

            }
            return listPixelNextOutZone;


        }

        public bool containsPixel(HashSet<Point> listPixel, Point pixel)
        {
            if (listPixel != null)
            {
                foreach (Point pixelTemp in listPixel)
                {
                    if (pixel.X == pixelTemp.X && pixel.Y == pixelTemp.Y)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
       
    }
}
