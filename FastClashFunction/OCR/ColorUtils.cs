﻿using System;
using System.Drawing;

namespace FastClashFunction.OCR
{
    public class ColorUtils
    {

        public static bool isMatchColor(Color color1, Color color2, double tolerance)
        {
            return differenceColor(color1, color2) <= tolerance;
        }

        public static double differenceColor(Color color1, Color color2)
        {
            double distance = 0;
            // Ignore transparency
            if ((color1.A == 255 && color2.A == 255) || (color1.A != 255 && color2.A != 255))
            {


                double dbl_input_red = Convert.ToDouble(color1.R);
                double dbl_input_green = Convert.ToDouble(color1.G);
                double dbl_input_blue = Convert.ToDouble(color1.B);

                // compute the Euclidean distance between the two colors
                // note, that the alpha-component is not used in this example
                double dbl_test_red = Math.Pow(Convert.ToDouble(((Color)color2).R) - dbl_input_red, 2.0);
                double dbl_test_green = Math.Pow(Convert.ToDouble
                    (((Color)color2).G) - dbl_input_green, 2.0);
                double dbl_test_blue = Math.Pow(Convert.ToDouble
                    (((Color)color2).B) - dbl_input_blue, 2.0);
                // it is not necessary to compute the square root
                // it should be sufficient to use:
                // temp = dbl_test_blue + dbl_test_green + dbl_test_red;
                // if you plan to do so, the distance should be initialized by 250000.0
                distance = Math.Sqrt(dbl_test_blue + dbl_test_green + dbl_test_red);
                //  Console.WriteLine(distance);
            }
            else
            {
                //  Console.WriteLine("Ignore color [" + color1 + "] / [" + color2 + "] because one is transparent and the other not");
            }
            //  Console.WriteLine("distance : "+distance);
            return distance;
        }

    }
}
