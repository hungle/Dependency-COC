﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace FastClashFunction.OCR
{
    public class SerializeUtils
    {

        public static  String Serialize<T>(T obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            String content = "";
            using (StringWriter textWriter = new StringWriter())
            {
                serializer.Serialize(textWriter, obj);
                content = textWriter.ToString();
                textWriter.Dispose();
            }
            return content;
        }

        public static T DeSerialize<T>(String xml)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            //   Console.WriteLine(DecryptFile(xml));
            TextReader reader = new StringReader(xml);
            object obj = deserializer.Deserialize(reader);
            T XmlData = (T)obj;
            reader.Close();
            return XmlData;
        }

    }
}
