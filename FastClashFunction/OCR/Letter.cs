﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FastClashFunction.OCR
{
    [Serializable]
    public class Letter
    {
        private SerializableDictionary<Point, int> listPointColor = new SerializableDictionary<Point, int>();
        private SerializableDictionary<Point, double> listPointMatchScore = new SerializableDictionary<Point, double>();
        private String name;
        //double matchScore;
        private Point posLetter;
        private int width;
        private int height;
        private int numberWhitePixel;


        public SerializableDictionary<Point, int> ListPointColor
        {
            set { this.listPointColor = value; }
            get { return this.listPointColor; }
        }

        public String Name
        {

            set { this.name = value; }
            get { return this.name; }
        }

        public SerializableDictionary<Point, double> ListPointMatchScore
        {

            set { this.listPointMatchScore = value; }
            get { return this.listPointMatchScore; }
        }

        public Point PosLetter
        {

            set { this.posLetter = value; }
            get { return this.posLetter; }
        }

        public int Width
        {

            set { this.width = value; }
            get { return this.width; }
        }

        public int Height
        {

            set { this.height = value; }
            get { return this.height; }
        }

     
        public int NumberWhitePixel
        {

            set { this.numberWhitePixel = value; }
            get { return this.numberWhitePixel; }
        }


        public Letter()
        { }

        public void reset()
        {
            listPointMatchScore = new SerializableDictionary<Point, double>();
        }

        public Letter(String name, SerializableDictionary<Point, int> listPointColor, int width)
        {
            this.name = name;
            this.listPointColor = listPointColor;
            this.width = width;
            
        }

        public Letter(SerializableDictionary<Point, int> listPointColor)
            : this("", listPointColor, 0)
        {
        }

        public Letter(String name)
            : this(name, new SerializableDictionary<Point, int>(), 0)
        {
        }

        public Letter(String name, int width)
            : this(name, new SerializableDictionary<Point, int>(), width)
        {
        }

        public void add(Point p, Color color)
        {
            listPointColor.Add(p, color.ToArgb());
        }

        public KeyValuePair<Point, int> getFirstPoint()
        {
            KeyValuePair<Point, int> keyValue = new KeyValuePair<Point, int>();
            if (listPointColor.Keys.Count > 0)
            {
                keyValue = listPointColor.ElementAt(0);
            }
            return keyValue;
        }

        public void searchHeight(Bitmap bmp)
        {
            numberWhitePixel = 0;
            int minHeight = 0;
            int maxHeight = 0;
            int tolerance = 60;
            bool found = false;
         
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                   
                    if (ColorUtils.isMatchColor(bmp.GetPixel(x, y), Color.FromArgb(255, 255, 255), tolerance))
                    {
                        minHeight = y;                       
                        found = true;
                        break;                        
                    }
                }
                if (found) break;
            }
            found = false;
            for (int y = bmp.Height - 1; y > 0; y--)
            {
                for (int x = 0; x < bmp.Width; x++)
                {

                    if (ColorUtils.isMatchColor(bmp.GetPixel(x, y), Color.FromArgb(255, 255, 255), tolerance))
                    {
                        maxHeight = y;
                        found = true;
                        break;                        
                    }
                }
                if (found) break;
            }

            this.height = maxHeight - minHeight + 1;


        }

        override
        public String ToString()
        {
            StringBuilder strBuilder = new StringBuilder();
            foreach (KeyValuePair<Point, int> keyValue in listPointColor)
            {
                strBuilder.AppendLine(keyValue.Key + " / " + Color.FromArgb(keyValue.Value));
            }
            return strBuilder.ToString();
        }
    }
}
