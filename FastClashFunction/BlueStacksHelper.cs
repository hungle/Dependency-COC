﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using Microsoft.Win32;

namespace FastClashFunction
{
    public static class BlueStacksHelper
    {
        private static IntPtr _bshandle = IntPtr.Zero;
        private static Process _bsProcess = null;

        public static bool IsBlueStacksFound { get { return _bshandle != IntPtr.Zero; } }

        public static IntPtr GetBlueStacksWindowHandle(bool force = false)
        {
            if (_bshandle == IntPtr.Zero || force)
                _bshandle = Win32.FindWindow("WindowsForms10.Window.8.app.0.33c0d9d", "BlueStacks App Player"); // First try
            if (_bshandle == IntPtr.Zero)
                _bshandle = Win32.FindWindow(null, "BlueStacks App Player"); // Maybe the class name has changes
            if (_bshandle == IntPtr.Zero)
            {
                Process[] proc = Process.GetProcessesByName("BlueStacks App Player"); // If failed, then try with .NET functions
                if (proc.Length == 0)
                    return IntPtr.Zero;
                _bshandle = proc[0].MainWindowHandle;
            }
            return _bshandle;
        }


        static List<IntPtr> GetAllChildrenWindowHandles(IntPtr hParent, int maxCount)
        {
            List<IntPtr> result = new List<IntPtr>();
            int ct = 0;
            IntPtr prevChild = IntPtr.Zero;
            while (true && ct < maxCount)
            {
                var currChild = Win32.FindWindowEx(hParent, prevChild, null, null);
                if (currChild == IntPtr.Zero) break;
                result.Add(currChild);
                prevChild = currChild;
                ++ct;
            }
            return result;
        }

        public static IntPtr GetMainControlOfBlueStacksHandle()
        {
            List<IntPtr> listIntPtrHere = GetAllChildrenWindowHandles(GetBlueStacksWindowHandle(), 20);
            while (listIntPtrHere.Count == 0)
            {
                Thread.Sleep(1000);
                listIntPtrHere = GetAllChildrenWindowHandles(GetBlueStacksWindowHandle(), 20);
            }
            return listIntPtrHere[0];
        }


        public static string GetBsDirectory()
        {
            var key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\BlueStacks", true);
            if (key != null)
            {
                var directory = Registry.GetValue(key.Name, "InstallDir", "C:\\Program Files\\BlueStacks\\").ToString();
                return directory;
            }
            return "";
        }

        public static void RunBlueStacks()
        {
            var directory = GetBsDirectory();
            try
            {
                if (GetBlueStacksWindowHandle() == IntPtr.Zero)
                {
                    _bsProcess = Process.Start(directory + "HD-StartLauncher.exe");
                }
                IsBlueStacksHidden = false;
            }
            catch
            {
                // ignored
            }
        }
        public static void CloseBlueStacks()
        {
            try
            {

            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static bool SetDimensionsIntoRegistry()
        {
            try
            {
                bool value = false;

                var key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\BlueStacks\Guests\Android\FrameBuffer\0", true);
                if (key != null)
                {
                    Registry.SetValue(key.Name, "WindowWidth", 0x0000035C, RegistryValueKind.DWord);
                    Registry.SetValue(key.Name, "WindowHeight", 0x000002D0, RegistryValueKind.DWord);
                    Registry.SetValue(key.Name, "GuestWidth", 0x0000035C, RegistryValueKind.DWord);
                    Registry.SetValue(key.Name, "GuestHeight", 0x000002D0, RegistryValueKind.DWord);

                    Registry.SetValue(key.Name, "Depth", 0x00000010, RegistryValueKind.DWord);
                    Registry.SetValue(key.Name, "FullScreen", 0x00000000, RegistryValueKind.DWord);
                    Registry.SetValue(key.Name, "WindowState", 0x00000001, RegistryValueKind.DWord);
                    Registry.SetValue(key.Name, "HideBootProgress", 0x00000001, RegistryValueKind.DWord);

                    key.Close();

                    value = true;
                }

                return value;
            }
            catch (Exception exception)
            {
                Debug.Assert(false, exception.Message);
                return false;
            }
        }

        public static bool Click(int x, int y)
        {
            return Click(new Point(x, y));
        }

        public static bool Click(Point point)
        {
            if (_bshandle == IntPtr.Zero)
                _bshandle = GetBlueStacksWindowHandle();
            if (_bshandle == IntPtr.Zero)
                return false;
            MouseHelper.ClickOnPoint(_bshandle, point);
            return true;
        }

        #region Properties

        /// <summary>
        /// Gets a value indicating whether BlueStacks is running.
        /// </summary>
        /// <value><c>true</c> if BlueStacks is running; otherwise, <c>false</c>.</value>
        public static bool IsBlueStacksRunning
        {
            get
            {
                _bshandle = IntPtr.Zero;
                return GetBlueStacksWindowHandle() != IntPtr.Zero;
            }
        }

        /// <summary>
        /// Gets a value indicating whether BlueStacks is hidden.
        /// </summary>
        public static bool IsBlueStacksHidden { get; set; }

        /// <summary>
        /// Gets a value indicating whether BlueStacks is running with required dimensions.
        /// </summary>
        /// <value><c>true</c> if this BlueStacks is running with required dimensions; otherwise, <c>false</c>.</value>
        public static bool IsRunningWithRequiredDimensions
        {
            get
            {
                Win32.Rect rct;
                Win32.GetClientRect(_bshandle, out rct);

                var width = rct.Right - rct.Left; // in Win32 Rect, right and bottom are considered as excluded from the rect. 
                var height = rct.Bottom - rct.Top;

                return (width == 860) && (height == 720);
            }
        }

        #endregion

        /// <summary>
        /// Activates and displays the window. If the window is 
        /// minimized or maximized, the system restores it to its original size 
        /// and position. An application should use this when restoring 
        /// a minimized window.
        /// </summary>
        /// <returns></returns>
        public static bool RestoreBlueStacks()
        {
            if (!IsBlueStacksRunning) return false;
            return Win32.ShowWindow(_bshandle, Win32.WindowShowStyle.Restore);
        }

        /// <summary>
        /// Activates the window and displays it in its current size and position.
        /// </summary>
        /// <returns></returns>
        public static bool ActivateBlueStacks()
        {
            if (!IsBlueStacksRunning) return false;
            return Win32.ShowWindow(_bshandle, Win32.WindowShowStyle.Show);
        }

        /// <summary>
        /// Hides the window and activates another window.
        /// </summary>
        /// <returns></returns>
        public static bool HideBlueStacks()
        {
            bool success;
            if (!IsBlueStacksRunning) return false;
            Win32.Rect rcWindow/*,rcClient*/;
            Win32.GetWindowRect(_bshandle, out rcWindow);
            var width = rcWindow.Right - rcWindow.Left;
            var height = rcWindow.Bottom - rcWindow.Top;
            success = Win32.MoveWindow(_bshandle, -32000, -32000, width, height, false);
            if (success) { IsBlueStacksHidden = !IsBlueStacksHidden; }
            return success;
        }

        public static bool ShowBlueStacks()
        {
            if (!IsBlueStacksRunning) return false;
            bool success;
            Win32.Rect rcWindow/*,rcClient*/;
            Win32.GetWindowRect(_bshandle, out rcWindow);
            //Win32.GetClientRect(_bshandle, out rcClient);
            var width = rcWindow.Right - rcWindow.Left;
            var height = rcWindow.Bottom - rcWindow.Top;
            if (Win32.MoveWindow(_bshandle, 0, 0, width, height, true))
            {
                success = ActivateBlueStacks(); ;
                if (success) { IsBlueStacksHidden = !IsBlueStacksHidden; }
                return success;
            }
            return false;
        }
    }
}