﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace FastClashFunction
{

    public unsafe class FastBitmap : IDisposable
    {
        Bitmap _bitmap;
        int _width;
        BitmapData _bitmapData = null;
        byte* _base = null;

        public int Width { set; get; }

        public int Height { set; get; }

        public FastBitmap(Bitmap bitmap)
        {
            this._bitmap = new Bitmap(bitmap);
            this.Height = bitmap.Height;
            this.Width = bitmap.Width;
        }

        public FastBitmap(int width, int height)
        {
            this._bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            this.Height = _bitmap.Height;
            this.Width = _bitmap.Width;
        }

        public void Dispose()
        {
            _bitmap.Dispose();
        }

        private Point PixelSize
        {
            get
            {
                GraphicsUnit unit = GraphicsUnit.Pixel;
                RectangleF bounds = _bitmap.GetBounds(ref unit);

                return new Point((int)bounds.Width, (int)bounds.Height);
            }
        }

        public void LockBitmap()
        {
            GraphicsUnit unit = GraphicsUnit.Pixel;
            RectangleF boundsF = _bitmap.GetBounds(ref unit);
            Rectangle bounds = new Rectangle(
                (int)boundsF.X,
                (int)boundsF.Y,
                (int)boundsF.Width,
                (int)boundsF.Height
            );

            // Figure out the number of bytes in a row
            // This is rounded up to be a multiple of 4
            // bytes, since a scan line in an image must always be a multiple of 4 bytes
            // in length.
            _width = (int)boundsF.Width * sizeof(PixelData);
            if (_width % 4 != 0)
            {
                _width = 4 * (_width / 4 + 1);
            }
            _bitmapData = _bitmap.LockBits(bounds, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            _base = (Byte*)_bitmapData.Scan0.ToPointer();
        }

        //public PixelData GetPixel(int x, int y)
        //{
        //    PixelData returnValue = *PixelAt(x, y);
        //    return returnValue;
        //}

        public Color GetPixel(int x, int y)
        {
            PixelData returnValue = *PixelAt(x, y);
            return Color.FromArgb(returnValue.Red, returnValue.Green, returnValue.Blue);
        }

        public Color GetPixel(Point point)
        {
            return GetPixel(point.X, point.Y);
        }

        public void SetPixel(int x, int y, PixelData colour)
        {
            PixelData* pixel = PixelAt(x, y);
            *pixel = colour;
        }

        public void UnlockBitmap()
        {
            _bitmap.UnlockBits(_bitmapData);
            _bitmapData = null;
            _base = null;
        }

        public PixelData* PixelAt(int x, int y)
        {
            return (PixelData*)(_base + y * _width + x * sizeof(PixelData));
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }

    public struct PixelData
    {
        public byte Blue;
        public byte Green;
        public byte Red;

        public PixelData(Color color)
        {
            Blue = color.B;
            Green = color.G;
            Red = color.R;
        }
    }

}
