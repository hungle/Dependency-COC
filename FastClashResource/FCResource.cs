﻿namespace FastClashResource
{
    public class FCResource
    {
        static public FCResource Do 
        {
        get { return new FCResource(); }
        }

        private FCResource()
        {
        }

        public string getResource(string language)
        {
            return (string)Properties.Resources.ResourceManager.GetObject(language.Replace('-','_'));
        }
    }
}
