﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;

namespace ImgProcessing.CV.PInvoke
{
    public class CvImageS
    {
        private Bitmap source;

        public CvImageS(Bitmap bSource)
        {
            if (bSource.Width == 860 && bSource.Height == 720)
            {
                crop(bSource);
            }
            this.source=bSource;
        }

        public Task<Point> searchBitmapAsync(Bitmap smallBmp, double tolerance = 0.31)
        {
            return Task.Run(() => searchBitmap(smallBmp, tolerance));
        }

        public Point searchBitmap(Bitmap smallBmp, double tolerance = 0.31)
        {
            BitmapData smallData =
              smallBmp.LockBits(new Rectangle(0, 0, smallBmp.Width, smallBmp.Height),
                       System.Drawing.Imaging.ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            BitmapData bigData =
              source.LockBits(new Rectangle(0, 0, source.Width, source.Height),
                       System.Drawing.Imaging.ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            int smallStride = smallData.Stride;
            int bigStride = bigData.Stride;

            int bigWidth = source.Width;
            int bigHeight = source.Height - smallBmp.Height + 1;
            int smallWidth = smallBmp.Width * 3;
            int smallHeight = smallBmp.Height;

            Rectangle location = Rectangle.Empty;
            int margin = Convert.ToInt32(255.0 * tolerance);

            unsafe
            {
                byte* pSmall = (byte*)(void*)smallData.Scan0;
                byte* pBig = (byte*)(void*)bigData.Scan0;

                int smallOffset = smallStride - smallBmp.Width * 3;
                int bigOffset = bigStride - source.Width * 3;

                bool matchFound = true;

                for (int y = 0; y < bigHeight; y++)
                {
                    for (int x = 0; x < bigWidth; x++)
                    {
                        byte* pBigBackup = pBig;
                        byte* pSmallBackup = pSmall;

                        //Look for the small picture.
                        for (int i = 0; i < smallHeight; i++)
                        {
                            int j = 0;
                            matchFound = true;
                            for (j = 0; j < smallWidth; j++)
                            {
                                //With tolerance: pSmall value should be between margins.
                                int inf = pBig[0] - margin;
                                int sup = pBig[0] + margin;
                                if (sup < pSmall[0] || inf > pSmall[0])
                                {
                                    matchFound = false;
                                    break;
                                }

                                pBig++;
                                pSmall++;
                            }

                            if (!matchFound) break;

                            //We restore the pointers.
                            pSmall = pSmallBackup;
                            pBig = pBigBackup;

                            //Next rows of the small and big pictures.
                            pSmall += smallStride * (1 + i);
                            pBig += bigStride * (1 + i);
                        }

                        //If match found, we return.
                        if (matchFound)
                        {
                            location.X = x;
                            location.Y = y;
                            location.Width = smallBmp.Width;
                            location.Height = smallBmp.Height;
                            break;
                        }
                        //If no match found, we restore the pointers and continue.
                        else
                        {
                            pBig = pBigBackup;
                            pSmall = pSmallBackup;
                            pBig += 3;
                        }
                    }

                    if (matchFound)
                    {
                        source.UnlockBits(bigData);
                        smallBmp.UnlockBits(smallData);
                        return new Point(location.X, location.Y);                    
                    }// break;

                    pBig += bigOffset;
                }
            }

            source.UnlockBits(bigData);
            smallBmp.UnlockBits(smallData);

            return new Point();
        }

        public void crop(Bitmap bSource)
        {
            List<Point> polygonPoints1 = new List<Point>();
            polygonPoints1.Add(new Point(0, 0));
            polygonPoints1.Add(new Point(434, 0));
            polygonPoints1.Add(new Point(14, 308));

            List<Point> polygonPoints2 = new List<Point>();
            polygonPoints2.Add(new Point(0, 0));
            polygonPoints2.Add(new Point(14, 308));
            polygonPoints2.Add(new Point(0, 720));

            List<Point> polygonPoints3 = new List<Point>();
            polygonPoints3.Add(new Point(0, 720));
            polygonPoints3.Add(new Point(14, 308));
            polygonPoints3.Add(new Point(428, 629));


            List<Point> polygonPoints4 = new List<Point>();
            polygonPoints4.Add(new Point(0, 720));
            polygonPoints4.Add(new Point(428, 629));
            polygonPoints4.Add(new Point(860, 720));

            List<Point> polygonPoints5 = new List<Point>();
            polygonPoints5.Add(new Point(860, 310));
            polygonPoints5.Add(new Point(428, 629));
            polygonPoints5.Add(new Point(860, 720));

            List<Point> polygonPoints6 = new List<Point>();
            polygonPoints6.Add(new Point(860, 310));
            polygonPoints6.Add(new Point(434, 0));
            polygonPoints6.Add(new Point(860, 0));


            //all of the rgb values are being set 1 inside the polygon 
            SolidBrush Brush = new SolidBrush(Color.FromArgb(1, 1, 1));
            //we have to prepare one mask of Multiplying operation for cropping region
            Graphics g = Graphics.FromImage(bSource);
            g.FillPolygon(Brushes.Black, polygonPoints1.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints2.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints3.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints4.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints5.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints6.ToArray());
            //   g.FillClosedCurve(Brushes.Black, polygonPoints.ToArray());

            //bSource.Save(@"D:\\test\\crop\\result.png");
        }

    }
}
