﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ImgProcessing.CV.PInvoke
{
    public class CVImageSBCv
    {
        private Bitmap _bSource;

        public CVImageSBCv(Bitmap bSource)
        {
            if (bSource.Width == 860 && bSource.Height == 720)
            {
                crop(bSource);
            }
            this._bSource = bSource;
        }

        public static HashSet<Point> RemoveDuplicate(HashSet<Point> listPixel)
        {
            HashSet<Point> newListPixel = new HashSet<Point>(listPixel);
            for (int i = 0; i < listPixel.Count; i++)
            {
                for (int j = i + 1; j < listPixel.Count; j++)
                {

                    if (Math.Abs(listPixel.ElementAt(i).X - listPixel.ElementAt(j).X) < 20 && Math.Abs(listPixel.ElementAt(i).Y - listPixel.ElementAt(j).Y) < 20)
                    {
                        newListPixel.Remove(listPixel.ElementAt(i));
                        break;
                    }
                }
            }
            return newListPixel;
        }



        public HashSet<Point> GetPixelMatchImage(Bitmap bTemplate, double tolerance)
        {
            Image<Bgr, byte> source = new Image<Bgr, byte>(_bSource);
            Image<Bgr, byte> template = new Image<Bgr, byte>(bTemplate);
            HashSet<Point> listPixelMatch = new HashSet<Point>();

            using (Image<Gray, float> result = source.MatchTemplate(template, Emgu.CV.CvEnum.TM_TYPE.CV_TM_CCOEFF_NORMED))
            { 

                float[, ,] matches = result.Data;
                for (int y = 0; y < matches.GetLength(0); y++)
                {
                    for (int x = 0; x < matches.GetLength(1); x++)
                    {
                        double matchScore = matches[y, x, 0];
                        if (matchScore > tolerance)
                        {
                            listPixelMatch.Add(new Point(x + bTemplate.Width / 2, y + bTemplate.Height / 2));

                        }

                    }

                }
            }
            return RemoveDuplicate(listPixelMatch);

        }



        public Point GetPointMathchImage(Bitmap bTemplate, double tolerance)
        {
            Image<Bgr, byte> source = new Image<Bgr, byte>(_bSource);
            Image<Bgr, byte> template = new Image<Bgr, byte>(bTemplate);
            HashSet<Point> listPixelMatch = new HashSet<Point>();

            using (Image<Gray, float> result = source.MatchTemplate(template, Emgu.CV.CvEnum.TM_TYPE.CV_TM_CCOEFF_NORMED))
            {
                double[] minValues, maxValues;
                Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

                // You can try different values of the threshold. I guess somewhere between 0.75 and 0.95 would be good.
                if (maxValues[0] > 0.9)
                {
                    template.Dispose();
                    return maxLocations[0];
                }
            }
            return new Point();

        }


        public void crop(Bitmap bSource)
        {
            List<Point> polygonPoints1 = new List<Point>();
            polygonPoints1.Add(new Point(0, 0));
            polygonPoints1.Add(new Point(434, 0));
            polygonPoints1.Add(new Point(14, 308));

            List<Point> polygonPoints2 = new List<Point>();
            polygonPoints2.Add(new Point(0, 0));
            polygonPoints2.Add(new Point(14, 308));
            polygonPoints2.Add(new Point(0, 720));

            List<Point> polygonPoints3 = new List<Point>();
            polygonPoints3.Add(new Point(0, 720));
            polygonPoints3.Add(new Point(14, 308));
            polygonPoints3.Add(new Point(428, 629));


            List<Point> polygonPoints4 = new List<Point>();
            polygonPoints4.Add(new Point(0, 720));
            polygonPoints4.Add(new Point(428, 629));
            polygonPoints4.Add(new Point(860, 720));

            List<Point> polygonPoints5 = new List<Point>();
            polygonPoints5.Add(new Point(860, 310));
            polygonPoints5.Add(new Point(428, 629));
            polygonPoints5.Add(new Point(860, 720));

            List<Point> polygonPoints6 = new List<Point>();
            polygonPoints6.Add(new Point(860, 310));
            polygonPoints6.Add(new Point(434, 0));
            polygonPoints6.Add(new Point(860, 0));


            //all of the rgb values are being set 1 inside the polygon 
            SolidBrush Brush = new SolidBrush(Color.FromArgb(1, 1, 1));
            //we have to prepare one mask of Multiplying operation for cropping region
            Graphics g = Graphics.FromImage(bSource);
            g.FillPolygon(Brushes.Black, polygonPoints1.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints2.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints3.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints4.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints5.ToArray());
            g.FillPolygon(Brushes.Black, polygonPoints6.ToArray());
            //   g.FillClosedCurve(Brushes.Black, polygonPoints.ToArray());

            //bSource.Save(@"D:\\test\\crop\\result.png");
        }

    }
}
